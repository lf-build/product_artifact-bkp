var gulp = require('gulp');
var replace = require('gulp-replace');
var argv = require('yargs').argv;
var path = require('path')
var fs = require('fs');

var currentFile = argv.file;
var configuration = argv.configuration;
var config = require("./" + configuration + '.json');
var api = require('./deployment-scripts/api_module.js')(config.authorizationToken);
var serviceUrl = config.baseUrl

var templateSvc = require('./deployment-scripts/templateService.js')(config.authorizationToken);
var lookupSvc = require('./deployment-scripts/lookupService.js')(config.authorizationToken);
var configurationSvc = require('./deployment-scripts/configurationService.js')(config.authorizationToken);
var ruleSvc = require('./deployment-scripts/ruleService.js')(config.authorizationToken);
var productSvc = require('./deployment-scripts/productService.js')(config.authorizationToken);
var filterSvc = require('./deployment-scripts/filterService.js')(config.authorizationToken);
var environments = ["qa", "dev", "uat", "staging", "stagsimulation", "prodsimulation", "production"]

/**
 * Publish a portal configuration to the deployment env of your choice
 * @example gulp --configuration=dev --file=./configurations/back-office-portal.dev.json publishConfiguration
 */
gulp.task('publishConfiguration', function () {
    if (!currentFile) {
        throw {
            name: "InvalidFileError",
            message: "A file must be specified with the flag --file",
            stack: new Error().stack
        }
    }

    console.log('Service Url:' + serviceUrl + config.configurationConfiguration.servicePort);
    return configurationSvc.saveConfiguration(
        environments,
        configuration,
        serviceUrl + config.configurationConfiguration.servicePort,
        currentFile,
        config
    );
});

gulp.task('publishAllConfigurations', function () {
    console.log('Service Url:' + serviceUrl + config.configurationConfiguration.servicePort);
    return configurationSvc.importConfiguration(environments, configuration, serviceUrl + config.configurationConfiguration.servicePort, currentFile + "/Configuration", config);
});

gulp.task('publishTemplate', function () {
    console.log('Service Url:' + serviceUrl + config.templateConfiguration.servicePort);
    console.log('Publishing Open Template ' + currentFile);
    return templateSvc.saveTemplate(serviceUrl + config.templateConfiguration.servicePort, currentFile, config);
});

gulp.task('publishAllTemplates', function () {
    console.log('Service Url:' + serviceUrl + config.templateConfiguration.servicePort);
    console.log('Publishing Open Template ' + currentFile + "/templates");
    return templateSvc.importTemplates(serviceUrl + config.templateConfiguration.servicePort, currentFile + "/Templates", config);
});

gulp.task('publishRule', function () {
    console.log('Service Url:' + serviceUrl + config.ruleConfiguration.servicePort);
    var pathObject = path.parse(currentFile);
    if (pathObject.ext.toLocaleLowerCase() == ".js" || pathObject.ext.toLocaleLowerCase() == ".ts") {
        console.log('Publishing Open Rule ' + currentFile);
        var data = fs.readFileSync(currentFile, 'utf8');
        return ruleSvc.saveRule(serviceUrl + config.ruleConfiguration.servicePort, data, config);
    }
    else {
        console.warn('Skipping file ' + currentFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
});

gulp.task('publishAllRule', function () {
    console.log('Service Url:' + serviceUrl + config.ruleConfiguration.servicePort);
    return ruleSvc.importRules(serviceUrl + config.ruleConfiguration.servicePort, currentFile + "/Rules", config);
});

gulp.task('publishLookup', function () {
    console.log('Service Url:' + serviceUrl + config.lookupServiceConfiguration.servicePort);
    console.log('Publishing Open Lookup ' + currentFile);
    return lookupSvc.saveLookup(serviceUrl + config.lookupServiceConfiguration.servicePort, currentFile);
});

gulp.task('publishAllLookup', function () {
    console.log('Service Url:' + serviceUrl + config.lookupServiceConfiguration.servicePort);
    console.log('Publishing Open Lookup ' + currentFile + "/lookup");
    return lookupSvc.importLookup(serviceUrl + config.lookupServiceConfiguration.servicePort, currentFile + "/Lookup");
});

gulp.task('publishProduct', function () {
    console.log('Service Url:' + serviceUrl + config.productServiceConfiguration.servicePort);
    console.log('Publishing Product ' + currentFile);
    return productSvc.saveProduct(serviceUrl + config.productServiceConfiguration.servicePort, currentFile);
});

gulp.task('publishAllProduct', function () {
    console.log('Service Url:' + serviceUrl + config.productServiceConfiguration.servicePort);
    console.log('Publishing Product ' + currentFile + "/Product");
    return productSvc.importProduct(serviceUrl + config.productServiceConfiguration.servicePort, currentFile + "/Product");
});


gulp.task('publishFilter', function () {
    console.log('Service Url:' + serviceUrl + config.filterServiceConfiguration.servicePort);
    console.log('Publishing Filter ' + currentFile);
    return filterSvc.saveFilter(serviceUrl + config.filterServiceConfiguration.servicePort, currentFile);
});

gulp.task('publishAllFilter', function () {
    console.log('Service Url:' + serviceUrl + config.filterServiceConfiguration.servicePort);
    console.log('Publishing Filter ' + currentFile + '/saveFilters');
    return filterSvc.importFilter(serviceUrl + config.filterServiceConfiguration.servicePort, currentFile + '/saveFilters');
});

gulp.task('publishAll', ['publishAllConfigurations', 'publishAllTemplates', 'publishAllRule', 'publishAllLookup', 'publishAllFilter']);

var walkSync = function (dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        }
        else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};