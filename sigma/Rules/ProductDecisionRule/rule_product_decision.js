function rule_product_decision(Product) {
    var mcaProducts = ['startermca', 'traditionalmca'];
    var mcaLocProducts = ['streamlineloc', 'premiumloc', 'platinumloc'];
    if (Product != null) {
        var productId = mcaProducts.filter(function (x) {
            if (x == Product.toLowerCase()) {
                return x;
            }
        });
        if (productId != null && productId.length > 0) {
            return 'mca';
        }
        productId = mcaLocProducts.filter(function (x) {
            if (x == Product.toLowerCase()) {
                return x;
            }
        });
        if (productId != null && productId.length > 0) {
            return 'mcaloc';
        } else {
            return 'OtherLenders';
        }
    }
    return 'Product is not available';
}





