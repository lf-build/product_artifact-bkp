function default_offer(payload) {
    if (payload != null && typeof (payload) != 'undefined') {
        var productId = '';
        if (payload.selectedProduct) {
            productId = payload.selectedProduct[0].ProductId.toLowerCase(); 
        } else if (payload.cashflowVerificationManualData) {
            productId = payload.cashflowVerificationManualData[0].ProductList.toLowerCase(); 
        }
        var StarterMCAOffer = {
            'MinAmount': 2500,
            'MaxAmount': 5000,
            'MinFactor': 1.45,
            'MaxFactor': 1.45,
            'MinDuration': 4,
            'MaxDuration': 4,
            'FunderFee': 295,
            'Comp': 15,
            'MaxGross': 20,
            'PSF': 0
        };
        var TraditionalMCAOffer = {
            'MinAmount': 5000,
            'MaxAmount': 75000,
            'MinFactor': 1.24,
            'MaxFactor': 1.34,
            'MinDuration': 4,
            'MaxDuration': 6,
            'FunderFee': 3,
            'Comp': 0,
            'MaxGross': 20,
            'PSF': 2
        };
        var StreamLineLocOffer = {
            'MinAmount': 5000,
            'MaxAmount': 50000,
            'MinFactor': 1.35,
            'MaxFactor': 1.44,
            'MinDuration': 6,
            'MaxDuration': 6,
            'FunderFee': 3,
            'Comp': 8,
            'MaxGross': 18,
            'PSF': 0
        };
        var PremiumLocOffer = {
            'MinAmount': 5000,
            'MaxAmount': 50000,
            'MinFactor': 1.29,
            'MaxFactor': 1.29,
            'MinDuration': 6,
            'MaxDuration': 6,
            'FunderFee': 3,
            'Comp': 7,
            'MaxGross': 18,
            'PSF': 0
        };
        var PletinumLocOffer = {
            'MinAmount': 5000,
            'MaxAmount': 50000,
            'MinFactor': 1.25,
            'MaxFactor': 1.25,
            'MinDuration': 6,
            'MaxDuration': 6,
            'FunderFee': 3,
            'Comp': 5,
            'MaxGross': 15,
            'PSF': 0
        };
        var Data = null;
        var result = 'Failed';
        var Productresult = {
            'productResult': Data
        };
        var StarterMCAdeal={
            'Payment' :'2378.5' ,
            'PaymentFrequency' : 'Weekly' ,
            'Funder' : 'Lendfoundry' ,
            'ApprovedAmount' :'70000' ,
            'Factor' : '1.5' ,
            'PSF' : '11.00' ,
            'ProcessingFees': '25.00' ,
            'comments' : 'Default Deal' ,
            'Product' : 'MCA Product 2' ,
            'Program' : 'Standard' ,
            'offerTerm': '6' ,
            'Comp' : '11' ,
            'MaxGross' : '11'  
        };
        var TraditionalMCAdeal={
            'Payment' :'12345' ,
            'PaymentFrequency' : 'Daily' ,
            'Funder' : 'Lendfoundry' ,
            'ApprovedAmount' :'80000' ,
            'Factor' : '1.2' ,
            'PSF' : '15.00' ,
            'ProcessingFees': '25.00' ,
            'comments' : 'Default Deal' ,
            'Product' : 'MCA Product 1' ,
            'Program' : 'Standard',
            'offerTerm': '6' ,
            'Comp' : '15' ,
            'MaxGross' : '15'  
        };
        var StreamLineLocdeal={
            'Payment' :'3287' ,
            'PaymentFrequency' : 'Weekly' ,
            'Funder' : 'Lendfoundry' ,
            'ApprovedAmount' :'85000' ,
            'Factor' : '1.2' ,
            'PSF' : '15.00' ,
            'ProcessingFees': '25.00' ,
            'comments' : 'Default Deal' ,
            'Product' : 'LOC Product 3' ,
            'Program' : 'Standard',
            'offerTerm': '6' ,
            'Comp' : '15' ,
            'MaxGross' : '15'  
        };
        var PremiumLocdeal={
            'Payment' :'56798' ,
            'PaymentFrequency' : 'Daily' ,
            'Funder' : 'Lendfoundry' ,
            'ApprovedAmount' :'70000' ,
            'Factor' : '1.2' ,
            'PSF' : '15.00' ,
            'ProcessingFees': '25.00' ,
            'comments' : 'Default Deal' ,
            'Product' : 'LOC Product 2' ,
            'Program' : 'Standard',
            'offerTerm': '6' ,
            'Comp' : '15' ,
            'MaxGross' : '15'  
        };
        var PletinumLocdeal={
            'Payment' :'78940' ,
            'PaymentFrequency' : 'Weekly' ,
            'Funder' : 'Lendfoundry' ,
            'ApprovedAmount' :'75000' ,
            'Factor' : '1.6' ,
            'PSF' : '15.00' ,
            'ProcessingFees': '25.00' ,
            'comments' : 'Default Deal' ,
            'Product' : 'LOC Product 1' ,
            'Program' : 'Standard',
            'offerTerm': '6' ,
            'Comp' : '15' ,
            'MaxGross' : '15'   
        };
        try {
            if (productId == null) {
                return {
                    'result': result,
                    'detail': null,
                    'data': Productresult,
                    'rejectcode': '',
                    'exception': ['application details not found']
                };
            }
            if (productId == 'startermca') {
                result = 'Passed';
                Data = {
                    'Offer': StarterMCAOffer,
                    'Deal':StarterMCAdeal,
                    'ProductId': 'mca',
                    'SubProductId': 'MCA Product 2'
                };
            } else if (productId == 'traditionalmca') {
                result = 'Passed';
                Data = {
                    'Offer': TraditionalMCAOffer,
                    'Deal': TraditionalMCAdeal,
                    'ProductId': 'mca',
                    'SubProductId': 'MCA Product 1'
                };
            } else if (productId == 'streamlineloc') {
                result = 'Passed';
                Data = {
                    'Offer': StreamLineLocOffer,
                    'Deal': StreamLineLocdeal,
                    'ProductId': 'mcaloc',
                    'SubProductId': 'LOC Product 3'
                };
            } else if (productId == 'premiumloc') {
                result = 'Passed';
                Data = {
                    'Offer': PremiumLocOffer,
                    'Deal': PremiumLocdeal,
                    'ProductId': 'mcaloc',
                    'SubProductId': 'LOC Product 2'
                };
            } else {
                result = 'Passed';
                Data = {
                    'Offer': PletinumLocOffer,
                    'Deal': PletinumLocdeal,
                    'ProductId': 'mcaloc',
                    'SubProductId': 'LOC Product 1'
                };
            }
            Productresult = {
                'productResult': Data
            };
            return {
                'result': result,
                'detail': null,
                'data': Data,
                'rejectcode': '',
                'exception': []
            };
        } catch (e) {
            return {
                'result': 'Failed',
                'detail': ['Unable to verify' + e.message],
                'data': Productresult,
                'rejectcode': '',
                'exception': [e.message]
            };
        }
    }
}