function verification_rule_merchantverification_datamerch(payload) {
	var result = 'Passed';
	var restrictedCategory = ['Slow Pay', 'Fraudulent', 'Split Payer',
		'Stacking History', 'Default Account', 'Criminal History',
		'Other'];
	var Data = {};
	var RistricCategoryMatch = '';
	var ReferenceNumber = '';
	var Notes = [];
	try {
		if (payload != null && payload != undefined) {
			var dataMerchResponse = payload.datamerchReport[0];

			if (dataMerchResponse != null && dataMerchResponse.MerchantNotes != null) {
				ReferenceNumber = payload.datamerchReport.referenceNumbers;
				Notes = dataMerchResponse.MerchantNotes.Notes;
				for (var i = 0; i < Notes.length; i++) {
					if (restrictedCategory.indexOf(Notes[i].Category) > -1) {
						RistricCategoryMatch = Notes[i].Category;
						result = 'Failed';
						break;
					}
				}
			}
		}
		else {
			result = 'Failed';

		}

		var Data = {
			'RistricCategoryMatch': RistricCategoryMatch,
			'ReferenceNumber': ReferenceNumber
		};

		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': ['Unable to verify'],
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	}
}