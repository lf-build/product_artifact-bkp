function verification_rule_sicverificationmanual(payload) {
	var objData = {};
	try {
		var result = 'Failed';
		if (payload != null && payload != undefined) {

			objData = payload.sicCodeVerificationManualData[0];
			if (objData != null && objData != '' && objData.IsSICVerificationDone == true) {
				result = 'Passed';
			}
		}

		return {
			'result': result,
			'detail': null,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': 'Unable to verify',
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	}
}