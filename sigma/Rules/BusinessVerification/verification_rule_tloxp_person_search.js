function verification_rule_tloxp_person_search(payload) {
    var objData = null;
    try {
        var result = 'Passed';

        if (payload != null && payload != undefined) {

            objData = payload.tloxpPersonReport[0];
            if (objData != null && objData != '') {
                
                if (objData.CreditScore==null||objData.CreditScore==''||objData.CreditScore<5||objData.ActiveFromSos==null||objData.ActiveFromSos!='Active'||objData.ActiveFromSos=='') {
                    result = 'Failed';
                }
            }
        }
        return {
            'result': result,
            'detail': null,
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': ['Unable to verify'],
            'data': objData,
            'rejectcode': '',
            'exception': []
        };
    }
}
