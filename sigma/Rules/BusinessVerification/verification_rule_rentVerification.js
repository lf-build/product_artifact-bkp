function verification_rule_rentVerification(payload) {
    var Name = '';
    var Phone = '';
    var result = 'Failed';
    function VerifyInput(payload) {
        try {
            if (payload != null && payload != undefined) {
                var objData = payload.rentVerificationData[0];
                if (objData != null) {
                    Name = objData.LLName;
                    Phone = objData.LLPhoneNumber;
                    if (Name != null && Phone != null) {
                        result = 'Passed';
                    }
                }
            }
            var Data = {
                'Name': Name,
                'Phone': Phone
            };
            return {
                'result': result,
                'detail': null,
                'data': Data,
                'rejectcode': '',
                'exception': []
            };
        } catch (e) {
            return {
                'result': 'Failed',
                'detail': 'Unable to verify',
                'data': objData,
                'rejectcode': '',
                'exception': []
            };
        }
    }
    return VerifyInput(payload);
}