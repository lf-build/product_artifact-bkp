function verification_rule_applicationdataverification(payload) {
	try {
		var result = 'Failed';
		var objData = null;
		if (payload != null && payload != undefined) {
			objData = payload.eligibility[0];
			if (objData != null) {
				if (objData.LoanAmountResult == true && objData.TimeInBusinessResult == true && objData.State != 'CA') {
					result = 'Passed';
				}
			}
		}
		return { 
			'result': result,
			'detail': null,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': e.message,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	}
}