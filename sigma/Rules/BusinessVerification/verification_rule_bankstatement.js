function verification_rule_bankstatement(payload) {
	var result = 'Failed';
	var AccountNumber = '';
	var RoutingNumber = '';
	var MinRoutingNumberLength = 9;
	try {
		if (payload != null && payload != undefined) {
			var objData = payload.bankVerificationData[0];
			if (objData != null) {
				AccountNumber = objData.accountMatch;
				RoutingNumber = objData.routingMatch;
				if (RoutingNumber.length >= MinRoutingNumberLength) {
					if (AccountNumber != '' && objData.nameMatch == true) {
						result = 'Passed';
					}
				}
			}
		}
		var Data = {
			'AccountNumber': AccountNumber,
			'RoutingNumber': RoutingNumber,
			'NameMatch': objData.nameMatch
		};
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': ['Unable to verify'],
			'data': null,
			'rejectcode': '',
			'exception': []
		};
	}
}