	function verification_rule_ComprehensivePersonalVerification_manual(payload) {
	try {
		var result = 'Failed';
		var objData = null;
		if (payload != null && payload != undefined) {
			objData = payload.comprehensivePersonalVerificationData[0];
			
			if (objData != null) {
				if (objData.finalReview != '') {
					result = 'Passed';
				}
			}						
		}
		return {
			'result': result,
			'detail': null,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': e.message,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	}
}