function verification_rule_fundingcallverification(payload) {
	var result = 'Failed';
	var Data = {};
	if (payload != null && payload != undefined) {
		Data = payload.fundingCallVerificationData[0];
		if (Data != null && Data.IsFundingCallDone == true) {
			result = 'Passed';
		}
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}