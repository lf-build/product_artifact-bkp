function verification_rule_businessidentityipaddressverification_whitepages(payload) {
    try {
        var result = 'Failed';
        var countryCode = null;
        if (payload != null && payload != undefined) {
            var objData = payload.whitepagesIdentityCheck[0];
            if (objData != null) {
                countryCode = objData.GeolocationCountryCode;
                if (countryCode.toLowerCase() == 'us') {
                    result = 'Passed';
                }
            }
        }
        var Data = {
            'Ip CountryCode': countryCode
        };
        return {
            'result': result,
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': 'Unable to verify',
            'data': null,
            'rejectcode': '',
            'exception': e.message
        };
    }
}