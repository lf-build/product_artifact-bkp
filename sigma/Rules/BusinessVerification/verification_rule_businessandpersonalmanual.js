function verification_rule_businessandpersonalmanual(payload) {
    if (payload != null) {
        var objData = payload.ComprehensiveBusinessandPersonalVerificationsManualData[0];
        var result = 'Passed';
        if (objData.IsFundindDone1 == false) {
            result = 'Failed';
        }
        return {
            'result': result,
            'detail': null,
            'data': null,
            'rejectcode': '',
            'exception': []
        };
    }
}