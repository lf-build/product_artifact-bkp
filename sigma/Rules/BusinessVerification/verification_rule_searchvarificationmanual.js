function verification_rule_searchvarificationmanual(payload) {
	var result = 'Failed';
	var Data = {};
	if (payload != null && payload != undefined) {
		Data = payload.searchVerificatioManualData;
		if (Data != null && Data.searchMatch == true) {
			result = 'Passed';
		}
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
