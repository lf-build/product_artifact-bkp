function verification_rule_sicverification(payload) {
	var self = this;
	var result = 'Passed';
	var sic1 = null;
	var sic2 = null;
	var sic1Matches = false;
	var sic2Matches = false;
	var lookupResult = {};
	var sicMatches = '';
	var lookupService = self.call('lookup');
	try {
		if (payload != null && payload != undefined) {
			var objData = payload.bizApiSearchReport[0];
			if (objData != null && objData != '') {
				sic1 = objData.FourDigitSIC1;
				sic2 = objData.FourDigitSIC2;
			}
			return lookupService.get('prohibitedSIC').then(function (response) {
				lookupResult = response;
				if (lookupResult != null && lookupResult != undefined) {
					for (var key in lookupResult) {
						if (sic1 != null && sic1 != '' && sic1 == lookupResult[key]) {
							sicMatches = key;
							sic1Matches = true;
							result = 'Failed';
						}
						if (sic2 != null && sic2 != '' && sic2 == lookupResult[key]) {
							sicMatches = key;
							sic2Matches = true;
							result = 'Failed';
						}
					};
				}
				var Data = {
					'sicMatches': sicMatches,
					'sic1Matches': sic1Matches,
					'sic2Matches': sic2Matches
				};
				return {
					'result': result,
					'detail': null,
					'data': Data,
					'rejectcode': '',
					'exception': []
				};
			});
		}
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': ['Unable to verify'],
			'data': null,
			'rejectcode': '',
			'exception': []
		};
	}
}