function ComputeSummary(payload) {
	var self = this;
	var account = payload.account;
	var startDate = payload.startDate;
	var endDate = payload.endDate;
	var MonthlyCashFlowsList = [];
	MonthlyCashFlowsList = payload.monthlyCashFlows;
	var cashflowService = self.call('cashflowibv');
	var objSummary = cashflowService.GetPlaidTransactionSummaryViewModel();
	objSummary.CountOfMonthlyStatement = MonthlyCashFlowsList.length;
	objSummary.StartDate = cashflowService.GetFormattedDate(startDate);
	objSummary.EndDate = cashflowService.GetFormattedDate(endDate);
	var AGS_Month = 3;
	var Last_N_Month = cashflowService.GetLastCalendarMonth(AGS_Month);
	var AGSDeposit_N_Month = 0;
	var TotalBrokerAGSDepositArray = new Array();
	var Plaidmonths = cashflowService.getPlaidMonths();
	var TotalRevenueAmount = 0,
		TotalRevenueCount = 0;
	var TotalDepositAmount = 0,
		TotalWithdrawalAmount = 0;
	var TotalDailyAverageBalance = 0;
	var TotalPayrollAmount = 0,
		TotalLoanPaymentAmount = 0,
		TotalNSFAmount = 0;
	var TotalPayrollCount = 0,
		TotalLoanPaymentCount = 0,
		TotalNSFCount = 0;
	var TotalDebitsCount = 0,
		TotalCreditsCount = 0;
	var TotalMonthlyIncomeArray = new Array();
	var TotalDailyBalanceArray = new Array();
	var TotalDailyDepositArray = new Array();
	var TotalMonthlyNegativeBalanceCount = 0;

	var TodaysDate = new Date();
	var MonthNames = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
	var CurrentMonthName = MonthNames[TodaysDate.getMonth()];
	objSummary.PDFReportName = account.Accountnumber + '-' + TodaysDate.getDate() + CurrentMonthName + TodaysDate.getFullYear() + '.pdf';
	MonthlyCashFlowsList.forEach(function (cf) {
		TotalRevenueAmount += cf.TotalMonthlyRevenueAmount;
		TotalRevenueCount += cf.TotalMonthlyRevenueCount;
		TotalDepositAmount += cf.TotalDepositAmount;
		TotalWithdrawalAmount += cf.TotalWithdrawalAmount;
		TotalDailyAverageBalance += cf.AverageDailyBalance;
		TotalPayrollAmount += cf.PayrollAmount;
		TotalLoanPaymentAmount += cf.LoanPaymentAmount;
		TotalNSFAmount += cf.NSFAmount;
		TotalPayrollCount += cf.NumberOfPayroll;
		TotalLoanPaymentCount += cf.NumberOfLoanPayment;
		TotalNSFCount += cf.NumberOfNSF;
		TotalCreditsCount += cf.DepositCount;
		TotalDebitsCount += cf.WithdrawalCount;
		TotalMonthlyIncomeArray.push(cf.TotalDepositAmount);
		if (cf.MonthlyTotalDailyBalanceArray != null && cf.MonthlyTotalDailyBalanceArray.length > 0) {
			cf.MonthlyTotalDailyBalanceArray.forEach(function (data) {
				TotalDailyBalanceArray.push(parseFloat(data) || 0);
			});
		}
		if (cf.MonthlyTotalDailyDepositArray != null && cf.MonthlyTotalDailyDepositArray.length > 0) {
			cf.MonthlyTotalDailyDepositArray.forEach(function (data) {
				TotalDailyDepositArray.push(parseFloat(data) || 0);
			});
		}
		TotalMonthlyNegativeBalanceCount = TotalMonthlyNegativeBalanceCount + cf.NumberOfNegativeBalance;
		if (TotalBrokerAGSDepositArray.length <= AGS_Month) {
			var tDateAGS = Plaidmonths.indexOf(cf.Name) + '-' + cf.Year;
			if (Last_N_Month.indexOf(tDateAGS) > -1) {
				TotalBrokerAGSDepositArray.push(cf.MonthlyTotalDailyDepositArray);
			}
		}
	});
	objSummary.TotalRevenueAmount = cashflowService.PlaidcustomRound((Math.abs(TotalRevenueAmount)), 2);
	objSummary.TotalRevenueCount = TotalRevenueCount;
	objSummary.AverageMonthlyRevenue = cashflowService.PlaidcustomRound((TotalRevenueAmount / MonthlyCashFlowsList.length), 2);
	objSummary.MedianMonthlyIncome = cashflowService.GetMedianSingleArray(TotalMonthlyIncomeArray);
	objSummary.MedianDailyBalance = cashflowService.GetMedianSingleArray(TotalDailyBalanceArray);
	objSummary.MaxDaysBelow100Count = cashflowService.GetMaxDaysBelow100(MonthlyCashFlowsList);
	objSummary.CVOfDailyBalance = cashflowService.PlaidcustomRound(cashflowService.DailyCoEfficient(TotalDailyBalanceArray), 2);
	objSummary.CVOfDailyDeposit = cashflowService.PlaidcustomRound(cashflowService.DailyCoEfficient(TotalDailyDepositArray), 2);
	objSummary.TotalCredits = TotalDepositAmount;
	objSummary.TotalDebits = TotalWithdrawalAmount;
	objSummary.TotalCreditsCount = TotalCreditsCount;
	objSummary.TotalDebitsCount = TotalDebitsCount;
	objSummary.AvailableBalance = account.AvailableBalance;
	objSummary.CurrentBalance = account.CurrentBalance;
	if (MonthlyCashFlowsList != null && MonthlyCashFlowsList.length > 0 && MonthlyCashFlowsList[0].AverageDailyBalance != undefined && MonthlyCashFlowsList[0].AverageDailyBalance != null) {
		objSummary.AverageBalanceLastMonth = MonthlyCashFlowsList[0].AverageDailyBalance;
	}
	objSummary.AverageDeposit = cashflowService.PlaidcustomRound((TotalDepositAmount / MonthlyCashFlowsList.length), 2);
	objSummary.AnnualCalculatedRevenue = cashflowService.PlaidcustomRound((TotalDepositAmount / MonthlyCashFlowsList.length) * 12, 2);
	objSummary.AverageWithdrawal = cashflowService.PlaidcustomRound((TotalWithdrawalAmount / MonthlyCashFlowsList.length), 2);
	objSummary.AverageDailyBalance = cashflowService.PlaidcustomRound((TotalDailyAverageBalance / MonthlyCashFlowsList.length), 2);
	objSummary.NumberOfNegativeBalance = TotalMonthlyNegativeBalanceCount;
	objSummary.NumberOfNSF = TotalNSFCount;
	objSummary.NSFAmount = cashflowService.PlaidcustomRound(TotalNSFAmount, 2);
	objSummary.LoanPaymentAmount = cashflowService.PlaidcustomRound((TotalLoanPaymentAmount), 2);
	objSummary.NumberOfLoanPayment = TotalLoanPaymentCount;
	objSummary.NumberOfPayroll = TotalPayrollCount;
	objSummary.PayrollAmount = cashflowService.PlaidcustomRound((Math.abs(TotalPayrollAmount)), 2);

	if (TotalBrokerAGSDepositArray != null && TotalBrokerAGSDepositArray.length > 0) {
		var tempTotalBrokerAGSDepositArray = [];
		TotalBrokerAGSDepositArray.forEach(function (deposits) {
			if (deposits != null) {
				deposits.forEach(function (amt) {
					tempTotalBrokerAGSDepositArray.push(amt);
				});
			}
		});
		AGSDeposit_N_Month = cashflowService.AGSSum(tempTotalBrokerAGSDepositArray);
		var Broker_AverageDepositVolume = AGSDeposit_N_Month / AGS_Month;
		objSummary.BrokerAGS = cashflowService.PlaidcustomRound(Broker_AverageDepositVolume * 12, 2);
	}
	if (TotalDailyDepositArray != null && TotalDailyDepositArray.length > 0) {
		var CAP_AverageDepositVolume = cashflowService.AGSSum(TotalDailyDepositArray) / MonthlyCashFlowsList.length;
		objSummary.CAPAGS = cashflowService.PlaidcustomRound(CAP_AverageDepositVolume * 12, 2);
	}
	return objSummary;
}
