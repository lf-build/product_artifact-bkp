function monthlySchedule(payload) {
	var loanSchedule = [];
	var payOrders = {};
	if (payload.eventData) {
		var term = payload.eventData.term;
		var paymentOrder = payload.eventData.frequency == 'Weekly' ? 4 : 23;
		var paybackAmount = payload.eventData.loanAmount;
		var lowerOrder = payload.eventData.loanSchedule[0];
		var loanScheduleData = payload.eventData.loanSchedule;
		var loanScheduleCount = parseInt(loanScheduleData.length / paymentOrder);
		var higherOrder = payload.eventData.loanSchedule[payload.eventData.loanSchedule.length - 1];
		var temp = loanScheduleData;
		for (var i = 0; i < term - 1; i++) {
			var principle = 0;
			var payment = 0;
			var finance = 0;
			var items = temp.slice(0, paymentOrder);
			temp.splice(0, paymentOrder);
			for (j = 0; j < items.length; j++) {
				principle = items[j].PrincipalAmount + principle;
				payment = items[j].PaymentAmount + payment;
				finance = items[j].InterestAmount + finance;
			}
			paybackAmount = paybackAmount + finance;
			var obj = {
				'PrincipalAmount': principle,
				'PaymentAmount': payment,
				'InterestAmount': finance,
				'PaybackAmount': paybackAmount
			};
			loanSchedule.push(obj);
		}
		if (temp.length > 0) {
			var principle = 0;
			var payment = 0;
			var finance = 0;
			for (var j = 0; j < temp.length; j++) {
				principle = temp[j].PrincipalAmount + principle;
				payment = temp[j].PaymentAmount + payment;
				finance = temp[j].InterestAmount + finance;
			}
			paybackAmount = paybackAmount + finance;
			var obj = {
				'PrincipalAmount': principle,
				'PaymentAmount': payment,
				'InterestAmount': finance,
				'PaybackAmount': paybackAmount
			};
			loanSchedule.push(obj);
		}
	}
	var higherPay = loanSchedule[0].PaymentAmount + loanSchedule[1].PaymentAmount;
	var lowerPay = loanSchedule[2].PaymentAmount + loanSchedule[3].PaymentAmount + loanSchedule[4].PaymentAmount + loanSchedule[5].PaymentAmount;
	payOrders.lowerPay = lowerPay;
	payOrders.higherPay = higherPay;
	var Data = {
		'loanSchedule': loanSchedule,
		'payOrders': payOrders
	};
	return {
		'result': 'Passed',
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}