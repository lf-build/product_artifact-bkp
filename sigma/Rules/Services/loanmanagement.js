function loanmanagement() {
    var self = this;
    var baseUrl = 'http://{{internal_ip}}:7092';

   
return {
    loanDetails: function (loanNumber) {
        var url = [baseUrl,loanNumber].join('/');
        return new Promise(function (resolve, reject) {
            try {
                self.http.get(url).catch(function (error) {
                    reject('loan service failed for1' + loanNumber + ':' + loanNumber);
                })
                    .then(function (response) {
                        resolve(response);
                    });
            } catch (exception) {
                reject('loan service failed for2 ' + loanNumber + ':' + loanNumber + ":" + exception);
            }
        });
    }
};
}