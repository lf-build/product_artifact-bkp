function cashflow() {
    var self = this;
    var baseUrl = 'http://cashflow:5000';
    return {
        getAllAccounts: function(entityType, entityId) {
            var url = [baseUrl, entityType, entityId, 'accounts', 'all'].join('/');
            return self.http.get(url);
        },
        getAllTransactions: function(entityType, entityId, accountId) {
            var url = [baseUrl, entityType, entityId, 'transaction', accountId].join('/');
            return self.http.get(url);
        },
        getTransactions: function(entityType, entityId, accountId) {
            var url = [baseUrl, entityType, entityId, 'transaction', accountId].join('/');
            return self.http.get(url);
        }
    };
}