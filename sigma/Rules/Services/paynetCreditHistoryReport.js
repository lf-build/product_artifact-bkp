function paynetCreditHistoryReport(entityType, entityId) {
    var self = this;
    var paynetService = self.call('paynetService');
    var eventHub = self.call('eventHub');
    var applicationService = self.call('application');
   

    return applicationService.getByApplicationNumber(entityId).then(function (applicationData) {
		return applicationData;
        var applicant = applicationData.applicant;
        var primaryAddress = applicationData.application.primaryAddress;

        return paynetService.search(entityType, entityId, applicant.businessTaxID, applicant.legalBusinessName, primaryAddress.addressLine1, primaryAddress.city, primaryAddress.state, primaryAddress.zipCode)
            .then(function (result) {
                var selectedCompany;
                if (result == null || result.companyList == null || result.companyList.length <= 0) {
                    result = {
                        'result': 'CompanyNotFound'
                    };
                    return Promise.resolve(result);
                }
                self.companyList = [];
                result.companyList.forEach(function (company) {
                    if (company.matchScore > 60) {
                        self.companyList.push(company);
                    }
                }, this);

                if (self.companyList.length <= 0) {
                    result = {
                        'result': 'Declined'
                    };
                    return Promise.resolve(result);
                } else {
                    self.positiveCompanyList = [];
                    self.reviewCompanyList = [];

                    self.companyList.forEach(function (company) {
                        if (company.matchScore > 90) {
                            self.positiveCompanyList.push(company);
                        } else if (company.matchScore >= 60 || company.matchScore <= 90) {
                            self.reviewCompanyList.push(company);
                        }
                    }, this);

                    if (self.positiveCompanyList.length == 1) {
                        selectedCompany = self.positiveCompanyList[0];
                    } else if (self.positiveCompanyList.length > 1) {
                        result = {
                            'result': 'ManualReview'
                        };
                        return Promise.resolve(result);

                    } else if (self.reviewCompanyList.length > 0) {
                        result = {
                            'result': 'ManualReview'
                        };
                        return Promise.resolve(result);
                        //Promise.reject('ManualReview');
                        //return;
                    } else {
                        result = {
                            'result': 'Declined'
                        };
                        return Promise.resolve(result);
                    }
                }

                // TODO:
                // If one company score > 90 - select
                // if more than one company Score>90 - manual
                // If any company company one or more between 60-90 then manual
                // If no company above 60 then no match (declined)
                //var company = result.companyList[0];

                return paynetService.getReport(entityType, entityId, selectedCompany.payNetId).catch(function () {
                    return paynetService.getReport(entityType, entityId, selectedCompany.payNetId);
                });
            })
            .then(function (result) {
                return Promise.all([eventHub.publish("SyndicationCalledEvent", {
                        'EntityType': entityType,
                        'ReferenceNumber': "123",
                        'EntityId': entityId,
                        'Data': result,
                        'Name': "PaynetCreditHistoryReport",
                    },

                    eventHub.publish("PaynetCreditHistoryReportCalled", {
                        'EntityType': entityType,
                        'ReferenceNumber': "",
                        'EntityId': entityId,
                        'Response': result
                    })), ]).then(function () {
                    return result;

                });
            });
    });

}