function dataAttribute() {
    var self = this;
    var baseUrl = 'http://{{internal_ip}}:7012';
    return {
        get: function(entityType, entityId, name) {
            var url = [baseUrl, entityType, entityId, name].join('/');
            return self.http.get(url);
        },
        getMultipleAttributes: function(entityType, entityId, name) {
            var url = [baseUrl, entityType, entityId, 'names', name].join('/');
            return self.http.get(url);
        },
        set: function(entityType, entityId, name, value) {
            var url = [baseUrl, entityType, entityId, name].join('/');
            return self.http.post(url, value);
        },
        set: function(entityType, entityId, name, secondaryName, value) {
            var url = [baseUrl, entityType, entityId, name, secondaryName].join('/');
            return self.http.post(url, value);
        },
        getAllAttributes: function(entityType, entityId) {
            var url = [baseUrl, entityType, entityId].join('/');
            return self.http.get(url);
        },
        setAttributes: function(entityType, entityId, attributes) {
            var url = [baseUrl, entityType, entityId].join('/');
            return self.http.post(url, attributes);
        },
        isAttributeValueExist: function(entityType, entityId, name) {
            var dataAttributeService = this;
            return new Promise(function(resolve, reject) {
                try {
                    dataAttributeService.get(entityType, entityId, name).catch(function(error) {
                            resolve(false);
                            return;
                        })
                        .then(function(response) {
                            resolve(response != null);
                        });
                } catch (exception) {
                    resolve(false);
                    return;
                }
            });
        }
    };
}