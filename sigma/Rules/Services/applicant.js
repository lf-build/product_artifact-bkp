function applicant() {
    var self = this;
    var baseUrl = 'http://business-applicant:5000';

    return {
        get: function (applicantId) {
            var url = [baseUrl, applicantId].join('/');
            return self.http.get(url);
        },
        associateUser:function(applicantId,userId){
            var url = [baseUrl, applicantId,'associate','user',userId].join('/');
            return self.http.put(url);
        }
    };
}