function ExtractPlaidData(input) {
	var errorData = [];
	try {
		var lstAccounts = [];
		var lstTransactions = [];
		var data = {};
		if (input != null) {
			var response = input.Response;
			var SelectedAccountId = response.SelectedAccountId;
			console.log(SelectedAccountId);
			if (response.Accounts != null && response.Accounts.length > 0) {
				console.log('Accounts Loggs' + response.Accounts.length);
				response.Accounts.forEach(function (account) {
					var objAccount = {
						'ProviderAccountId': '',
						'AvailableBalance': 0,
						'CurrentBalance': 0,
						'BankName': '',
						'AccountType': '',
						'AccountNumber': '',
						'Source': '',
						'IsCashflowAccount': false
					};
					objAccount.Source = 'Plaid';
					objAccount.ProviderAccountId = account.Id;
					if (account.Id == SelectedAccountId) {
						objAccount.IsCashflowAccount = true;
					}
					objAccount.BankName = account.InstitutionType;
					objAccount.AccountType = account.Type;
					if (account.AccountMeta != null) {
						objAccount.AccountNumber = account.AccountMeta.Number;
					}
					if (account.Balance != null) {
						objAccount.AvailableBalance = account.Balance.Available;
						objAccount.CurrentBalance = account.Balance.Current;
					}
					lstAccounts.push(objAccount);
				});
			}
			if (response.Transactions != null && response.Transactions.length > 0) {
				console.log(response.Transactions);
				response.Transactions.forEach(function (transaction) {
					var objTransaction = {
						'ProviderAccountId': '',
						'AccountId': '',
						'Amount': 0,
						'Categories': '',
						'CategoryId': '',
						'Description': '',
						'Meta': '',
						'Pending': '',
						'TransactionDate': ''
					};
					objTransaction.ProviderAccountId = transaction.Account;
					objTransaction.Amount = transaction.Amount;
					objTransaction.TransactionDate = transaction.Date;
					objTransaction.Description = transaction.Name;
					objTransaction.Pending = transaction.Pending;
					objTransaction.Categories = transaction.Category;
					objTransaction.CategoryId = transaction.CategoryId;
					lstTransactions.push(objTransaction);
				});
			}
			data = {
				Accounts: lstAccounts,
				Transactions: lstTransactions
			};
		}
	} catch (e) {
		return 'Unable to execute rule' + e;
	}
	return data;
}