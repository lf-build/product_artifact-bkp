function extractFinicityTransactions(input) {
    function ConvertEPOCDate(d) {
        var date = new Date(d * 1000);
        date = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
        return date;
    }
    if (input != null) {
        var cashflowResponse = input.Response;
        var lstTransactions = [];
        if (cashflowResponse != null && cashflowResponse.length > 0) {
            cashflowResponse.forEach(function(transaction) {
                var Categories = [];
                var objTransaction = {
                    'ProviderAccountId': '',
                    'AccountId': '',
                    'Amount': 0,
                    'Categories': '',
                    'CategoryId': '',
                    'Description': '',
                    'Meta': '',
                    'Pending': 'False',
                    'TransactionDate': ''
                };
                objTransaction.ProviderAccountId = transaction.accountId;
                objTransaction.Amount = transaction.amount;
                objTransaction.TransactionDate = ConvertEPOCDate(transaction.transactionDate);
                objTransaction.Description = transaction.description;
                objTransaction.CategoryId = transaction.categorization.category;
                Categories.push(transaction.categorization.scheduleC);
                if (Categories != null && Categories.length > 0) {
                    objTransaction.Categories = Categories;
                }
                if (transaction.status == 'pending') {
                    objTransaction.Pending = 'True';
                }
                lstTransactions.push(objTransaction);
            });
            return lstTransactions;
        }
    }
    return null;
}