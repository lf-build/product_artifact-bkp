function LoanScheduleEmailData(payload) {
	 try {
	if (payload != null) {
		var eventDetails = payload.eventData.EventData;
		var emailId =eventDetails.LoanResponse.PrimaryApplicantDetails.Emails != null ?eventDetails.LoanResponse.PrimaryApplicantDetails.Emails[0].EmailAddress : '';
		var name = jsUcfirst(eventDetails.LoanResponse.PrimaryApplicantDetails.Name.First + ' ' + eventDetails.LoanResponse.PrimaryApplicantDetails.Name.Last);		
		var businessName = eventDetails.LoanResponse.PrimaryBusinessDetails != null ? eventDetails.LoanResponse.PrimaryBusinessDetails.BusinessName : '';
		var scheduleDate = eventDetails.ScheduleResponse != null && eventDetails.ScheduleResponse.ScheduleVersionDate != null ? eventDetails.ScheduleResponse.ScheduleVersionDate.Time : null;
		var reason = eventDetails.ScheduleResponse.ScheduledCreatedReason;
		var currentBalance = eventDetails.AccountingResponse != null && eventDetails.AccountingResponse.PBOT != null ? eventDetails.AccountingResponse.PBOT.TotalPrincipalOutStanding + eventDetails.AccountingResponse.PBOT.TotalInterestOutstanding:0; 
		var newDrawDownAmount = eventDetails.ScheduleResponse != null ?  eventDetails.ScheduleResponse.LoanAmount :0;
		var totalBalanceDue = currentBalance + newDrawDownAmount;
		var scheduleList = eventDetails.ScheduleResponse != null ? eventDetails.ScheduleResponse.ScheduleDetails : [];
		var payoffSchedule = eventDetails.ScheduleResponse != null ? eventDetails.ScheduleResponse : [];
		var todayDate = new Date();		
        var date = todayDate.getFullYear() + '-' + todayDate.getMonth() + 1 + '-' + todayDate.getDate();
		var loanDue = eventDetails.AccountingResponse != null? eventDetails.AccountingResponse.PBOT.TotalPrincipalOutStanding +eventDetails.AccountingResponse.PBOT.TotalInterestOutstanding+eventDetails.AccountingResponse.PBOT.TotalFeeOutstanding  :0;
		var payoffAmount = eventDetails.AccountingResponse != null? (eventDetails.AccountingResponse.PayOff != null ?eventDetails.AccountingResponse.PayOff.PayOffAmount:0) :0;
		var payoffDate = eventDetails.AccountingResponse != null? (eventDetails.AccountingResponse.PayOff != null ?eventDetails.AccountingResponse.PayOff.PayOffDate :'') :'';
		
		var activationDate = eventDetails.ScheduleResponse != null && eventDetails.ScheduleResponse.FundedDate != null ? eventDetails.ScheduleResponse.FundedDate.Time : null;
		var modReason = eventDetails.ScheduleResponse != null && eventDetails.ScheduleResponse.ModReason != null ? eventDetails.ScheduleResponse.ModReason : '';
		var totalPrincipalOutstanding = eventDetails.AccountingResponse != null && eventDetails.AccountingResponse.PBOT != null ? eventDetails.AccountingResponse.PBOT.TotalPrincipalOutStanding : 0;
		var totalInterestOutstanding = eventDetails.AccountingResponse != null && eventDetails.AccountingResponse.PBOT != null ? eventDetails.AccountingResponse.PBOT.TotalInterestOutstanding : 0;
		var noOfPayments = eventDetails.ScheduleResponse != null ? eventDetails.ScheduleResponse.Tenure : 0;
		var interestRate = 
		eventDetails.LoanResponse != null 
			? (eventDetails.LoanResponse.ProductPortfolioType == 'MCA' || eventDetails.LoanResponse.ProductPortfolioType == 'MCALOC' 
				? (eventDetails.ScheduleResponse != null 
					? eventDetails.ScheduleResponse.FactorRate 
					: 0)
				: (eventDetails.ScheduleResponse != null 
					? eventDetails.ScheduleResponse.AnnualRate
					: 0))
			: 0;
		var remainderAmount = eventDetails.ScheduleResponse != null && eventDetails.ScheduleResponse.RemainderDetails != null ? eventDetails.ScheduleResponse.RemainderDetails.RemainderAmount : 0;
		var labelForRate = eventDetails.LoanResponse != null ? ((eventDetails.LoanResponse.ProductPortfolioType == 'MCA' || eventDetails.LoanResponse.ProductPortfolioType == 'MCALOC' ) ? 'Factor' : 'Interest Rate') :  'Interest Rate';
		var labelForRateAmount = eventDetails.LoanResponse != null ? ((eventDetails.LoanResponse.ProductPortfolioType == 'MCA' || eventDetails.LoanResponse.ProductPortfolioType == 'MCALOC' ) ? 'Finance Charge' : 'Interest') :  'Interest';
		
		var result = {
			Email: emailId,
			Name : name,
			LoanNumber: payload.entityId,
			BusinessName: businessName,
			ScheduleDate: scheduleDate,
			Reason: reason,
			CurrentBalance: totalBalanceDue - newDrawDownAmount,
			NewDrawDownAmount: newDrawDownAmount,
			TotalBalanceDue: totalBalanceDue,
			ScheduleList: scheduleList,
			PayoffSchedule: payoffSchedule,
			TodayDate: date,
			LoanDue: loanDue,
			PayoffAmount: payoffAmount,
			PaidoffDate: payoffDate,
			ActivationDate: activationDate,
			ModReason: modReason,
			TotalPrincipalOutstanding: totalPrincipalOutstanding,
			TotalInterestOutstanding: totalInterestOutstanding,
			NoOfPayments: noOfPayments,
			InterestRate: interestRate,
			RemainderAmount: remainderAmount,
			LabelForRate :labelForRate ,
			LabelForRateAmount :labelForRateAmount
		};
		return {
				'result': 'Passed',
				'detail': null,
				'data': result,
				'rejectcode': '',
				'exception': []
			};

	}
	var errorData = [];
	errorData.push('Data Not found for email');
	return {

		'result' : 'Failed',
		'detail' : null,
		'data' : '',
		'rejectcode' : '',
		'exception': errorData
	};
	
	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	};
	 }
    catch (e) {
        return {
            'result': false,
            'detail': null,
            'data': null,
            'exception': [e.message],
            'rejectCode': ''
        };
    }
}
