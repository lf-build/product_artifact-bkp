function LoanNotificationEmailData(payload) {
	var self = this;
	if (payload) {
		var eventDetails = payload.eventData.EventData;
		var loanNo = eventDetails.LoanNumber;
		var bankName='';
		var bankService =self.call('loanbankdetails');
		var applicationService = self.call('loanmanagement');
		try {
			return applicationService.loanDetails(loanNo).then(function (data) {
				bankService.loanDetails(loanNo).then(function (data) { bankName=data.name;  });
				var url = '{{server_url}}:{{borrower_port}}/login';
				var queryStringData = '?token=';
				var result = {
					Email:data.primaryApplicantDetails.emails[0].emailAddress,
					Name: data.primaryApplicantDetails.name.first,
					ContactName: data.primaryApplicantDetails.name.first,
					LoanNumber: data.loanNumber,
                    LegalBusinessName: data.primaryBusinessDetails.businessName,
                    PaymentDate:eventDetails.PaymentInfo.LastPaymentReceivedDate,
					PaymentAmount:eventDetails.PaymentInfo.LastPaymentAmount,
					numberofdays:eventDetails.Due.DPDDays,
					Nameofbank:bankName,
					ScheduleDate:eventDetails.ScheduleDate,
					AmountDue:eventDetails.InstallmentAmount,
                    DateOfPaymentDue:eventDetails.ProcessingDate,
					DateofMissedpayment:eventDetails.ProcessingDate,
					PaymentDate:eventDetails.PaymentInfo.LastPaymentReceivedDate,
                    FeeAmount:eventDetails.FeeAmount,
                    FeeName:eventDetails.FeeName,
					Logo: '{{Logo_url}}',
					url: url,
					queryString: queryStringData				
				};
				return {
					result: 'Passed',
					detail: null,
					data: result,
					rejectcode: '',
					exception: []
				};
			});
		} catch (e) {
			var errorData = [];
			errorData.push(e);
			return {
				result: 'Failed',
				detail: null,
				data: '',
				rejectcode: '',
				exception: errorData
			};
		}
	} else {
		var errorData = [];
		return {
			result: 'Failed',
			detail: null,
			data: '',
			rejectcode: '',
			exception: errorData
		};
	}
}