function CalculateTierBasedFee(payload) {
	if(payload == null || payload.LoanSchedule == null){
		return 0;
	}
	var lineAmount = payload.LoanSchedule.CreditLimit;
	if(lineAmount >= 10000 && lineAmount <=25000){
		return 100;
	}
	else if(lineAmount >= 26000 && lineAmount <=50000){
		return 250;
	}
	else if(lineAmount >= 51000 && lineAmount <=100000){
		return 350;
	}
	else if(lineAmount >= 101000){
		return 400;
	}
	return 0;
}