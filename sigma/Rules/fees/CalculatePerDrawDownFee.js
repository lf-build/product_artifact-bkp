function CalculatePerDrawDownFee(payload) {

	if (payload == null || payload.eventData.drawDown == null || payload.eventData.selectedDealoffer == null) {
		return {
			'result': 'Failed',
			'detail': null,
			'data': null,
			'rejectcode': '',
			'exception': ['payload Data not fount']
		};
	}

	var drawDownFee = payload.eventData.drawDown.DrawDownFeeAmount;
	var drawdownAmount = payload.eventData.drawDown.ApprovedAmount;
	var feeAmount = 0;
	feeAmount = drawDownFee;
	var Data = {
		'feeAmount': feeAmount
	};

	return {
		'result': 'Passed',
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};

}
