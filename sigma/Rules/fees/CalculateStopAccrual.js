function CalculateStopAccrual(payload) {
	if (payload == null || payload.EventData == null) {
		return false;
	}
	if(payload.EventData.Due != null && payload.EventData.Due.DPDDays >= 60){
		return true;
	}
	return false;
}