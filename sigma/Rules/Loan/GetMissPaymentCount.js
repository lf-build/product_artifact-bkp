function GetMissPaymentCount(payload) {

    var self = this;
    var loanFilters = self.call('LoanMatrics');
    if (payload != null && payload.Loan != null) {
        var loanNumber = payload.Loan.LoanNumber;
        return loanFilters.getLoanMatrics(loanNumber).then(function (result) {        
            if(result != null)
            {
                for (var i = 0; i < result.metrics.length; i++) {
                    if(result.metrics[i].name == 'SuccessivePaymentMissed'){                       
                            return result.metrics[i].value;
                        }
                    }
            }
            return 0; 
        }).catch(function () {
            return 0;
        })
    }
return 0;
}