function data_attribute_whitepages_identitycheck(payload) {
    var result = 'Passed';
    if (payload == null) {
        return 'null';
    }
    var primaryAddressChecks = null;
    var primaryPhoneChecks = null;
    var ipAddressChecks = null;
    var geolocation = null;
    var emailAddressChecks = null;
    var primaryPhoneIsValid = '';
    var phoneToName = '';
    var phoneToAddress = '';
    var phoneCountryCode = '';
    var phoneIsCommercial = '';
    var phoneLineType = '';
    var phoneIsPrepaid = '';
    var primaryAddressIsValid = '';
    var addressToName = '';
    var addressIsCommercial = '';
    var emailIsValid = '';
    var emailToName = '';
    var isDisposable = '';
    var emailFirstSeenDate = '';
    var emailFirstSeenDays = '';
    var geolocationPostalCode = '';
    var geolocationCityName = '';
    var geolocationCountryCode = '';
    var geolocationCountryName = '';
    var geolocationContinentCode = '';
    var distanceFromAddress = '';
    var name = '';
    var phone = '';
    var email = '';
    var countryName = '';
    var city = '';
    var postalCode = '';
    var stateCode = '';
    var streetLine1 = '';
    var streetLine2 = '';
    var identityCheckScore = '';
    var ipIsValid = '';
    var ipIsProxy = '';
    var EmailFirstSeen = false;
    var Distance = false;
    var PhoneLine = true;
    var PhoneMatch = false;
    var AddressMatch = false;
    var EmailMatch = false;
    var EmailNeverSeenBefore = false;
    var PhoneNameMatch = false;
    var MatchResponse = 'match';
    var linetypes = ['non-fixed voip', 'toll free'];
	var CountryCodeIsValid = false;
    var response = payload.eventData.Response;
    if (response != null) {
        primaryAddressChecks = response.PrimaryAddressChecks;
        primaryPhoneChecks = response.PrimaryPhoneChecks;
        ipAddressChecks = response.IpAddressChecks;
        emailAddressChecks = response.EmailAddressChecks;
        identityCheckScore = response.IdentityCheckScore;
        if (response.Request != null) {
            name = response.Request.Name;
            phone = response.Request.PrimaryPhone;
            email = response.Request.EmailAddress;
            postalCode = response.Request.PrimaryPostalCode;
            stateCode = response.Request.PrimaryStateCode;
            streetLine1 = response.Request.PrimaryStreetLine_1;
            streetLine2 = response.Request.PrimaryStreetLine_2;
            countryName = response.Request.PprimaryCountryCode;
            city = response.Request.PrimaryCity;
        }
        if (primaryAddressChecks != null) {
            primaryAddressIsValid = primaryAddressChecks.IsValid;
            addressToName = primaryAddressChecks.AddressToName;
            addressIsCommercial = primaryAddressChecks.IsCommercial;
            if (addressToName.toLowerCase() == MatchResponse) {
                AddressMatch = true;
            }
        }
        if (primaryPhoneChecks != null) {
            primaryPhoneIsValid = primaryPhoneChecks.IsValid;
            phoneToName = primaryPhoneChecks.PhoneToName;
            phoneToAddress = primaryPhoneChecks.PhoneToAddress;
            phoneCountryCode = primaryPhoneChecks.CountryCode;
            phoneIsCommercial = primaryPhoneChecks.IsCommercial;
            phoneLineType = primaryPhoneChecks.LineType;
            phoneIsPrepaid = primaryPhoneChecks.IsPrepaid;
            if (linetypes.indexOf(phoneLineType.toLowerCase()) > -1) {
                PhoneLine = false;
            }
            if (phoneToName.toLowerCase() == MatchResponse) {
                PhoneNameMatch = true;
            }
            if (phoneToAddress.toLowerCase() == MatchResponse) {
                PhoneMatch = true;
            }
        }
        if (ipAddressChecks != null) {
            distanceFromAddress = ipAddressChecks.DistanceFromAddress;
            ipIsValid = ipAddressChecks.IsValid;
            ipIsProxy = ipAddressChecks.IsProxy;
            geolocation = response.IpAddressChecks.Geolocation;
            if (geolocation != null) {
                geolocationPostalCode = geolocation.PostalCode;
                geolocationCityName = geolocation.CityName;
                geolocationCountryCode = geolocation.CountryCode;
                geolocationCountryName = geolocation.CountryName;
                geolocationContinentCode = geolocation.ContinentCode;				 
				if (geolocationCountryCode.toLowerCase() == 'us') {
					CountryCodeIsValid = true;
				}
            }
            if (parseInt(distanceFromAddress) < 1000) {
                Distance = true;
            }
        }
        if (emailAddressChecks != null) {
            emailIsValid = emailAddressChecks.IsValid;
            emailToName = emailAddressChecks.EmailToName;
            isDisposable = emailAddressChecks.IsDisposable;
            emailFirstSeenDate = emailAddressChecks.EmailFirstSeenDate;
            emailFirstSeenDays = emailAddressChecks.EmailFirstSeenDays;
            if (parseInt(emailFirstSeenDays) > 90) {
                EmailFirstSeen = true;
            }
            if (emailFirstSeenDate != null && emailFirstSeenDate != '') {
                EmailNeverSeenBefore = true;
            }
            if (emailToName.toLowerCase() == MatchResponse) {
                EmailMatch = true;
            }
        }
    }
    var Data = {
        'whitepageReport': {
            'Name': name,
            'Phone': phone,
            'PrimaryPhoneIsValid': primaryPhoneIsValid,
            'PhoneToName': phoneToName,
            'PhoneToAddress': phoneToAddress,
            'PhoneCountryCode': phoneCountryCode,
            'PhoneIsCommercial': phoneIsCommercial,
            'PhoneLineType': phoneLineType,
            'PhoneIsPrepaid': phoneIsPrepaid,
            'PrimaryAddressIsValid': primaryAddressIsValid,
            'AddressToName': addressToName,
            'AddressIsCommercial': addressIsCommercial,
            'EmailIsValid': emailIsValid,
            'EmailToName': emailToName,
            'EmailIsDisposable': isDisposable,
            'EmailFirstSeenDate': emailFirstSeenDate,
            'EmailFirstSeenDays': emailFirstSeenDays,
            'DistanceFromAddress': distanceFromAddress,
            'GeolocationPostalCode': geolocationPostalCode,
            'GeolocationCityName': geolocationCityName,
            'GeolocationCountryCode': geolocationCountryCode,
            'GeolocationCountryName': geolocationCountryName,
            'GeolocationContinentCode': geolocationContinentCode,
            'IdentityCheckScore': identityCheckScore,
            'CountryName': countryName,
            'City': city,
            'PostalCode': postalCode,
            'StateCode': stateCode,
            'StreetLine1': streetLine1,
            'StreetLine2': streetLine2,
            'IpIsValid': ipIsValid,
            'IpIsProxy': ipIsProxy,
            'PhoneMatch': PhoneMatch,
            'AddressMatch': AddressMatch,
            'Distance': Distance,
            'EmailNeverSeenBefore': EmailNeverSeenBefore,
            'EmailFirstSeen': EmailFirstSeen,
            'EmailMatch': EmailMatch,
            'PhoneNameMatch': PhoneNameMatch,
            'PhoneLine': PhoneLine,
            'referenceNumber': payload.eventData.ReferenceNumber,
			'CountryCodeIsValid': CountryCodeIsValid
        }
    };
    return {
        'result': result,
        'detail': null,
        'data': Data,
        'rejectcode': '',
        'exception': []
    };
}