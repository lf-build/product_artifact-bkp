function data_attribute_lexisnexis_businessreport(payload) {
	var result = "Passed";
	var businessState = "";
	var yearStarted = "";
	var yearsInBusiness = null;
	var state = "";
	var uccoriginalFilingDate = "";
	var latestFilingType = "";
	var businessType = "";
	var isActive = false;
	var filingAgencyName = "";
	var isforeClosed = "";
	var recordingDate = "";
	var inGoodStanding = "";
	var filingDate = null;
	var originalFilingDate = null;
	var bankruptcy = null;
	var filingDateObj = null;
	var uccFilingDateDifferance = null;
	var recordingDateDifferance = null;
	var foreClosedProperty = "";
	var isforeClosed = "";
	try {
		if (payload.eventData != null && typeof payload != "undefined") {
			var syndicationResult = payload.eventData.Response.report;
			if (syndicationResult.bankruptcy != null) {
				if (syndicationResult.bankruptcy.asDebtors != null && syndicationResult.bankruptcy.asDebtors.length > 0) {
					var debtors = syndicationResult.bankruptcy.asDebtors.sort(function (a, b) {
							return new Date(b.originalFilingDate) - new Date(a.originalFilingDate)
						});
					var originalFilingDateObj = debtors[0].originalFilingDate;
					if (originalFilingDateObj != null) {
						originalFilingDate = new Date(originalFilingDateObj.year, originalFilingDateObj.month - 1, originalFilingDateObj.day)
					}
				}
			}
			if (syndicationResult.property != null) {
				if (syndicationResult.property.propertyRecords != null && syndicationResult.property.propertyRecords.properties != null) {
					var properties = syndicationResult.property.propertyRecords.properties.sort(function (a, b) {
							return new Date(b.recordingDate) - new Date(a.recordingDate)
						});
					foreClosedProperty = properties[0];
					isforeClosed = properties[0].isforeClosed;
					recordingDate = properties[0].recordingDate
				}
			}
			if (syndicationResult.best != null) {
				yearStarted = syndicationResult.best.yearStarted;
				yearsInBusiness = syndicationResult.best.yearsInBusiness;
				state = syndicationResult.best.address.state;
				isActive = syndicationResult.best.isActive
			}
			if (syndicationResult.ucc != null) {
				if (syndicationResult.ucc.asDebtor != null) {
					if (syndicationResult.ucc.asDebtor.activeUcCs != null && syndicationResult.ucc.asDebtor.activeUcCs.length > 0) {
						var activeUcCs = syndicationResult.ucc.asDebtor.activeUcCs.sort(function (a, b) {
								return new Date(b.originalFilingDate) - new Date(a.originalFilingDate)
							});
						var uccoriginalFilingDateObj = activeUcCs[0].originalFilingDate;
						if (uccoriginalFilingDateObj != null) {
							uccoriginalFilingDate = new Date(uccoriginalFilingDateObj.year, uccoriginalFilingDateObj.month - 1, uccoriginalFilingDateObj.day)
						}
						latestFilingType = syndicationResult.ucc.asDebtor.activeUcCs[0].latestFilingType;
						filingAgencyName = syndicationResult.ucc.asDebtor.activeUcCs[0].filingAgencyName
					}
				}
			}
		}
		var Data = {
			businessReport: {
				OriginalFilingDate: originalFilingDate,
				ForeClosedProperty: foreClosedProperty,
				IsforeClosed: isforeClosed,
				RecordingDate: recordingDate,
				YearStarted: yearStarted,
				YearsInBusiness: yearsInBusiness,
				State: state,
				IsActive: isActive,
				UccoriginalFilingDate: uccoriginalFilingDate,
				LatestFilingType: latestFilingType,
				FilingAgencyName: filingAgencyName,
				referenceNumber: payload.eventData.referenceNumber
			}
		};
		return {
			result: result,
			detail: null,
			data: Data,
			rejectcode: "",
			exception: []
		}
	} catch (e) {
		return {
			result: "Failed",
			detail: ["Error in Rule" + e.message],
			data: e.message,
			rejectcode: "",
			exception: [e.message]
		}
	}
}
