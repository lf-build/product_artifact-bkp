function data_attribute_probe_companyhistory(payload) {
    var result = 'Passed';
    var name = '';
    var designation = '';
    var appointmentDate = '';
    var year = '';
    var earning_fc = '';
    var expedniture_fc = '';
    var gross_fixed_assets = '';
    var trade_receivable_exceeding_six_months = '';
    var athorisedSignatories = null;
    var financialParameter = null;
    if (typeof (payload) != 'undefined') {
        if (payload.eventData.Response.AuthorizedSignatoryResponse != null) {
            if (payload.eventData.Response.AuthorizedSignatoryResponse.AthorisedSignatories != null && payload.eventData.Response.AuthorizedSignatoryResponse.AthorisedSignatories.length > 0)
                athorisedSignatories = payload.eventData.Response.AuthorizedSignatoryResponse.AthorisedSignatories[0];
        }
        if (payload.eventData.Response.FinancialParameterResponse != null) {
            if (payload.eventData.Response.FinancialParameterResponse.FinancialParameter != null && payload.eventData.Response.FinancialParameterResponse.FinancialParameter.length > 0) {
                financialParameter = payload.eventData.Response.FinancialParameterResponse.FinancialParameter[0];
            }
        }
        if (athorisedSignatories != null && athorisedSignatories.length > 0) {
            name = athorisedSignatories.name;
            designation = athorisedSignatories.designation;
            appointmentDate = athorisedSignatories.dateOfAppointment;
        }
        if (financialParameter != null && financialParameter.length > 0) {
            year = financialParameter.year;
            earning_fc = financialParameter.earning_fc;
            expedniture_fc = financialParameter.expedniture_fc;
            gross_fixed_assets = financialParameter.gross_fixed_assets;
            trade_receivable_exceeding_six_months = financialParameter.trade_receivable_exceeding_six_months;
        }

        var Data = {
            'ProbeCompanyHistory': {
                'athorisedSignatories': athorisedSignatories,
                'financialParameter': financialParameter,
                'referenceNumber': payload.eventData.ReferenceNumber
            }
        };
        return {
            'result': result,
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };
    }
}