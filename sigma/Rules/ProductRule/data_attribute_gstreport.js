function data_attribute_gstreport(payload) {
    var result = 'Passed';
    var ContactInformation=null;
    var Registrationdetails=null;
    var ReportedTurnover=null;
    var Registrationdate='';
    var BusinessName='';
    var NatureofBusiness='';
    var AuthorizedSignatories='';
    var BusinessCategory='';
    var RegistrationStatus='';
    var LegalBusinessName='';
    var TurnoverpreGST='';
    var TurnoverpostGST='';
   
    if (typeof (payload) != 'undefined') {
        var reportDetail = payload.eventData.Response.result;
        if (reportDetail != null) {
           ContactInformation= reportDetail.profile.contacted;
           Registrationdate=reportDetail.profile.rgdt;
           BusinessName=reportDetail.profile.tradeNam;
           NatureofBusiness=reportDetail.profile.nba;
           AuthorizedSignatories=reportDetail.profile.mbr;
           BusinessCategory=reportDetail.profile.ctb;
           RegistrationStatus=reportDetail.profile.sts;
           LegalBusinessName=reportDetail.profile.lgnm;
           TurnoverpreGST=reportDetail.transaction_summary.gt;
           TurnoverpostGST=reportDetail.transaction_summary.turnover;
        }

        Registrationdetails={
            'Registrationdate':Registrationdate,
            'BusinessName':BusinessName,
            'NatureofBusiness':NatureofBusiness,
            'AuthorizedSignatories':AuthorizedSignatories,
            'BusinessCategory':BusinessCategory,
            'RegistrationStatus':RegistrationStatus,
            'LegalBusinessName':LegalBusinessName
        };

        ReportedTurnover={
            'TurnoverpreGST':TurnoverpreGST,
            'TurnoverpostGST':TurnoverpostGST
        };
        var Data = {
            'GstReportDetail': {
                'ContactInformation':ContactInformation,
                'Registrationdetails':Registrationdetails,
                'ReportedTurnover':ReportedTurnover,
                'referenceNumber': payload.eventData.ReferenceNumber
            }
        };
        return {
            'result': result,
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };
    }
}