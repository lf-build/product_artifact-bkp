function taggingRuleForStatusChanged(payload) {

	var applyTags = [];
	var removeTags = [];
	if (payload != null && payload.eventData != null && payload.eventData.Data != null && payload.eventData.Data.NewStatus != null) {
		var objFundingStatus = payload.eventData.Data;
		if (objFundingStatus.EntityType == 'loan' && (objFundingStatus.NewStatus == '200.70' || objFundingStatus.NewStatus == '200.80' || objFundingStatus.NewStatus == '200.90' || objFundingStatus.NewStatus == '200.11')) {
			removeTags.push('DPD 0');
		}
	}
	var Data = {
		'ApplyTags': applyTags,
		'RemoveTags': removeTags
	};
	return {
		'result': 'Passed',
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
