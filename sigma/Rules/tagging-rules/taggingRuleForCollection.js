function taggingRuleForCollection(payload) {

	var applyTags = [];
	var removeTags = [];
	if (payload != null && payload.eventData != null && payload.eventData.Data != null && payload.eventData.Data.LoanResponse != null) {
		var appliedCollectionStage = payload.eventData.Data.LoanResponse.CollectionStage;
		var removedCollectionStage = payload.eventData.Data.OldCollectionStage;

		applyTags.push(appliedCollectionStage);
		if (removedCollectionStage != null && removedCollectionStage != '' &&removedCollectionStage != undefined ) {
			removeTags.push(removedCollectionStage);
		}
	}
	var Data = {
		'ApplyTags': applyTags,
		'RemoveTags': removeTags
	};
	return {
		'result': 'Passed',
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
