var matchStrings = ['this.call(\'', 'this.service(\'', 'self.call(\'', 'self.service(\'', 'this.call("', 'this.service("', 'self.call("', 'self.service("'];
var path = require('path');
var fs = require('fs');
var harpMinify = require("harp-minify")

var walkSync = function (dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        }
        else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

function getDependencies(str, matchString) {
    var all = [];
    var firstMatch = str.split(matchString);
    if (firstMatch[1] != undefined) {
        var lastIndex = firstMatch[1].indexOf('\'');
        if (matchString.indexOf('"')>=0) {
            lastIndex = firstMatch[1].indexOf('"');
        }
        var pushIt = str.split(matchString)[1].substring(0, lastIndex).replace(/'/g, '');
        all.push(pushIt);
        var newStr = str.substring(firstMatch[0].length + lastIndex, str.length);
        if (firstMatch.length > 2 && newStr.indexOf(matchString) >= 0) {
            var newMatch = getDependencies(newStr,matchString);
            if (newMatch != null) {
                for(var p=0;p<newMatch.length;p++){
                    all.push(newMatch[p]);
                }
            }
        }
        return all;
    }
    return null;
}

function replaceString(config, data) {
    
        if (config != null && config.data_replacement != null && config.data_replacement.length > 0) {
            for (i in config.data_replacement) {
                var regex = new RegExp("{{" + config.data_replacement[i].name + "}}", "g");
                data = data.replace(regex, config.data_replacement[i].value);
            }
        }
        return data;
    }

function saveRule(serviceUrl, rule, token,config) {
    var api = require('./api_module.js')(token);

    return new Promise(function (resolve, reject) {
        rule.source = replaceString(config, rule.source);
        
        api.put(serviceUrl, rule).then(function (result) {
            if (result.statusCode == 200) {
                try{
                    var fnName = rule.source.substr(0, rule.source.indexOf("("))
                    var fnName = fnName.replace('function', '').trim();
                    console.log("\r\n[Done] Publishing rule: " + fnName);
                }catch(er){
                    console.log("\r\n[Done] Publishing rule " + er);
                }                
                resolve(result);
            }
            else {
                console.error("\r\n\r\nSave Rule Failed " + JSON.stringify(rule) + result.statusCode + ",message:" + result.statusMessage + ",response:" + JSON.stringify(result.body));
                //reject(response);
                resolve(result);
            }
        });
    });
}
function getRule(data) {
    try {
        var source = harpMinify.js(data, {
            compress: false,
            mangle: false
        });
        data = source;
    }
    catch (exception) {
        //console.log("Minification failed with message ");
        data = data.substring(data.indexOf('function'));
    }

    var dependencies = [];
    for (var j = 0; j < matchStrings.length; j++) {
        var childDependency = getDependencies(data, matchStrings[j]);
        if (childDependency != null) {
            for (k = 0; k < childDependency.length; k++) {
                if (dependencies.indexOf(childDependency[k]) == -1) {
                    dependencies.push(childDependency[k]);
                }
            }
        }
    }
    return { source: data, dependencies: dependencies };
}
function importRules(serviceUrl, folderPath, token,config) {
    var self = this;
    var files = walkSync(folderPath);
    var rulesToProcess = [];

    for (var i = 0; i < files.length; i++) {
        var element = files[i];
        var pathObject = path.parse(element);
        if (pathObject.ext.toLocaleLowerCase() == ".js" || pathObject.ext.toLocaleLowerCase() == ".ts") {
            var data = fs.readFileSync(element, 'utf8');
            rulesToProcess.push(getRule(data));
        }
        else {
            //console.warn('Skipping file ' + element + ' as extension :' + pathObject.ext.toLocaleLowerCase());
        }
    }
    rulesToProcess = rulesToProcess.sort(function (a, b) {
        return a.dependencies.length - b.dependencies.length;
    });

    rulesToProcess = rulesToProcess.map(r => () => saveRule(serviceUrl, r, token,config));

    return rulesToProcess.reduce((a, c) => {
        return a.then(_ => {
            return c();
        }).catch(reason => {
            //TODO: Check if fail, it will publish others
            console.error('Rule failed: ', reason);
            return c();
        });
    }, new Promise((resolve) => resolve({})));
}

module.exports = function (token) {
    return {
        saveRule: function (serviceUrl, source,config) {
            return saveRule(serviceUrl, getRule(source), token,config);
        },
        importRules: function (serviceUrl, folderPath,config) {
            return importRules(serviceUrl, folderPath, token,config);
        }
    }
};