var path = require('path');
var fs = require('fs');

var walkSync = function (dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        } else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

function saveTemplate(serviceUrl, templateFile, config, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(templateFile);
    var data = "";
    if (pathObject.ext.toLocaleLowerCase() == ".html") {
        var htmlData = fs.readFileSync(templateFile, 'utf8');
        var templateSchema = fs.readFileSync(templateFile.replace('.html', '.json'), 'utf8');
        templateSchema = templateSchema.replace("{{body}}", '');
        var template = JSON.parse(templateSchema);
        htmlData = replaceString(config, htmlData);
        template.body = htmlData.replace(/\r|\n|\t/g, '');

        return api.delete(serviceUrl + '/' + template.name + '/' + template.version + '/' + template.format, data).then(function (result) {
            return api.post(serviceUrl, template).then(function (result) {
                if (result.statusCode == 200) {
                    console.log("[Done] Publishing template: " + templateFile);
                } else {
                    console.log("[Error] Failed to publish template: " + templateFile);
                }
            });
        });
    } else {
        return Promise.resolve(true);
    }
}

function importTemplates(serviceUrl, folderPath, config, token) {
    var files = walkSync(folderPath);
    var templateToProcess = [];

    for (var i = 0; i < files.length; i++) {
        var pathObject = path.parse(files[i]);
        if (pathObject.ext.toLocaleLowerCase() == ".html") {
            templateToProcess.push(files[i]);
        }
    }

    templateToProcess = templateToProcess.map(r => () => saveTemplate(serviceUrl, r, config, token));

    return templateToProcess.reduce((a, c) => {
        return a.then(_ => {
            return c();
        }).catch(reason => {
            //TODO: Check if fail, it will publish others
            console.error('Template failed: ', reason);
            return c();
        });
    }, new Promise((resolve) => resolve({})));
}

function replaceString(config, data) {
    
        if (config != null && config.data_replacement != null && config.data_replacement.length > 0) {
            for (i in config.data_replacement) {
                var regex = new RegExp("<<" + config.data_replacement[i].name + ">>", "g");
                data = data.replace(regex, config.data_replacement[i].value);
            }
        }
        return data;
    }
    
module.exports = function (token) {
    return {
        saveTemplate: function (serviceUrl, templateFile, config) {
            return saveTemplate(serviceUrl, templateFile,config, token);
        },
        importTemplates: function (serviceUrl, folderPath, config) {
            return importTemplates(serviceUrl, folderPath, config, token);
        }
    }
}