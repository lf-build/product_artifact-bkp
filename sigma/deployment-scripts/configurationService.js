var path = require('path');
var fs = require('fs');

var walkSync = function (dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        } else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

function saveConfiguration(enviornments, enviornment, serviceUrl, configFile, config, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(configFile);
    var fileEnv = null;
    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        var isEnvSpecificFile = false;
        var configuratinoKey = pathObject.name;
        for (i = 0; i < enviornments.length; i++) {
            if (pathObject.name.endsWith("." + enviornments[i])) {
                fileEnv = enviornments[i];
                isEnvSpecificFile = true;
            }
        }
        if (isEnvSpecificFile == true) {
            if (fileEnv != enviornment) {
                console.log('Skipping file '+ pathObject.name + ' as not specific to ' + enviornment);
                return;
            } else {
                configuratinoKey = configuratinoKey.replace("." + enviornment, '');
                console.log('Using file '+ pathObject.name + ' as specific to ' + enviornment + ' With key ' + configuratinoKey);
            }
        }

        //console.log('Publishing Configuration ' + configFile);
        var data = fs.readFileSync(configFile, 'utf8');
        data = replaceString(config, data);
        //console.log('Configuring for ' + configuratinoKey);
        // return api.delete(serviceUrl + '/' + configuratinoKey, data).then(function (result) {
        //     if (result.statusCode == 200 || result.statusCode == 204 || result.statusCode == 404 || result.statusCode==405) {
                //console.log("Configuration Deleted " + configuratinoKey);
                return api.post(serviceUrl + '/' + configuratinoKey, data.replace(/192.168.1.65/g, serviceUrl.split(':')[1].replace('//', '')).replace(/192.168.1.59/g, serviceUrl.split(':')[1].replace('//', ''))).then(function (result) {
                    if (result.statusCode == 200 || result.statusCode == 204 || result.statusCode == 404) {
                        console.log("[Done] Publishing configuration: " + configuratinoKey);
                    } else {
                        console.log(data);
                        console.log("[Error] Failed to publish configuration: " + configuratinoKey);
                    }
                });
            // } else {
            //     console.error("[Error] Delete configuration failed for " + configuratinoKey + ",message:" + result.statusMessage + ",response:" + result.body);
            // }
        //});
    } else {
        console.warn('[Warning] Skipping file ' + configFile + ' as extension :' + pathObject.ext.toLocaleLowerCase());
    }
}

function replaceString(config, data) {

    if (config != null && config.data_replacement != null && config.data_replacement.length > 0) {
        for (i in config.data_replacement) {
            var regex = new RegExp("{{" + config.data_replacement[i].name + "}}", "g");
            data = data.replace(regex, config.data_replacement[i].value);
        }
    }
    return data;
}

function importConfiguration(enviornments, enviornment, serviceUrl, folderPath, config, token) {
    var files = walkSync(folderPath);
    var configToProcess = [];

    for (var i = 0; i < files.length; i++) {
        var element = files[i];
        var pathObject = path.parse(element);
        if (pathObject.ext.toLocaleLowerCase() == ".json") {
            configToProcess.push(element);
        } else {
            console.warn('[Warning] Skipping file ' + element + ' as extension :' + pathObject.ext.toLocaleLowerCase());
        }
    }

    configToProcess = configToProcess.map(r => () => saveConfiguration(enviornments, enviornment, serviceUrl, r, config, token));

    return configToProcess.reduce((a, c) => {
        return a.then(_ => {
            return c();
        }).catch(reason => {
            //TODO: Check if fail, it will publish others
            console.error('[Error] Configuration failed: ', reason);
            return c();
        });
    }, new Promise((resolve) => resolve({})));
}

module.exports = function (token) {
    return {
        saveConfiguration: function (enviornments, enviornment, serviceUrl, configFile, config) {
            return saveConfiguration(enviornments, enviornment, serviceUrl, configFile, config, token);
        },
        importConfiguration: function (enviornments, enviornment, serviceUrl, folderPath, config) {
            return importConfiguration(enviornments, enviornment, serviceUrl, folderPath, config, token);
        }
    }
}