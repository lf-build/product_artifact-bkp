function GenerateOcrolusCashflow(payload) {
    var result = 'Passed';
    var monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];
    var Data = {};
    if (typeof (payload) != 'undefined') {
        try {
            var ocrolusResponse = payload.eventData.Data.Response.response;
            var valuemodel = {};
            if (ocrolusResponse != null && ocrolusResponse != undefined) {
                valuemodel = GetValues(ocrolusResponse, payload.eventData.AccountId);
            }
            Data['key'] = valuemodel;
            return {
                'result': result,
                'detail': null,
                'data': Data,
                'rejectcode': '',
                'exception': []
            };
        }
        catch (e) {
            return {
                'result': 'Failed',
                'detail': null,
                'data': null,
                'rejectcode': '',
                'exception': ['Unable to verify : ' + e.message]
            };
        }
    }
    else {
        return {
            'result': 'Failed',
            'detail': null,
            'data': null,
            'rejectcode': '',
            'exception': []
        };
    }
    function GetValues(data, accountid) {
        var SelectedTransactions = [];
        var MonthlyCashFlowsList = [];
        var CategorySummaryList = [];
        var TransactionList = [];
        var RecurringList = [];
        var MCARecurringList = [];
        var myCustomCashFlow = {
            'MonthlyCashFlows': [{}],
            'TransactionSummary': {},
            'CategorySummary': [{}],
            'TransactionList': [{}],
            'RecurringList': [{}],
            'MCARecurringList': [{}],
            'GridReport': {}
        };
        var CashFlowViewModel = {
            'AccountHeader': '',
            'AccountID': '',
            'AccountNumber': '',
            'InstitutionName': '',
            'AccountType': '',
            'referenceNumber': '',
            'IsSelected': false,
            'Source': 'Ocrolus',
            'CashFlow': myCustomCashFlow
        };
        var cashFlowResult = CashFlowViewModel;
        cashFlowResult.AccountHeader = data.bank_accounts[0].name + '-' + data.bank_accounts[0].account_number + '-' + data.bank_accounts[0].account_type;
        cashFlowResult.AccountNumber = data.bank_accounts[0].account_number;
        cashFlowResult.InstitutionName = data.bank_accounts[0].name;
        cashFlowResult.AccountType = data.bank_accounts[0].account_type;
        cashFlowResult.AccountID = accountid;
        cashFlowResult.referenceNumber = payload.eventData.ReferenceNumber;
        var t = myCustomCashFlow;
        for (var i = 0; i < data.bank_accounts.length; i++) {
            var monthlycashflow = GetMonthlyCashflow(data.bank_accounts[i]);
            if (monthlycashflow != null) {
                MonthlyCashFlowsList.push(monthlycashflow);
            }
        }
        t.MonthlyCashFlows = MonthlyCashFlowsList;
        t.TransactionSummary = GetTransactionSummary(data);
        t.CategorySummary = CategorySummaryList;
        t.TransactionList = TransactionList;
        t.RecurringList = RecurringList;
        t.MCARecurringList = MCARecurringList;
        return cashFlowResult;
    }
    function GetTransactionSummary(data) {
        var TransactionSummaryViewModel = {
            CountOfMonthlyStatement: 0,
            StartDate: '',
            EndDate: '',
            AverageDeposit: 0,
            AnnualCalculatedRevenue: 0.00,
            AverageWithdrawal: 0,
            AverageDailyBalance: 0,
            NumberOfNegativeBalance: 0,
            NumberOfNSF: 0,
            NSFAmount: 0,
            LoanPaymentAmount: 0,
            NumberOfLoanPayment: 0,
            PayrollAmount: 0,
            NumberOfPayroll: 0,
            ChangeInDepositVolume: 0,
            TotalCredits: 0,
            TotalDebits: 0,
            TotalCreditsCount: 0,
            TotalDebitsCount: 0,
            AvailableBalance: 0,
            CurrentBalance: 0,
            AverageBalanceLastMonth: 0,
            MedianMonthlyIncome: 0,
            MedianDailyBalance: 0,
            MaxDaysBelow100Count: 0,
            CVOfDailyDeposit: 0,
            CVOfDailyBalance: 0,
            AverageMonthlyRevenue: 0.00,
            TotalRevenueAmount: 0.00,
            TotalRevenueCount: 0,
            PDFReportName: '',
            BrokerAGS: 0,
            CAPAGS: 0,
            BrokerAGSText: ''
        };
        var objTransactionSummary = TransactionSummaryViewModel;
        objTransactionSummary.AverageDeposit = CustomRound(Object.values(data.average_deposit_by_month)[0], 2);
        objTransactionSummary.AverageDailyBalance = CustomRound(Object.values(data.average_daily_balance_by_month)[0], 2);
        objTransactionSummary.AverageMonthlyRevenue = CustomRound(Object.values(data.estimated_revenue_by_month)[0]);
        return objTransactionSummary;
    }
    function GetMonthlyCashflow(data) {
        var MonthlyCashFlowViewModel = {
            Name: '',
            Year: 0,
            BeginingBalance: 0.00,
            EndingBalance: 0.00,
            DepositCount: 0,
            WithdrawalCount: 0,
            TotalDepositAmount: 0.00,
            TotalWithdrawalAmount: 0.00,
            AverageDailyBalance: 0.00,
            AverageDeposit: 0.00,
            IncDecBalance: 0.00,
            AverageDailyBalancePercent: 0.00,
            PeriodFrom: '',
            PeriodTo: '',
            GridRowHeader: '',
            MonthInNumber: 0,
            FirstTransactionDate: '',
            EndTransactionDate: '',
            MinDepositAmount: 0.00,
            MaxDepositAmount: 0.00,
            MinWithdrawalAmount: 0.00,
            MaxWithdrawalAmount: 0.00,
            NumberOfNSF: 0,
            NSFAmount: 0.00,
            LoanPaymentAmount: 0.00,
            NumberOfLoanPayment: 0,
            PayrollAmount: 0.00,
            NumberOfPayroll: 0,
            NumberOfNegativeBalance: 0.00,
            CustomAttributes: '',
            DaysBelow100Count: 0,
            TotalMonthlyRevenueAmount: 0.00,
            TotalMonthlyRevenueCount: 0,
            PDFReportName: '',
            DateOfMonthlyCycle: ''
        };
        var period = data.periods[0];
        var currentMonthCashFlow = MonthlyCashFlowViewModel;
        var DateFormated = new Date(period.begin_date);
        currentMonthCashFlow.Name = monthNames[DateFormated.getMonth()];
        currentMonthCashFlow.Year = DateFormated.getFullYear();
        currentMonthCashFlow.BeginingBalance = CustomRound(period.begin_balance, 2);
        currentMonthCashFlow.EndingBalance = CustomRound(period.end_balance, 2);
        currentMonthCashFlow.TotalDepositAmount = CustomRound((Object.values(data.deposits_sum_by_month)[0]), 2);
        currentMonthCashFlow.AverageDailyBalance = CustomRound((Object.values(data.average_daily_balance_by_month)[0]), 2);
        currentMonthCashFlow.AverageDeposit = CustomRound(Object.values(data.average_deposit_by_month)[0], 2);
        currentMonthCashFlow.IncDecBalance = CustomRound((currentMonthCashFlow.BeginingBalance - currentMonthCashFlow.EndingBalance), 2);
        currentMonthCashFlow.AverageDailyBalancePercent = ReturnZeroIfInfinity(ADBPercentageCalculation(currentMonthCashFlow.AverageDailyBalance, currentMonthCashFlow.TotalDepositAmount));
        currentMonthCashFlow.PeriodFrom = getFormattedDateForMonthPeriod(new Date(period.begin_date));
        currentMonthCashFlow.PeriodTo = getFormattedDateForMonthPeriod(new Date(period.end_date));
        currentMonthCashFlow.MonthInNumber = DateFormated.getMonth() + 1;
        currentMonthCashFlow.MaxDepositAmount = (Object.values(data.deposit_max_by_month)[0])[0].amount;
        currentMonthCashFlow.MinDepositAmount = (Object.values(data.deposit_min_by_month)[0])[0].amount;
        currentMonthCashFlow.NumberOfNSF = data.nsf_transactions.length;
        currentMonthCashFlow.NumberOfNegativeBalance = Object.keys(data.negative_balances_by_month).length;
        currentMonthCashFlow.TotalMonthlyRevenueAmount = Object.values(data.estimated_revenue_by_month)[0];
        return currentMonthCashFlow;
    }
    function CustomRound(value, precision) {
        var multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    }
    function ReturnZeroIfInfinity(numberToCheck) {
        return (numberToCheck == Infinity || isNaN(numberToCheck)) ? 0.00 : numberToCheck;
    }
    function ADBPercentageCalculation(averageDailyBalance, totalDepositAmount) {
        var resultADBPercentage = ((averageDailyBalance * 100) / totalDepositAmount);
        return CustomRound(resultADBPercentage, 2);
    }
    function getFormattedDateForMonthPeriod(date) {
        var options = {
            year: 'numeric',
            month: 'short',
            day: '2-digit'
        };
        return date.toLocaleDateString('en-US', options);
    }
}