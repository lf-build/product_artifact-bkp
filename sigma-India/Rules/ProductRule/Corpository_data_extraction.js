function corpository_data_extraction(payload) {
    var companies = null;
    var companyName = '';
    var registeredAddress = '';
    var classOfCompany = '';
    var listingStatus = '';
    var status = '';
    var directors = [];
    var financialSnapshotHistory = [];
    var companyProfile=null;
    var result = 'Failed';

    try {
        if (payload !== null) {
            if (payload.eventData !== null && payload.eventData.Response !== null) {
                companies = payload.eventData.Response.Companies;
                if (companies !== null && companies.length > 0) {
                    var company = companies[0];
                   
                    if (company !== null) {
                        companyProfile=company.CompanyProfile;
                        if (company.CompanyProfile !== null) {
                            companyName = company.CompanyProfile.CompanyName;
                            registeredAddress = company.CompanyProfile.RegisteredAddress;
                            classOfCompany = company.CompanyProfile.ClassOfCompany;
                            listingStatus = company.CompanyProfile.ListingStatus;
                            status = company.CompanyProfile.Status;
                        }
                        if (company.Signatories !== null && company.Signatories.length > 0) {
                            directors = (company.Signatories);
                        }
                        if (company.FinancialSnapshotHistory !== null && company.FinancialSnapshotHistory.length > 0) {
                            financialSnapshotHistory = (company.FinancialSnapshotHistory);
                        }
                    }
                }
                result = 'Passed';
            }
        }
        var data ={
			'corpository': {
            'CompanyName': companyName,
            'CompanyProfile':companyProfile,
            'RegisteredAddress': registeredAddress,
            'ClassOfCompany': classOfCompany,
            'ListingStatus': listingStatus,
            'Status': status,
            'Directors': directors,
			'FinancialSnapshotHistory': financialSnapshotHistory
			}
        };
        return {
            'result': result,
            'detail': null,
            'data': data,
            'rejectcode': '',
            'exception': []
        };

    } catch (e) {
        return {
            'result': result,
            'detail': null,
            'data': null,
            'rejectcode': '',
            'exception': [e.message]
        };
    }
}
