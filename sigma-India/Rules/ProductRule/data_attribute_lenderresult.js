function data_attribute_lenderresult(payload) {
	var lenderName = '';
	var statusCode = '';
	var result = 'Passed';
	if (typeof(payload) != 'undefined' && payload != null) {
		var source = payload.sourceLenderResult[0];
		if (source != null) {
			lenderName = source.lenderName;
			statusCode = source.statusCode;
		}
	}
	var Data = {
		'lenderName': lenderName,
		'statusCode': statusCode
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}