function data_attribute_yelp_phonesearch(payload) {
	var result = 'Passed';
	var rating = '';
	var isClosed = null;
	var phone = null;
	var postalcode = '';
	var city = '';
	var yelpbusinessdetailResponse = null;
	if (typeof (payload) != 'undefined') {
		if (payload.eventData.Response.Businesses != null && payload.eventData.Response.Businesses.length > 0) {
			yelpbusinessdetailResponse = payload.eventData.Response.Businesses[0];
		}
		if (yelpbusinessdetailResponse != null) {
			rating = yelpbusinessdetailResponse.Rating;
			isClosed = yelpbusinessdetailResponse.IsClosed;
			phone = yelpbusinessdetailResponse.Phone;
			if (yelpbusinessdetailResponse.Location != null) {
				city = yelpbusinessdetailResponse.Location.City;
				postalcode = yelpbusinessdetailResponse.Location.ZipCode;
			}
		}

	}
	var Data = {
		'yelpReport': {
			'Rating': rating,
			'IsClosed': isClosed,
			'Phone': phone,
			'City': city,
			'PostalCode': postalcode,
			'referenceNumber': payload.eventData.ReferenceNumber
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
