function data_attribute_finicity_save_data(payload) {
	var CustomerId='';
	var InstitutionLoginId ='';
	var result = 'Passed';
	if (typeof (payload) != 'undefined'&&payload!=null) {
		var finicityResponse = payload.eventData.Response.CustomerAccountsDetails[0];
		if (finicityResponse != null) {
			CustomerId=finicityResponse.customerId;
			InstitutionLoginId=finicityResponse.institutionLoginId;
		}
	}
	var Data = {
		'finicitydatadetails': {
			'CustomerId': CustomerId,
			'InstitutionLoginId':InstitutionLoginId
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
