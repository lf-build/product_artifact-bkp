function data_attribute_probe(payload) {
    var result = 'Passed';
    var Cin = '';
    var CompanyName = '';
    var Incorporationdate = '';
    var City = '';
    var State = '';
    var Classification = '';
    var Companystatus = '';
    if (typeof (payload) != 'undefined') {
        var companyDetails = payload.eventData.Response.Company;
        if (companyDetails != null) {
            Cin = companyDetails.cin;
            CompanyName = companyDetails.legalName;
            Incorporationdate = companyDetails;
            Classification = companyDetails.classification;
            Companystatus = companyDetails.status;
            if (companyDetails.registeredAddress != null) {
                City = companyDetails.registeredAddress.city;
                State = companyDetails.registeredAddress.state;
            }
        }
        var Data = {
            'ProbeCompanyDetails': {
                'Cin': Cin,
                'CompanyName': CompanyName,
                'Incorporationdate': Incorporationdate,
                'City': City,
                'State': State,
                'Classification': Classification,
                'Companystatus': Companystatus,
                'referenceNumber': payload.eventData.ReferenceNumber
            }
        };
        return {
            'result': result,
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };
    }
}