function data_attribute_aadhaardata(payload) {
	var result = 'Passed';
	var response = null;
	if (typeof (payload) != 'undefined') {
		if (payload.eventData.Response != null) {
			response = payload.eventData.Response;
		}

	}
	var Data = {
		'AadhaarData': {
			'AadhaarData':response,
			'referenceNumber': payload.eventData.ReferenceNumber
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
