function data_attribute_addcustomer_finicity(payload) {
	var CustomerId='';
	var result = 'Passed';
	if (typeof (payload) != 'undefined'&&payload!=null) {
		var finicityResponse = payload.eventData.Response;
		if (finicityResponse != null) {
			CustomerId=finicityResponse.CustomerId;
		}
	}
	var Data = {
		'finicitydetails': {
			'CustomerId': CustomerId
		}
	};
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
