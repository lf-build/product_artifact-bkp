function GeneratePlaidTransactionCashFlow(payload) {
    var Plaidmonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var result = 'Passed';
    var Category_Payroll = '21009000';
    var Category_LoanPayment = '16003000';
    var Category_NSF = '10007000';
    var Category_SkipDeposit = ['21000000', '21001000'];
    var Category_Revenue = ['21002000', '21004000', '21005000', '21007000', '21007001', '21007002', '21010000', '21010001', '21010002', '21010003', '21010004', '21010005', '21010006', '21010007', '21010008', '21010009', '21010010', '21010011', '16001000', '21012000', '21012001', '21012002'];
    var endingBalance = 0;
    var beginingBalance = 0;
    var StartDate;
    var EndDate;
    var NegativeBalanceCount = 0;
    var allAccountsCashflows = {};
    var DataAttributePostFix = '';
    var allowedCategory = ['21009000', '21002000', '10001000', '16003000', '10007000'];
    var ExcludeWeekEnds = false;
    var addMissingMonths = true;
    var periodStringFrom = '';
    var periodStringTo = '';
    var calculateRecurringTransaction = true;
    var self = this;
    var cashflowService = self.call('cashflow');
    if (typeof (payload) != 'undefined') {
        try {
            var entityId = payload.entityId;
            var entityType = payload.entityType;
            var cashFlowInput = payload.eventData;
            var account = cashFlowInput.Accounts;
            return cashflowService.getTransactions(entityType, entityId, account.Id).then(function (allTrans) {
                var AllTransactions = [];
                AllTransactions = allTrans.transaction;
                var myCustomCashFlow = {
                    'MonthlyCashFlows': [{}
                    ],
                    'TransactionSummary': {},
                    'CategorySummary': [{}
                    ],
                    'TransactionList': [{}
                    ],
                    'RecurringList': [{}
                    ],
                    'MCARecurringList': [{}
                    ],
                    'GridReport': {}
                };
                var PlaidCashFlowViewModel = {
                    'AccountHeader': '',
                    'AccountID': '',
                    'AccountNumber': '',
                    'InstitutionName': '',
                    'AccountType': '',
                    'referenceNumber': '',
                    'IsSelected': false,
                    'Source': '',
                    'CashFlow': myCustomCashFlow
                };
                var cashFlowResult = PlaidCashFlowViewModel;
                endingBalance = account.CurrentBalance;
                beginingBalance = endingBalance;
                var SelectedTransactions = [];
                var MonthlyCashFlowsList = [];
                var CategorySummaryList = [];
                var TransactionList = [];
                var RecurringList = [];
                var MCARecurringList = [];
                var LastFourDigitAccountNumber = account.AccountNumber;
                if (account.AccountNumber != null) {
                    LastFourDigitAccountNumber = account.AccountNumber.substr(-4);
                }
                if (AllTransactions != null && AllTransactions.length > 0) {
                    SelectedTransactions = AllTransactions.filter(function (trans) {
                        return trans.providerAccountId == account.ProviderAccountId;
                    });
                    if (SelectedTransactions != null && SelectedTransactions.length > 0) {
                        var totalTransactions = SelectedTransactions.length;
                        SelectedTransactions.sort((a, b) => {
                            var start = +new Date(a.transactionDate);
                            var elapsed = +new Date(b.transactionDate) - start;
                            return elapsed;
                        });
                        if (calculateRecurringTransaction == true) {
                            GetRecurringSummary(account.Id, SelectedTransactions, function (trs) {
                                RecurringList = trs;
                            });
                            GetRecurringSummaryByCategoryId(account.Id, SelectedTransactions, function (trs) {
                                MCARecurringList = trs;
                            });
                        }
                        GetCategorySummary(SelectedTransactions, function (s) {
                            CategorySummaryList = s;
                        });
                        EndDate = SelectedTransactions[0].transactionDate;
                        StartDate = SelectedTransactions[totalTransactions - 1].transactionDate;
                        var maxTransactionDate = new Date(EndDate);
                        var currentMonth = maxTransactionDate.getMonth();
                        var currentYear = maxTransactionDate.getFullYear();
                        var firstDayOfMonth = new Date(currentYear, currentMonth, 1);
                        var lastDayOfMonth = new Date(currentYear, currentMonth + 1, 0);
                        var diffrenceInDays = lastDayOfMonth.getDate().toString();
                        periodStringFrom = getFormattedDateForMonthPeriod(firstDayOfMonth);
                        periodStringTo = getFormattedDateForMonthPeriod(lastDayOfMonth);
                        var lastMonth = currentMonth + '-' + currentYear;
                        var minTransactionDate = new Date(StartDate);
                        var firstMonth = minTransactionDate.getMonth() + '-' + minTransactionDate.getFullYear();
                        var isFirstTransaction = true;
                        var isLastMonth = true;
                        var dateOfLastTransaction;
                        var TotalDailyBalanceArray = new Array();
                        TotalDailyBalanceArray.push(beginingBalance);
                        var TotalDailyDepositArray = new Array();
                        var TotalBrokerAGSDepositArray = new Array();
                        var AGS_Month = 3;
                        var Last_N_Month = GetLastCalendarMonth(AGS_Month);
                        var AGSDeposit_N_Month = 0;
                        while (SelectedTransactions.length > 0) {
                            var currentMonthTransactions = SelectedTransactions.filter(function (item) {
                                if (new Date(item.transactionDate).getMonth() == currentMonth && new Date(item.transactionDate).getFullYear() == currentYear) {
                                    return item;
                                }
                            });
                            if (addMissingMonths == true) {
                                if ((currentMonthTransactions == null || currentMonthTransactions == undefined) || (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length == 0)) {
                                    var tempTransaction = {
                                        'categoryId': 0,
                                        'amount': 0,
                                        'transactionDate': (currentMonth + 1) + '/01/' + currentYear,
                                        'IsDummyRecord': true
                                    };
                                    currentMonthTransactions = [];
                                    currentMonthTransactions.push(tempTransaction);
                                }
                            }
                            if (currentMonthTransactions != null && currentMonthTransactions != undefined && currentMonthTransactions.length > 0) {
                                var PlaidMonthlyCashFlowViewModel = {
                                    Name: '',
                                    Year: 0,
                                    BeginingBalance: 0.00,
                                    EndingBalance: 0.00,
                                    DepositCount: 0,
                                    WithdrawalCount: 0,
                                    TotalDepositAmount: 0.00,
                                    TotalWithdrawalAmount: 0.00,
                                    AverageDailyBalance: 0.00,
                                    AverageDeposit: 0.00,
                                    IncDecBalance: 0.00,
                                    AverageDailyBalancePercent: 0.00,
                                    PeriodFrom: '',
                                    PeriodTo: '',
                                    GridRowHeader: '',
                                    MonthInNumber: 0,
                                    FirstTransactionDate: '',
                                    EndTransactionDate: '',
                                    MinDepositAmount: 0.00,
                                    MaxDepositAmount: 0.00,
                                    MinWithdrawalAmount: 0.00,
                                    MaxWithdrawalAmount: 0.00,
                                    NumberOfNSF: 0,
                                    NSFAmount: 0.00,
                                    LoanPaymentAmount: 0.00,
                                    NumberOfLoanPayment: 0,
                                    PayrollAmount: 0.00,
                                    NumberOfPayroll: 0,
                                    NumberOfNegativeBalance: 0.00,
                                    CustomAttributes: '',
                                    DaysBelow100Count: 0,
                                    TotalMonthlyRevenueAmount: 0.00,
                                    TotalMonthlyRevenueCount: 0,
                                    PDFReportName: '',
                                    DateOfMonthlyCycle: ''
                                };
                                var currentMonthCashFlow = PlaidMonthlyCashFlowViewModel;
                                var monthlyDailyBalance = new Array(31);
                                var MinDepositAmount = 0,
                                    MaxDepositAmount = 0,
                                    MinWithdrawalAmount = 0,
                                    MaxWithdrawalAmount = 0;
                                var DepositCount = 0,
                                    WithdrawalCount = 0,
                                    TotalMonthlyDepositAmount = 0,
                                    TotalMonthlyWithdrawalAmount = 0;
                                var TotalMonthlyLoanPaymentAmount = 0,
                                    TotalMonthlyLoanPaymentCount = 0;
                                var TotalMonthlyPayrollAmount = 0,
                                    TotalMonthlyPayrollCount = 0;
                                var TotalMonthlyNSFAmount = 0,
                                    TotalMonthlyNSFCount = 0;
                                var TotalMonthlyRevenueAmount = 0,
                                    TotalMonthlyRevenueCount = 0;
                                currentMonthCashFlow.Name = Plaidmonths[currentMonth];
                                currentMonthCashFlow.Year = currentYear;
                                var monthInNumber = (parseInt(currentMonth) + 1).toString();
                                currentMonthCashFlow.MonthInNumber = parseInt(currentMonth) + 1;
                                var firstDayOfMonth = new Date(currentYear, currentMonth, 1);
                                var lastDayOfMonth = new Date(currentYear, currentMonth + 1, 0);
                                var diffrenceInDays = lastDayOfMonth.getDate().toString();
                                periodStringFrom = getFormattedDateForMonthPeriod(firstDayOfMonth);
                                periodStringTo = getFormattedDateForMonthPeriod(lastDayOfMonth);
                                monthInNumber = monthInNumber.length > 1 ? monthInNumber : '0' + monthInNumber;
                                currentMonthCashFlow.PDFReportName = LastFourDigitAccountNumber + '_' + monthInNumber + '_' + currentYear + '.pdf';
                                currentMonthCashFlow.EndingBalance = PlaidcustomRound(beginingBalance, 2);
                                var index = -1;
                                currentMonthTransactions.forEach(function (objTransaction) {
                                    if (objTransaction != undefined) {
                                        if (objTransaction.categoryId != undefined) {
                                            if (objTransaction.categoryId == Category_Payroll) {
                                                TotalMonthlyPayrollAmount = TotalMonthlyPayrollAmount + objTransaction.amount;
                                                TotalMonthlyPayrollCount = TotalMonthlyPayrollCount + 1;
                                                currentMonthCashFlow.PayrollAmount = Math.abs(TotalMonthlyPayrollAmount);
                                                currentMonthCashFlow.NumberOfPayroll = TotalMonthlyPayrollCount;
                                            } else if (objTransaction.categoryId == Category_NSF) {
                                                TotalMonthlyNSFAmount = TotalMonthlyNSFAmount + objTransaction.amount;
                                                TotalMonthlyNSFCount = TotalMonthlyNSFCount + 1;
                                                currentMonthCashFlow.NSFAmount = TotalMonthlyNSFAmount;
                                                currentMonthCashFlow.NumberOfNSF = TotalMonthlyNSFCount;
                                            } else if (objTransaction.categoryId == Category_LoanPayment) {
                                                TotalMonthlyLoanPaymentAmount = TotalMonthlyLoanPaymentAmount + objTransaction.amount;
                                                TotalMonthlyLoanPaymentCount = TotalMonthlyLoanPaymentCount + 1;
                                                currentMonthCashFlow.LoanPaymentAmount = TotalMonthlyLoanPaymentAmount;
                                                currentMonthCashFlow.NumberOfLoanPayment = TotalMonthlyLoanPaymentCount;
                                            } else if (Category_Revenue.indexOf(objTransaction.categoryId) > -1) {
                                                TotalMonthlyRevenueAmount = TotalMonthlyRevenueAmount + objTransaction.amount;
                                                TotalMonthlyRevenueCount = TotalMonthlyRevenueCount + 1;
                                                currentMonthCashFlow.TotalMonthlyRevenueAmount = Math.abs(TotalMonthlyRevenueAmount);
                                                currentMonthCashFlow.TotalMonthlyRevenueCount = TotalMonthlyRevenueCount;
                                            }
                                        }
                                        var currentTransactionDate = new Date(objTransaction.transactionDate);
                                        var day = currentTransactionDate.getDate();
                                        var calculateDailyEnding = isFirstTransaction ? isFirstTransaction : (getFormattedDate(dateOfLastTransaction) != getFormattedDate(currentTransactionDate));
                                        if (calculateDailyEnding) {
                                            endingBalance = beginingBalance;
                                            monthlyDailyBalance[day - 1] = endingBalance;
                                        }
                                        dateOfLastTransaction = currentTransactionDate;
                                        var transactionAmount = objTransaction.amount;
                                        if (transactionAmount < 0) {
                                            beginingBalance -= Math.abs(transactionAmount);
                                            TotalDailyDepositArray.push(Math.abs(transactionAmount));
                                            var tempDate = new Date(objTransaction.transactionDate).getMonth() + '-' + new Date(objTransaction.transactionDate).getFullYear();
                                            if (Last_N_Month.indexOf(tempDate) > -1) {
                                                TotalBrokerAGSDepositArray.push(Math.abs(transactionAmount));
                                            }
                                            if (transactionAmount < MaxDepositAmount || MaxDepositAmount == 0) {
                                                MaxDepositAmount = transactionAmount;
                                            }
                                            if (transactionAmount > MinDepositAmount || MinDepositAmount == 0) {
                                                MinDepositAmount = transactionAmount;
                                            }
                                            DepositCount = DepositCount + 1;
                                            TotalMonthlyDepositAmount = TotalMonthlyDepositAmount + transactionAmount;
                                        } else if (transactionAmount > 0) {
                                            beginingBalance += transactionAmount;
                                            if (transactionAmount > MaxWithdrawalAmount || MaxWithdrawalAmount == 0) {
                                                MaxWithdrawalAmount = transactionAmount;
                                            }
                                            if (transactionAmount < MinWithdrawalAmount || MinWithdrawalAmount == 0) {
                                                MinWithdrawalAmount = transactionAmount;
                                            }
                                            WithdrawalCount = WithdrawalCount + 1;
                                            TotalMonthlyWithdrawalAmount = TotalMonthlyWithdrawalAmount + transactionAmount;
                                        }
                                        isFirstTransaction = false;
                                        index = -1;
                                        index = SelectedTransactions.indexOf(objTransaction, 0);
                                        if (index > -1) {
                                            SelectedTransactions.splice(index, 1);
                                        }
                                        if (totalTransactions > 1 && index > -1) {
                                            TotalDailyBalanceArray.push(beginingBalance);
                                        }
                                    }
                                    if (index > -1) {
                                        totalTransactions -= 1;
                                    }
                                });
                                currentMonthCashFlow.TotalWithdrawalAmount = PlaidcustomRound(TotalMonthlyWithdrawalAmount, 2);
                                currentMonthCashFlow.WithdrawalCount = WithdrawalCount;
                                currentMonthCashFlow.TotalDepositAmount = PlaidcustomRound(Math.abs(TotalMonthlyDepositAmount), 2);
                                currentMonthCashFlow.DepositCount = DepositCount;
                                currentMonthCashFlow.MinDepositAmount = Math.abs(MinDepositAmount);
                                currentMonthCashFlow.MaxDepositAmount = Math.abs(MaxDepositAmount);
                                currentMonthCashFlow.MinWithdrawalAmount = MinWithdrawalAmount;
                                currentMonthCashFlow.MaxWithdrawalAmount = MaxWithdrawalAmount;
                                currentMonthCashFlow.DateOfMonthlyCycle = diffrenceInDays;
                                currentMonthCashFlow.AverageDeposit = ReturnZeroIfInfinity(AverageDepositCalculation(currentMonthCashFlow.TotalDepositAmount, DepositCount));
                                if (currentMonthTransactions[0].IsDummyRecord == undefined) {
                                    currentMonthCashFlow.FirstTransactionDate = PlaidcustomDate(dateOfLastTransaction);
                                    if (currentMonthTransactions.length > 0) {
                                        currentMonthCashFlow.EndTransactionDate = PlaidcustomDate(new Date(currentMonthTransactions[0].transactionDate));
                                    }
                                }
                                currentMonthCashFlow.BeginingBalance = PlaidcustomRound(beginingBalance, 2);
                                var totalDaysInmonth = 0;
                                totalDaysInmonth = plaidGetDaysInMonth(dateOfLastTransaction.getMonth(), dateOfLastTransaction.getFullYear());
                                var monthCheck = currentMonth + '-' + currentYear;
                                var MonthlyResponse = {};
                                if (firstMonth == lastMonth) {
                                    MonthlyResponse = CalculateADBFirstMonth(monthlyDailyBalance, totalDaysInmonth);
                                } else if (lastMonth == monthCheck) {
                                    MonthlyResponse = CalculateADB(monthlyDailyBalance, totalDaysInmonth, TotalDailyBalanceArray);
                                } else if (firstMonth == monthCheck) {
                                    MonthlyResponse = CalculateADBFirstMonth(monthlyDailyBalance, totalDaysInmonth);
                                } else {
                                    MonthlyResponse = CalculateADB(monthlyDailyBalance, totalDaysInmonth, TotalDailyBalanceArray);
                                }
                                currentMonthCashFlow.AverageDailyBalance = MonthlyResponse.ADB;
                                currentMonthCashFlow.NumberOfNegativeBalance = PlaidcustomRound(MonthlyResponse.NegativeDaysCount, 2);
                                NegativeBalanceCount = NegativeBalanceCount + MonthlyResponse.NegativeDaysCount;
                                currentMonthCashFlow.DaysBelow100Count = GetDaysBelow100Count(monthlyDailyBalance);
                                currentMonthCashFlow.IncDecBalance = PlaidcustomRound((currentMonthCashFlow.BeginingBalance - currentMonthCashFlow.EndingBalance), 2);
                                currentMonthCashFlow.AverageDailyBalancePercent = ReturnZeroIfInfinity(ADBPercentageCalculation(currentMonthCashFlow.AverageDailyBalance, currentMonthCashFlow.TotalDepositAmount));
                                currentMonthCashFlow.PeriodFrom = periodStringFrom;
                                currentMonthCashFlow.PeriodTo = periodStringTo;
                                MonthlyCashFlowsList.push(PlaidsortObject(currentMonthCashFlow));
                                isLastMonth = false;
                                if (Last_N_Month.indexOf(monthCheck) > -1 && TotalBrokerAGSDepositArray != null && TotalBrokerAGSDepositArray.length > 0) {
                                    AGSDeposit_N_Month = AGSSum(TotalBrokerAGSDepositArray);
                                }
                            }
                            currentMonth -= 1;
                            if (currentMonth < 0) {
                                currentYear -= 1;
                                currentMonth = 11;
                            }
                        }
                        var PlaidTransactionSummaryViewModel = {
                            CountOfMonthlyStatement: 0,
                            StartDate: '',
                            EndDate: '',
                            AverageDeposit: 0,
                            AnnualCalculatedRevenue: 0.00,
                            AverageWithdrawal: 0,
                            AverageDailyBalance: 0,
                            NumberOfNegativeBalance: 0,
                            NumberOfNSF: 0,
                            NSFAmount: 0,
                            LoanPaymentAmount: 0,
                            NumberOfLoanPayment: 0,
                            PayrollAmount: 0,
                            NumberOfPayroll: 0,
                            ChangeInDepositVolume: 0,
                            TotalCredits: 0,
                            TotalDebits: 0,
                            TotalCreditsCount: 0,
                            TotalDebitsCount: 0,
                            AvailableBalance: 0,
                            CurrentBalance: 0,
                            AverageBalanceLastMonth: 0,
                            MedianMonthlyIncome: 0,
                            MedianDailyBalance: 0,
                            MaxDaysBelow100Count: 0,
                            CVOfDailyDeposit: 0,
                            CVOfDailyBalance: 0,
                            AverageMonthlyRevenue: 0.00,
                            TotalRevenueAmount: 0.00,
                            TotalRevenueCount: 0,
                            PDFReportName: '',
                            BrokerAGS: 0,
                            CAPAGS: 0,
                            BrokerAGSText: ''
                        };
                        var objTransactionSummary = PlaidTransactionSummaryViewModel;
                        objTransactionSummary.PDFReportName = LastFourDigitAccountNumber + '.pdf';
                        objTransactionSummary.CountOfMonthlyStatement = MonthlyCashFlowsList.length;
                        objTransactionSummary.StartDate = getFormattedDate(StartDate);
                        objTransactionSummary.EndDate = getFormattedDate(EndDate);
                        var TotalRevenueAmount = 0,
                            TotalRevenueCount = 0;
                        var TotalDepositAmount = 0;
                        var TotalWithdrawalAmount = 0;
                        var TotalDailyAverageBalance = 0;
                        var TotalPayrollAmount = 0,
                            TotalLoanPaymentAmount = 0,
                            TotalNSFAmount = 0;
                        var TotalPayrollCount = 0,
                            TotalLoanPaymentCount = 0,
                            TotalNSFCount = 0;
                        var TotalDebitsCount = 0,
                            TotalCreditsCount = 0;
                        var TotalMonthlyIncomeArray = new Array();
                        MonthlyCashFlowsList.forEach(function (cashflow) {
                            TotalRevenueAmount += cashflow.TotalMonthlyRevenueAmount;
                            TotalRevenueCount += cashflow.TotalMonthlyRevenueCount;
                            TotalDepositAmount += cashflow.TotalDepositAmount;
                            TotalWithdrawalAmount += cashflow.TotalWithdrawalAmount;
                            TotalDailyAverageBalance += cashflow.AverageDailyBalance;
                            TotalPayrollAmount += cashflow.PayrollAmount;
                            TotalLoanPaymentAmount += cashflow.LoanPaymentAmount;
                            TotalNSFAmount += cashflow.NSFAmount;
                            TotalPayrollCount += cashflow.NumberOfPayroll;
                            TotalLoanPaymentCount += cashflow.NumberOfLoanPayment;
                            TotalNSFCount += cashflow.NumberOfNSF;
                            TotalCreditsCount += cashflow.DepositCount;
                            TotalDebitsCount += cashflow.WithdrawalCount;
                            TotalMonthlyIncomeArray.push(cashflow.TotalDepositAmount);
                        });
                        objTransactionSummary.TotalRevenueAmount = PlaidcustomRound((Math.abs(TotalRevenueAmount)), 2);
                        objTransactionSummary.TotalRevenueCount = TotalRevenueCount;
                        objTransactionSummary.AverageMonthlyRevenue = PlaidcustomRound((TotalRevenueAmount / MonthlyCashFlowsList.length), 2);
                        objTransactionSummary.MedianMonthlyIncome = GetMedianSingleArray(TotalMonthlyIncomeArray);
                        objTransactionSummary.MedianDailyBalance = GetMedianSingleArray(TotalDailyBalanceArray);
                        objTransactionSummary.MaxDaysBelow100Count = GetMaxDaysBelow100(MonthlyCashFlowsList);
                        objTransactionSummary.CVOfDailyBalance = PlaidcustomRound(DailyCoEfficient(TotalDailyBalanceArray), 2);
                        objTransactionSummary.CVOfDailyDeposit = PlaidcustomRound(DailyCoEfficient(TotalDailyDepositArray), 2);
                        objTransactionSummary.TotalCredits = TotalDepositAmount;
                        objTransactionSummary.TotalDebits = TotalWithdrawalAmount;
                        objTransactionSummary.TotalCreditsCount = TotalCreditsCount;
                        objTransactionSummary.TotalDebitsCount = TotalDebitsCount;
                        objTransactionSummary.AvailableBalance = account.AvailableBalance;
                        objTransactionSummary.CurrentBalance = account.CurrentBalance;
                        objTransactionSummary.AverageBalanceLastMonth = MonthlyCashFlowsList[0].AverageDailyBalance;
                        objTransactionSummary.AverageDeposit = PlaidcustomRound((TotalDepositAmount / MonthlyCashFlowsList.length), 2);
                        objTransactionSummary.AnnualCalculatedRevenue = PlaidcustomRound((TotalDepositAmount / MonthlyCashFlowsList.length) * 12, 2);
                        objTransactionSummary.AverageWithdrawal = PlaidcustomRound((TotalWithdrawalAmount / MonthlyCashFlowsList.length), 2);
                        objTransactionSummary.AverageDailyBalance = PlaidcustomRound((TotalDailyAverageBalance / MonthlyCashFlowsList.length), 2);
                        objTransactionSummary.NumberOfNegativeBalance = NegativeBalanceCount;
                        objTransactionSummary.NumberOfNSF = TotalNSFCount;
                        objTransactionSummary.NSFAmount = PlaidcustomRound(TotalNSFAmount, 2);
                        objTransactionSummary.LoanPaymentAmount = PlaidcustomRound((TotalLoanPaymentAmount), 2);
                        objTransactionSummary.NumberOfLoanPayment = TotalLoanPaymentCount;
                        objTransactionSummary.NumberOfPayroll = TotalPayrollCount;
                        objTransactionSummary.PayrollAmount = PlaidcustomRound((Math.abs(TotalPayrollAmount)), 2);
                        if (AGSDeposit_N_Month != 0) {
                            var Broker_AverageDepositVolume = AGSDeposit_N_Month / AGS_Month;
                            objTransactionSummary.BrokerAGS = PlaidcustomRound(Broker_AverageDepositVolume * 12, 2);
                        }
                        var BrokerAGSMonthlyLimit = 4;
                        if (MonthlyCashFlowsList.length < BrokerAGSMonthlyLimit) {
                            objTransactionSummary.BrokerAGS = 0;
                            objTransactionSummary.BrokerAGSText = 'Less than 3 months data';
                        }
                        if (TotalDailyDepositArray != null && TotalDailyDepositArray.length > 0) {
                            var CAP_AverageDepositVolume = AGSSum(TotalDailyDepositArray) / MonthlyCashFlowsList.length;
                            objTransactionSummary.CAPAGS = PlaidcustomRound(CAP_AverageDepositVolume * 12, 2);
                        }
                        var t = myCustomCashFlow;
                        t.MonthlyCashFlows = MonthlyCashFlowsList;
                        t.CategorySummary = CategorySummaryList;
                        t.TransactionList = TransactionList;
                        t.RecurringList = RecurringList;
                        t.MCARecurringList = MCARecurringList;
                        var objTemp = PlaidsortObject(objTransactionSummary);
                        t.TransactionSummary = objTemp;
                        t.GridReport = GetGridReport(MonthlyCashFlowsList,
                            account.AccountType,
                            account.AccountType,
                            account.AccountNumber);
                        cashFlowResult.CashFlow = t;
                    } else {
                        var PlaidTransactionSummaryViewModel = {
                            CountOfMonthlyStatement: 0,
                            StartDate: '',
                            EndDate: '',
                            AverageDeposit: 0,
                            AnnualCalculatedRevenue: 0.00,
                            AverageWithdrawal: 0,
                            AverageDailyBalance: 0,
                            NumberOfNegativeBalance: 0,
                            NumberOfNSF: 0,
                            NSFAmount: 0,
                            LoanPaymentAmount: 0,
                            NumberOfLoanPayment: 0,
                            PayrollAmount: 0,
                            NumberOfPayroll: 0,
                            ChangeInDepositVolume: 0,
                            TotalCredits: 0,
                            TotalDebits: 0,
                            TotalCreditsCount: 0,
                            TotalDebitsCount: 0,
                            AvailableBalance: 0,
                            CurrentBalance: 0,
                            AverageBalanceLastMonth: 0,
                            MedianMonthlyIncome: 0,
                            MedianDailyBalance: 0,
                            MaxDaysBelow100Count: 0,
                            CVOfDailyDeposit: 0,
                            CVOfDailyBalance: 0,
                            AverageMonthlyRevenue: 0.00,
                            TotalRevenueAmount: 0.00,
                            TotalRevenueCount: 0,
                            PDFReportName: '',
                            BrokerAGS: 0,
                            CAPAGS: 0,
                            BrokerAGSText: ''
                        };
                        var objTransactionSummary = PlaidTransactionSummaryViewModel;
                        objTransactionSummary.AvailableBalance = account.AvailableBalance;
                        objTransactionSummary.CurrentBalance = account.CurrentBalance;
                        var t = myCustomCashFlow;
                        var objTemp = PlaidsortObject(objTransactionSummary);
                        t.TransactionSummary = objTemp;
                        cashFlowResult.CashFlow = t;
                    }
                } else {
                    var PlaidTransactionSummaryViewModel = {
                        CountOfMonthlyStatement: 0,
                        StartDate: '',
                        EndDate: '',
                        AverageDeposit: 0,
                        AnnualCalculatedRevenue: 0.00,
                        AverageWithdrawal: 0,
                        AverageDailyBalance: 0,
                        NumberOfNegativeBalance: 0,
                        NumberOfNSF: 0,
                        NSFAmount: 0,
                        LoanPaymentAmount: 0,
                        NumberOfLoanPayment: 0,
                        PayrollAmount: 0,
                        NumberOfPayroll: 0,
                        ChangeInDepositVolume: 0,
                        TotalCredits: 0,
                        TotalDebits: 0,
                        TotalCreditsCount: 0,
                        TotalDebitsCount: 0,
                        AvailableBalance: 0,
                        CurrentBalance: 0,
                        AverageBalanceLastMonth: 0,
                        MedianMonthlyIncome: 0,
                        MedianDailyBalance: 0,
                        MaxDaysBelow100Count: 0,
                        CVOfDailyDeposit: 0,
                        CVOfDailyBalance: 0,
                        AverageMonthlyRevenue: 0.00,
                        TotalRevenueAmount: 0.00,
                        TotalRevenueCount: 0,
                        PDFReportName: '',
                        BrokerAGS: 0,
                        CAPAGS: 0,
                        BrokerAGSText: ''
                    };
                    var objTransactionSummary = PlaidTransactionSummaryViewModel;
                    objTransactionSummary.AvailableBalance = account.AvailableBalance;
                    objTransactionSummary.CurrentBalance = account.CurrentBalance;
                    var t = myCustomCashFlow;
                    var objTemp = PlaidsortObject(objTransactionSummary);
                    t.TransactionSummary = objTemp;
                    cashFlowResult.CashFlow = t;
                }
                var formattedAccountNumber = account.BankName;
                cashFlowResult.InstitutionName = account.BankName;
                DataAttributePostFix = account.Id;
                cashFlowResult.referenceNumber = payload.eventData.ReferenceNumber;
                cashFlowResult.AccountID = account.Id;
                cashFlowResult.AccountType = account.AccountType;
                cashFlowResult.AccountHeader = formattedAccountNumber + '-' + account.AccountNumber + '-' + account.AccountType;
                cashFlowResult.AccountNumber = account.AccountNumber;
                cashFlowResult.Source = account.Source;
                allAccountsCashflows = cashFlowResult;
                var resultKey = DataAttributePostFix;
                var Data = {};
                Data[resultKey] = allAccountsCashflows;
                return {
                    'result': result,
                    'detail': null,
                    'data': Data,
                    'rejectcode': '',
                    'exception': []
                };
            });
        } catch (e) {
            return {
                'result': 'Failed',
                'detail': null,
                'data': null,
                'rejectcode': '',
                'exception': ['Unable to verify : ' + e.message]
            };
        }
    } else {
        return {
            'result': 'Failed',
            'detail': null,
            'data': null,
            'rejectcode': '',
            'exception': []
        };
    }
    function RemoveSpace(str) {
        var result = '';
        var allStr = str.split(' ');
        for (var s = 0; s < allStr.length; s++) {
            result += allStr[s].toString();
        }
        return result;
    }
    function FilterOutlier(trans, handleSuccess) {
        handleSuccess(trans);
    }
    function GetRecurringSummaryByCategoryId(accountId, trans, handleSuccess) {
        var recurringSummary = [];
        var categoryId = '21002000';
        var daySpanFlag = 15;
        var unsureFlag = 3;
        if (trans != null && trans.length > 0) {
            var allDebitTransactions = trans.filter(function (item) {
                return item.amount > 0 && item.categoryId == categoryId && GetDaysBetweenDate(item.transactionDate) <= daySpanFlag;
            });
            var moduloGroups = GetGroupByData(allDebitTransactions, 'amount');
            if (moduloGroups != null) {
                for (var property in moduloGroups) {
                    if (moduloGroups[property].constructor === Array) {
                        var grp = moduloGroups[property];
                        if (grp != null && grp != undefined && grp.length < unsureFlag) { }
                        else {
                            var moduloSubGroups = GetGroupByData(grp, 'description');
                            if (moduloSubGroups != null) {
                                for (var p in moduloSubGroups) {
                                    if (moduloSubGroups[p].constructor === Array) {
                                        var grpSub = moduloSubGroups[p];
                                        if (grpSub != null && grpSub != undefined && grpSub.length >= unsureFlag) {
                                            var allMatchingTransactions = [];
                                            GetTransactionList(grpSub, function (trs) {
                                                allMatchingTransactions = trs;
                                            });
                                            var temp = {
                                                'Account': accountId,
                                                'Amount': grpSub[0].amount,
                                                'RoudingAmount': grpSub[0].amount,
                                                'Transactions': allMatchingTransactions,
                                                'ConfidenceLevel': 'High',
                                                'TotalTransactions': grpSub.length,
                                                'IsRecurring': true,
                                                'Status': 'RECURRING'
                                            };
                                            recurringSummary.push(temp);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        handleSuccess(recurringSummary);
    }
    function GetRecurringSummary(accountId, trans, handleSuccess) {
        var recurringSummary = [];
        var moduloRange = 5;
        var roundingMethodType = 2;
        var unsureFlag = 3;
        var minTransactionFlag = 5;
        var dayDifferenceFlag = 7;
        if (trans != null && trans.length > 0) {
            var allDebitTransactions = trans.filter(function (item) {
                return item.amount > 0;
            });
            for (var i = 0; i < allDebitTransactions.length; i++) {
                var rNumber = GetRoundingAmount(allDebitTransactions[i].amount, roundingMethodType);
                allDebitTransactions[i].RoudingAmount = rNumber;
                allDebitTransactions[i].RoudingAmountModulus = rNumber % moduloRange;
            }
            var moduloGroups = GetGroupByData(allDebitTransactions, 'RoudingAmountModulus');
            if (moduloGroups != null) {
                for (var property in moduloGroups) {
                    if (moduloGroups[property].constructor === Array) {
                        var grp = moduloGroups[property];
                        if (grp != null && grp != undefined && grp.length <= unsureFlag) { }
                        else {
                            var moduloSubGroups = GetGroupByData(grp, 'RoudingAmount');
                            if (moduloSubGroups != null) {
                                for (var p in moduloSubGroups) {
                                    if (moduloSubGroups[p].constructor === Array) {
                                        var grpSub = moduloSubGroups[p];
                                        if (grpSub != null && grpSub != undefined && grpSub.length >= minTransactionFlag) {
                                            FilterOutlier(grpSub, function (d) {
                                                grpSub = d;
                                            });
                                            grpSub.sort((a, b) => {
                                                var start = +new Date(a.transactionDate);
                                                var elapsed = +new Date(b.transactionDate) - start;
                                                return elapsed;
                                            });
                                            for (var i = 0; i < grpSub.length; i++) {
                                                if (grpSub[i + 1] != undefined) {
                                                    var diffDays = GetDaysBetweenTransaction(grpSub[i].transactionDate, grpSub[i + 1].transactionDate);
                                                    grpSub[i].diffDays = diffDays;
                                                }
                                            }
                                            var MinValue = GetMinMaxValue(grpSub, 'min');
                                            var MaxValue = GetMinMaxValue(grpSub, 'max');
                                            var rangeDifference = MaxValue - MinValue;
                                            if (rangeDifference > 0) {
                                                if (rangeDifference <= dayDifferenceFlag) {
                                                    var allMatchingTransactions = [];
                                                    GetTransactionList(grpSub, function (trs) {
                                                        allMatchingTransactions = trs;
                                                    });
                                                    var temp = {
                                                        'Account': accountId,
                                                        'Amount': grpSub[0].amount,
                                                        'RoudingAmount': grpSub[0].RoudingAmount,
                                                        'Transactions': allMatchingTransactions,
                                                        'ConfidenceLevel': 'High',
                                                        'Min': MinValue,
                                                        'Max': MaxValue,
                                                        'MaxMinDiff': rangeDifference,
                                                        'TotalTransactions': grpSub.length,
                                                        'IsRecurring': true,
                                                        'Status': 'RECURRING'
                                                    };
                                                    recurringSummary.push(temp);
                                                } else { }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        handleSuccess(recurringSummary);
    }
    function GetTransactionList(trans, handleSuccess) {
        var TransactionList = [];
        if (trans != null) {
            for (var t = 0; t < trans.length; t++) {
                var customDate = getCustomFormattedDate(trans[t].transactionDate);
                var transItem = {
                    'Date': customDate.Date,
                    'Description': trans[t].description.trim(),
                    'Amount': Math.abs(trans[t].amount),
                    'TransactionType': trans[t].amount < 0 ? 'Credit' : 'Debit',
                    'RunningBalance': 0,
                    'Month': customDate.Month,
                    'Year': customDate.Year,
                    'Credit': trans[t].Amount < 0 ? Math.abs(trans[t].Amount) : 0,
                    'Debit': trans[t].Amount > 0 ? Math.abs(trans[t].Amount) : 0
                };
                TransactionList.push(transItem);
            }
        }
        handleSuccess(TransactionList);
    }
    function CalcultationLastMonthsList(monthwiseData) {
        var array = [3, 6, 12];
        var dict = {};
        var currentMonth = parseInt((new Date().getMonth() + 1));
        var currentYear = parseInt(new Date().getFullYear());
        for (var i = 0; i < array.length; i++) {
            if (monthwiseData.length >= array[i]) {
                var monthDataList = monthwiseData.filter(function (item) {
                    var count = 0;
                    if (item.MonthInNumber != currentMonth && item.Year == currentYear && count < array[i]) {
                        count++;
                        return item;
                    }
                });
                var list = [];
                list.push(CalculateLastMonthDataAverage(monthDataList));
                list.push(CalculateLastMonthDataMin(monthDataList));
                list.push(CalculateLastMonthDataMax(monthDataList));
                dict[array[i]] = list;
            }
        }
        function CalculateLastMonthDataAverage(lastMonthlist) {
            var monthValue = lastMonthlist.length;
            var DepositsTemp = 0.00;
            var DepositsCountTemp = 0;
            var DepositAverageTemp = 0.00;
            var BeginingBalanceTemp = 0.00;
            var EndingBalanceTemp = 0.00;
            var IncDecTemp = 0.00;
            var ADBTemp = 0.00;
            var ADBPercentTemp = 0.00;
            var NegativeDaysTemp = 0;
            var NSFTemp = 0;
            for (var i = 0; i < monthValue; i++) {
                DepositsTemp += (lastMonthlist[i].TotalDepositAmount == null ? 0 : lastMonthlist[i].TotalDepositAmount);
                DepositsCountTemp += (lastMonthlist[i].DepositCount == null ? 0 : lastMonthlist[i].DepositCount);
                DepositAverageTemp += (lastMonthlist[i].AverageDeposit == null ? 0 : lastMonthlist[i].AverageDeposit);
                BeginingBalanceTemp += (lastMonthlist[i].BeginingBalance == null ? 0 : lastMonthlist[i].BeginingBalance);
                EndingBalanceTemp += (lastMonthlist[i].EndingBalance == null ? 0 : lastMonthlist[i].EndingBalance);
                IncDecTemp += (lastMonthlist[i].IncDecBalance == null ? 0 : lastMonthlist[i].IncDecBalance);
                ADBTemp += (lastMonthlist[i].AverageDailyBalance == null ? 0 : lastMonthlist[i].AverageDailyBalance);
                NegativeDaysTemp += (lastMonthlist[i].NumberOfNegativeBalance == null ? 0 : lastMonthlist[i].NumberOfNegativeBalance);
                NSFTemp += (lastMonthlist[i].NumberOfNSF == null ? 0 : lastMonthlist[i].NumberOfNSF);
            }
            var avgObject = {
                Name: '',
                GridRowHeader: 'AVG',
                Year: 0,
                BeginingBalance: BeginingBalanceTemp / monthValue,
                EndingBalance: EndingBalanceTemp / monthValue,
                DepositCount: DepositsCountTemp / monthValue,
                WithdrawalCount: 0,
                TotalDepositAmount: DepositsTemp / monthValue,
                TotalWithdrawalAmount: 0.00,
                AverageDailyBalance: ADBTemp / monthValue,
                AverageDeposit: DepositAverageTemp / monthValue,
                IncDecBalance: IncDecTemp / monthValue,
                AverageDailyBalancePercent: ADBPercentageCalculation(ADBTemp / monthValue, DepositsTemp / monthValue),
                PeriodFrom: '',
                PeriodTo: '',
                MonthInNumber: 0,
                FirstTransactionDate: '',
                EndTransactionDate: '',
                MinDepositAmount: 0.00,
                MaxDepositAmount: 0.00,
                MinWithdrawalAmount: 0.00,
                MaxWithdrawalAmount: 0.00,
                NumberOfNSF: NSFTemp / monthValue,
                NSFAmount: 0.00,
                LoanPaymentAmount: 0.00,
                NumberOfLoanPayment: 0,
                PayrollAmount: 0.00,
                NumberOfPayroll: 0,
                NumberOfNegativeBalance: NegativeDaysTemp / monthValue,
                CustomAttributes: '',
                DaysBelow100Count: 0,
                TotalMonthlyRevenueAmount: 0.00,
                TotalMonthlyRevenueCount: 0,
                PDFReportName: '',
                DateOfMonthlyCycle: ''
            };
            return avgObject;
        }
        function CalculateLastMonthDataMin(lastMonthlist) {
            var monthValue = lastMonthlist.length;
            var DepositsTemp = 0.00;
            var DepositsCountTemp = 0;
            var DepositAverageTemp = 0.00;
            var BeginingBalanceTemp = 0.00;
            var EndingBalanceTemp = 0.00;
            var IncDecTemp = 0.00;
            var ADBTemp = 0.00;
            var ADBPercentTemp = 0.00;
            var NegativeDaysTemp = 0;
            var NSFTemp = 0;
            DepositsTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'TotalDepositAmount');
            DepositsCountTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'DepositCount');
            DepositAverageTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'AverageDeposit');
            BeginingBalanceTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'BeginingBalance');
            EndingBalanceTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'EndingBalance');
            IncDecTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'IncDecBalance');
            ADBTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'AverageDailyBalance');
            NegativeDaysTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'NumberOfNegativeBalance');
            NSFTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'NumberOfNSF');
            ADBPercentTemp = GetMinMaxValueCashflow(lastMonthlist, 'min', 'AverageDailyBalancePercent');
            var avgObject = {
                Name: '',
                GridRowHeader: 'MIN',
                Year: 0,
                BeginingBalance: BeginingBalanceTemp,
                EndingBalance: EndingBalanceTemp,
                DepositCount: DepositsCountTemp,
                WithdrawalCount: 0,
                TotalDepositAmount: DepositsTemp,
                TotalWithdrawalAmount: 0.00,
                AverageDailyBalance: ADBTemp,
                AverageDeposit: DepositAverageTemp,
                IncDecBalance: IncDecTemp,
                AverageDailyBalancePercent: ADBPercentTemp,
                PeriodFrom: '',
                PeriodTo: '',
                MonthInNumber: 0,
                FirstTransactionDate: '',
                EndTransactionDate: '',
                MinDepositAmount: 0.00,
                MaxDepositAmount: 0.00,
                MinWithdrawalAmount: 0.00,
                MaxWithdrawalAmount: 0.00,
                NumberOfNSF: NSFTemp,
                NSFAmount: 0.00,
                LoanPaymentAmount: 0.00,
                NumberOfLoanPayment: 0,
                PayrollAmount: 0.00,
                NumberOfPayroll: 0,
                NumberOfNegativeBalance: NegativeDaysTemp,
                CustomAttributes: '',
                DaysBelow100Count: 0,
                TotalMonthlyRevenueAmount: 0.00,
                TotalMonthlyRevenueCount: 0,
                PDFReportName: '',
                DateOfMonthlyCycle: ''
            };
            return avgObject;
        }
        function CalculateLastMonthDataMax(lastMonthlist) {
            var monthValue = lastMonthlist.length;
            var DepositsTemp = 0.00;
            var DepositsCountTemp = 0;
            var DepositAverageTemp = 0.00;
            var BeginingBalanceTemp = 0.00;
            var EndingBalanceTemp = 0.00;
            var IncDecTemp = 0.00;
            var ADBTemp = 0.00;
            var ADBPercentTemp = 0.00;
            var NegativeDaysTemp = 0;
            var NSFTemp = 0;
            DepositsTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'TotalDepositAmount');
            DepositsCountTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'DepositCount');
            DepositAverageTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'AverageDeposit');
            BeginingBalanceTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'BeginingBalance');
            EndingBalanceTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'EndingBalance');
            IncDecTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'IncDecBalance');
            ADBTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'AverageDailyBalance');
            NegativeDaysTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'NumberOfNegativeBalance');
            NSFTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'NumberOfNSF');
            ADBPercentTemp = GetMinMaxValueCashflow(lastMonthlist, 'max', 'AverageDailyBalancePercent');
            var avgObject = {
                Name: '',
                GridRowHeader: 'MAX',
                Year: 0,
                BeginingBalance: BeginingBalanceTemp,
                EndingBalance: EndingBalanceTemp,
                DepositCount: DepositsCountTemp,
                WithdrawalCount: 0,
                TotalDepositAmount: DepositsTemp,
                TotalWithdrawalAmount: 0.00,
                AverageDailyBalance: ADBTemp,
                AverageDeposit: DepositAverageTemp,
                IncDecBalance: IncDecTemp,
                AverageDailyBalancePercent: ADBPercentTemp,
                PeriodFrom: '',
                PeriodTo: '',
                MonthInNumber: 0,
                FirstTransactionDate: '',
                EndTransactionDate: '',
                MinDepositAmount: 0.00,
                MaxDepositAmount: 0.00,
                MinWithdrawalAmount: 0.00,
                MaxWithdrawalAmount: 0.00,
                NumberOfNSF: NSFTemp,
                NSFAmount: 0.00,
                LoanPaymentAmount: 0.00,
                NumberOfLoanPayment: 0,
                PayrollAmount: 0.00,
                NumberOfPayroll: 0,
                NumberOfNegativeBalance: NegativeDaysTemp,
                CustomAttributes: '',
                DaysBelow100Count: 0,
                TotalMonthlyRevenueAmount: 0.00,
                TotalMonthlyRevenueCount: 0,
                PDFReportName: '',
                DateOfMonthlyCycle: ''
            };
            return avgObject;
        }
        function GetMinMaxValueCashflow(Values, search, item) {
            var result = 0;
            if (search == 'min') {
                if (Values.length > 0)
                    result = Values[0][item];
                for (var i = 0; i < Values.length; i++) {
                    if (Values[i][item] != undefined && Values[i][item] <= result) {
                        result = Values[i][item];
                    }
                }
            } else if (search == 'max') {
                for (var i = 0; i < Values.length; i++) {
                    if (Values[i][item] != undefined && Values[i][item] >= result) {
                        result = Values[i][item];
                    }
                }
            }
            if (result == null)
                result = 0;
            return result;
        }
        return dict;
    }
    function GetCategorySummary(trans, handleSuccess) {
        var predefinedCategoryTransactions = [];
        var CategorySummaryList = [];
        for (var c = 0; c < trans.length; c++) {
            if (allowedCategory.indexOf(trans[c].categoryId) > -1) {
                predefinedCategoryTransactions.push(trans[c]);
            }
        }
        var temp = [];
        var dayRange = 30;
        var allData = GetCategoryLookup();
        for (var n = 0; n < allData.length; n++) {
            var tempIndex = allData[n].CustomCategoryId;
            temp[tempIndex] = {
                TransactionCount: 0,
                CategoryName: allData[n].Name,
                TransactionTotal: 0.00,
                CategoryId: allData[n].PlaidCategoryId,
                CustomCategoryId: tempIndex,
                LastMonthTransactionCount: 0,
                LastMonthTransactionTotal: 0.00
            };
            CategorySummaryList.push(temp[tempIndex]);
        }
        predefinedCategoryTransactions.reduce(function (res, value) {
            var tIndex = parseInt(value.categoryId);
            var objTemp = temp.filter(function (e) {
                return e.CategoryId == tIndex;
            });
            var customIndex = 0;
            if (objTemp != null) {
                if (objTemp.length == 1) {
                    customIndex = objTemp[0].CustomCategoryId;
                } else {
                    if (value.amount > 0) {
                        customIndex = 4;
                    } else {
                        customIndex = 5;
                    }
                }
                temp[customIndex].TransactionTotal += Math.abs(parseFloat(value.amount));
                temp[customIndex].TransactionCount += 1;
                if (GetDaysBetweenDate(value.transactionDate) <= dayRange) {
                    temp[customIndex].LastMonthTransactionTotal += Math.abs(parseFloat(value.amount));
                    temp[customIndex].LastMonthTransactionCount += 1;
                }
            }
            return temp;
        }, {});
        handleSuccess(CategorySummaryList);
    }
    function GetCategoryLookup() {
        var myCat = [{
            'PlaidCategoryId': '21002000',
            'Name': 'ACH Credit',
            'CustomCategoryId': '5'
        }, {
            'PlaidCategoryId': '21002000',
            'Name': 'ACH Debit',
            'CustomCategoryId': '4'
        }, {
            'PlaidCategoryId': '10001000',
            'Name': 'Overdraft',
            'CustomCategoryId': '3'
        }, {
            'PlaidCategoryId': '21009000',
            'Name': 'Payroll',
            'CustomCategoryId': '1'
        }, {
            'PlaidCategoryId': '16003000',
            'Name': 'Loan Payment',
            'CustomCategoryId': '2'
        }, {
            'PlaidCategoryId': '10007000',
            'Name': 'NSF',
            'CustomCategoryId': '6'
        }
        ];
        return myCat;
    }
    function GetLastCalendarMonth(counter) {
        var today = new Date();
        var lastNMonths = [];
        for (var i = 1; i <= counter; i++) {
            var month = today.getMonth() - i;
            var year = today.getFullYear();
            if (month < 0) {
                month = month + 12;
                year = year - 1;
            }
            lastNMonths.push(month + '-' + year);
        }
        return lastNMonths;
    }
    function AGSSum(numbersArray) {
        if (numbersArray != null && numbersArray.length > 0) {
            return numbersArray.reduce(function (a, b) {
                return a + b
            });
        } else {
            return 0;
        }
    }
    function GetDaysBetweenTransaction(d1, d2) {
        var date1 = new Date(d1);
        var date2 = new Date(d2);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    }
    function GetRoundingAmount(amount, methodType) {
        var result = amount;
        switch (methodType) {
            case 1:
                result = Math.floor(amount);
                break;
            case 2:
                result = Math.ceil(amount);
                break;
        }
        return result;
    }
    function GetGroupByData(data, prop) {
        return data.reduce(function (groups, item) {
            var val = item[prop];
            groups[val] = groups[val] || [];
            groups[val].push(item);
            return groups;
        }, {});
    }
    function GetMinMaxValue(Values, search) {
        var result = 0;
        if (search == 'min') {
            result = 10000;
            for (var i = 0; i < Values.length; i++) {
                if (Values[i].diffDays != undefined && Values[i].diffDays <= result) {
                    if (Values[i].diffDays != 0) {
                        result = Values[i].diffDays;
                    }
                }
            }
        } else if (search == 'max') {
            for (var i = 0; i < Values.length; i++) {
                if (Values[i].diffDays != undefined && Values[i].diffDays >= result) {
                    result = Values[i].diffDays;
                }
            }
        }
        return result;
    }
    function GetDaysBetweenDate(d1) {
        var date1 = new Date(d1);
        var date2 = new Date();
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    }
    function getFormattedDate(date) {
        date = new Date(date);
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return month + '/' + day + '/' + year;
    }
    function getFormattedDateForMonthPeriod(date) {
        var options = {
            year: 'numeric',
            month: 'short',
            day: '2-digit'
        };
        return date.toLocaleDateString('en-US', options);
    }
    function getCustomFormattedDate(date) {
        date = new Date(date);
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        var MonthName = Plaidmonths[date.getMonth()];
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        var customData = {
            Date: month + '/' + day + '/' + year,
            Month: MonthName,
            Year: year
        };
        return customData;
    }
    function isWeekday(year, month, day) {
        var day = new Date(year, month, day).getDay();
        return day != 0 && day != 6;
    }
    function plaidGetDaysInMonth(month, year) {
        var TotalDaysInMonth = new Date(year, month + 1, 0).getDate();
        if (ExcludeWeekEnds == false) {
            return TotalDaysInMonth;
        } else {
            var weekdays = 0;
            for (var i = 0; i < TotalDaysInMonth; i++) {
                if (isWeekday(year, month, i + 1))
                    weekdays++;
            }
            return weekdays;
        }
    }
    function PlaidcustomRound(value, precision) {
        var multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    }
    function AverageDepositCalculation(totalDepositAmountMonths, countOfDeposits) {
        var resultDepositeAvg = totalDepositAmountMonths / countOfDeposits;
        return PlaidcustomRound(resultDepositeAvg, 2);
    }
    function ReturnZeroIfInfinity(numberToCheck) {
        return (numberToCheck == Infinity || isNaN(numberToCheck)) ? 0.00 : numberToCheck;
    }
    function ADBPercentageCalculation(averageDailyBalance, totalDepositAmount) {
        var resultADBPercentage = ((averageDailyBalance * 100) / totalDepositAmount);
        return PlaidcustomRound(resultADBPercentage, 2);
    }
    function PlaidsortObject(o) {
        var sorted = {},
            key,
            a = [];
        for (key in o) {
            if (o.hasOwnProperty(key)) {
                a.push(key);
            }
        }
        a.sort();
        for (key = 0; key < a.length; key++) {
            sorted[a[key]] = o[a[key]];
        }
        return sorted;
    }
    function Plaidpad(s) {
        return (s < 10) ? '0' + s : s;
    }
    function PlaidcustomDate(d) {
        return [Plaidpad(d.getMonth() + 1), Plaidpad(d.getDate()), d.getFullYear()].join('/');
    }
    function PlaidcalculateAvgBalanceOfMonth(dailyBalanceList, numberOfDaysInMonth) {
        var totalMonthlyDailyBalance = 0;
        var emptyCount = 1;
        for (var index = 30; index >= 0; index--) {
            if (dailyBalanceList[index] == undefined) {
                emptyCount++;
            } else {
                totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
                emptyCount = 1;
            }
        }
        return PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
    }
    function FindFirstIndexValue(arry) {
        var value = 0;
        var index = 0;
        arry.some(function (v, i) {
            value = v;
            index = i;
            return true;
        });

        return value;
    }
    function CalculateADB(dailyBalanceList, numberOfDaysInMonth, totalDailyBalance) {
        var response = {};
        var totalMonthlyDailyBalance = 0;
        var daysCounter = numberOfDaysInMonth - 1;
        var emptyCount = 1;
        var negativeDaysCount = 0;
        var IsFirstTransactionNegative = false;
        if (FindFirstIndexValue(dailyBalanceList) < 0) {
            IsFirstTransactionNegative = true;
        }
        for (var index = daysCounter; index >= 0; index--) {
            if (dailyBalanceList[index] == undefined) {
                emptyCount++;
            } else {
                if (dailyBalanceList[index] < 0) {
                    negativeDaysCount = negativeDaysCount + emptyCount;
                }
                totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
                emptyCount = 1;
            }
        }
        if (emptyCount > 1) {
            var lastDailyBalance = totalDailyBalance[totalDailyBalance.length - 1] * (emptyCount - 1);
            totalMonthlyDailyBalance += lastDailyBalance;
            if (IsFirstTransactionNegative == true) {
                negativeDaysCount = negativeDaysCount + (emptyCount - 1);
            }
        }
        response =
            {
                ADB: PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2),
                NegativeDaysCount: negativeDaysCount
            };
        return response;
    }
    function CalculateADBFirstMonth(dailyBalanceList, numberOfDaysInMonth) {
        var response = {};
        var totalMonthlyDailyBalance = 0;
        var negativeDaysCount = 0;
        var emptyCount = 1;
        var daysCounter = numberOfDaysInMonth - 1;
        for (var index = daysCounter; index >= 0; index--) {
            if (dailyBalanceList[index] == undefined) {
                emptyCount++;
            } else {
                if (dailyBalanceList[index] < 0) {
                    negativeDaysCount = negativeDaysCount + emptyCount;
                }
                totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
                emptyCount = 1;
            }
        }
        if (emptyCount > 1) {
            numberOfDaysInMonth = (numberOfDaysInMonth - emptyCount) + 1;
        }
        response =
            {
                ADB: PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2),
                NegativeDaysCount: negativeDaysCount
            };
        return response;
    }
    function CalculateADBLastMonth(dailyBalanceList, numberOfDaysInMonth) {
        var totalMonthlyDailyBalance = 0;
        var emptyCount = 1;
        var lastDailyBalance = 0;
        var IsLastBalance = false;
        var daysCounter = numberOfDaysInMonth - 1;
        for (var index = daysCounter; index >= 0; index--) {
            if (dailyBalanceList[index] == undefined) {
                emptyCount++;
            } else {
                if (IsLastBalance == false) {
                    IsLastBalance = true;
                    lastDailyBalance = dailyBalanceList[index];
                }
                totalMonthlyDailyBalance += (dailyBalanceList[index] * emptyCount);
                emptyCount = 1;
            }
        }
        if (emptyCount > 1) {
            emptyCount = emptyCount - 1;
            totalMonthlyDailyBalance += lastDailyBalance * emptyCount;
        }
        return PlaidcustomRound((totalMonthlyDailyBalance / numberOfDaysInMonth), 2);
    }
    function GetDaysBelow100Count(data) {
        var count = 0;
        data = RemoveUndefinedData(data);
        for (var c = 0; c < data.length; c++) {
            if (data[c] < 1000) {
                count = count + 1;
            }
        }
        return count;
    }
    function RemoveUndefinedData(data) {
        data = data.filter(function (element) {
            return element !== undefined;
        });
        return data;
    }
    function GetMedianSingleArray(data) {
        data = RemoveUndefinedData(data);
        var m = data.sort(function (a, b) {
            return a - b;
        });
        var middle = Math.floor((m.length - 1) / 2);
        if (m.length % 2) {
            return m[middle];
        } else {
            return (m[middle] + m[middle + 1]) / 2.0;
        }
    }
    function GetMaxDaysBelow100(data) {
        return Math.max.apply(Math, data.map(function (o) {
            return o.DaysBelow100Count;
        }));
    }
    function DailyCoEfficient(data, sum, count) {
        Math.mean = function (array) {
            return array.reduce(function (a, b) {
                return a + b;
            }) / array.length;
        };
        Math.stDeviation = function (array, mean) {
            var dev = array.map(function (itm) {
                return (itm - mean) * (itm - mean);
            });
            return Math.sqrt(dev.reduce(function (a, b) {
                return a + b;
            }) / (array.length - 1));
        };
        data = RemoveUndefinedData(data);
        if (data != null && data.length > 0) {
            var average = Math.mean(data);
            var standardDeviation = Math.stDeviation(data, average);
            if (standardDeviation != undefined && !isNaN(standardDeviation)) {
                return (standardDeviation / average) * 100;
            }
            return 0;
        } else {
            return 0;
        }
    }
    function GetGridReportFormat() {
        var gridReport = {
            'GridRowHeader': '',
            'GridRowOrder': 100,
            'ColumnValue': {
                'Deposits': 0.00,
                'DepositCount': 0,
                'DepositAverage': 0.00,
                'BeginningBalance': 0.00,
                'EndingBalance': 0.00,
                'IncDec': 0.00,
                'ADB': 0.00,
                'ADBPercentage': 0.00,
                'NegativeDays': 0,
                'NSF': 0,
                'Name': '',
                'Year': ''
            }
        };
        return gridReport;
    }
    function GetTotalForGridReport(arrayData, type) {
        var totalDepositsAmount = 0.00;
        var totalDepositsCount = 0;
        var totalADB = 0.00;
        var totalNegativeDays = 0;
        var totalNSF = 0;
        var beginningBalance = 0.00;
        var endingBalance = 0.00;
        var IncDec = 0.00;
        var ADBPercentage = 0.00;
        var DepositAverage = 0.00;
        var result = {
            totalDepositsAmount: 0.00,
            totalDepositsCount: 0,
            totalADB: 0.00,
            totalNegativeDays: 0,
            totalNSF: 0,
            beginningBalance: 0.00,
            endingBalance: 0.00,
            IncDec: 0.00,
            ADBPercentage: 0.00,
            DepositAverage: 0.00
        };
        if (type == 'AVG') {
            arrayData.forEach(function (objData) {
                totalDepositsAmount += objData.ColumnValue.Deposits;
                totalDepositsCount += objData.ColumnValue.DepositCount;
                DepositAverage += objData.ColumnValue.DepositAverage;
                totalADB += objData.ColumnValue.ADB;
                totalNegativeDays += objData.ColumnValue.NegativeDays;
                totalNSF += objData.ColumnValue.NSF;
                beginningBalance += objData.ColumnValue.BeginningBalance;
                endingBalance += objData.ColumnValue.EndingBalance;
                ADBPercentage += objData.ColumnValue.ADBPercentage;
                IncDec += objData.ColumnValue.IncDec;
            });
        } else if (type == 'MIN' || type == 'MAX') {
            totalDepositsAmount = arrayData[0].ColumnValue.Deposits;
            totalDepositsCount = arrayData[0].ColumnValue.DepositCount;
            DepositAverage = arrayData[0].ColumnValue.DepositAverage;
            totalADB = arrayData[0].ColumnValue.ADB;
            totalNegativeDays = arrayData[0].ColumnValue.NegativeDays;
            totalNSF = arrayData[0].ColumnValue.NSF;
            beginningBalance = arrayData[0].ColumnValue.BeginningBalance;
            endingBalance = arrayData[0].ColumnValue.EndingBalance;
            ADBPercentage = arrayData[0].ColumnValue.ADBPercentage;
            IncDec = arrayData[0].ColumnValue.IncDec;
            if (type == 'MIN') {
                for (var X = 0; X < arrayData.length; X++) {
                    if (totalDepositsAmount > arrayData[X].ColumnValue.Deposits) {
                        totalDepositsAmount = arrayData[X].ColumnValue.Deposits;
                    }
                    if (totalDepositsCount > arrayData[X].ColumnValue.DepositCount) {
                        totalDepositsCount = arrayData[X].ColumnValue.DepositCount;
                    }
                    if (DepositAverage > arrayData[X].ColumnValue.DepositAverage) {
                        DepositAverage = arrayData[X].ColumnValue.DepositAverage;
                    }
                    if (totalADB > arrayData[X].ColumnValue.ADB) {
                        totalADB = arrayData[X].ColumnValue.ADB;
                    }
                    if (totalNegativeDays > arrayData[X].ColumnValue.NegativeDays) {
                        totalNegativeDays = arrayData[X].ColumnValue.NegativeDays;
                    }
                    if (totalNSF > arrayData[X].ColumnValue.NSF) {
                        totalNSF = arrayData[X].ColumnValue.NSF;
                    }
                    if (beginningBalance > arrayData[X].ColumnValue.BeginningBalance) {
                        beginningBalance = arrayData[X].ColumnValue.BeginningBalance;
                    }
                    if (endingBalance > arrayData[X].ColumnValue.EndingBalance) {
                        endingBalance = arrayData[X].ColumnValue.EndingBalance;
                    }
                    if (ADBPercentage > arrayData[X].ColumnValue.ADBPercentage) {
                        ADBPercentage = arrayData[X].ColumnValue.ADBPercentage;
                    }
                    if (IncDec > arrayData[X].ColumnValue.IncDec) {
                        IncDec = arrayData[X].ColumnValue.IncDec;
                    }
                }
            } else if (type == 'MAX') {
                for (var Y = 0; Y < arrayData.length; Y++) {
                    if (totalDepositsAmount < arrayData[Y].ColumnValue.Deposits) {
                        totalDepositsAmount = arrayData[Y].ColumnValue.Deposits;
                    }
                    if (totalDepositsCount < arrayData[Y].ColumnValue.DepositCount) {
                        totalDepositsCount = arrayData[Y].ColumnValue.DepositCount;
                    }
                    if (DepositAverage < arrayData[Y].ColumnValue.DepositAverage) {
                        DepositAverage = arrayData[Y].ColumnValue.DepositAverage;
                    }
                    if (totalADB < arrayData[Y].ColumnValue.ADB) {
                        totalADB = arrayData[Y].ColumnValue.ADB;
                    }
                    if (totalNegativeDays < arrayData[Y].ColumnValue.NegativeDays) {
                        totalNegativeDays = arrayData[Y].ColumnValue.NegativeDays;
                    }
                    if (totalNSF < arrayData[Y].ColumnValue.NSF) {
                        totalNSF = arrayData[Y].ColumnValue.NSF;
                    }
                    if (beginningBalance < arrayData[Y].ColumnValue.BeginningBalance) {
                        beginningBalance = arrayData[Y].ColumnValue.BeginningBalance;
                    }
                    if (endingBalance < arrayData[Y].ColumnValue.EndingBalance) {
                        endingBalance = arrayData[Y].ColumnValue.EndingBalance;
                    }
                    if (ADBPercentage < arrayData[Y].ColumnValue.ADBPercentage) {
                        ADBPercentage = arrayData[Y].ColumnValue.ADBPercentage;
                    }
                    if (IncDec < arrayData[Y].ColumnValue.IncDec) {
                        IncDec = arrayData[Y].ColumnValue.IncDec;
                    }
                }
            }
        }
        result.totalDepositsAmount = totalDepositsAmount;
        result.totalDepositsCount = totalDepositsCount;
        result.DepositAverage = DepositAverage;
        result.totalADB = totalADB;
        result.totalNegativeDays = totalNegativeDays;
        result.totalNSF = totalNSF;
        result.beginningBalance = beginningBalance;
        result.endingBalance = endingBalance;
        result.ADBPercentage = ADBPercentage;
        result.IncDec = IncDec;
        return result;
    }
    function GetLastNMonthGridReport(gridData) {
        var fixedGridRowHeader = ['AVG', 'MIN', 'MAX'];
        var objGridDataNMonth = [];
        var groupData = gridData.filter(function (data) {
            return (data.GridRowOrder > 0);
        });
        if (groupData != null && groupData.length > 0) {
            fixedGridRowHeader.forEach(function (objRow) {
                var groupReport = GetGridReportFormat();
                groupReport.GridRowHeader = objRow;
                var AVGData = GetTotalForGridReport(groupData, objRow);
                if (objRow == 'AVG') {
                    groupReport.ColumnValue.Deposits = AVGData.totalDepositsAmount > 0 ? PlaidcustomRound(AVGData.totalDepositsAmount / groupData.length, 2) : 0.00;
                    groupReport.ColumnValue.DepositCount = AVGData.totalDepositsCount > 0 ? PlaidcustomRound(AVGData.totalDepositsCount / groupData.length, 2) : 0;
                    groupReport.ColumnValue.DepositAverage = AVGData.DepositAverage ? PlaidcustomRound(AVGData.DepositAverage / groupData.length, 2) : 0.00;
                    groupReport.ColumnValue.ADB = AVGData.totalADB > 0 ? PlaidcustomRound(AVGData.totalADB / groupData.length, 2) : 0.00;
                    groupReport.ColumnValue.NegativeDays = AVGData.totalNegativeDays > 0 ? PlaidcustomRound(AVGData.totalNegativeDays / groupData.length, 2) : 0;
                    groupReport.ColumnValue.NSF = AVGData.totalNSF > 0 ? PlaidcustomRound(AVGData.totalNSF / groupData.length, 2) : 0;
                    groupReport.ColumnValue.BeginningBalance = AVGData.beginningBalance > 0 ? PlaidcustomRound(AVGData.beginningBalance / groupData.length, 2) : 0;
                    groupReport.ColumnValue.EndingBalance = AVGData.endingBalance > 0 ? PlaidcustomRound(AVGData.endingBalance / groupData.length, 2) : 0;
                    groupReport.ColumnValue.ADBPercentage = AVGData.ADBPercentage > 0 ? PlaidcustomRound(AVGData.ADBPercentage / groupData.length, 2) : 0;
                    groupReport.ColumnValue.IncDec = AVGData.IncDec > 0 ? PlaidcustomRound(AVGData.IncDec / groupData.length, 2) : 0;
                    groupReport.ColumnValue.GridRowOrder = 1;
                } else {
                    groupReport.ColumnValue.Deposits = PlaidcustomRound(AVGData.totalDepositsAmount, 2);
                    groupReport.ColumnValue.DepositCount = PlaidcustomRound(AVGData.totalDepositsCount, 2);
                    groupReport.ColumnValue.DepositAverage = PlaidcustomRound(AVGData.DepositAverage, 2);
                    groupReport.ColumnValue.ADB = PlaidcustomRound(AVGData.totalADB, 2);
                    groupReport.ColumnValue.NegativeDays = PlaidcustomRound(AVGData.totalNegativeDays, 2);
                    groupReport.ColumnValue.NSF = PlaidcustomRound(AVGData.totalNSF, 2);
                    groupReport.ColumnValue.BeginningBalance = PlaidcustomRound(AVGData.beginningBalance, 2);
                    groupReport.ColumnValue.EndingBalance = PlaidcustomRound(AVGData.endingBalance, 2);
                    groupReport.ColumnValue.ADBPercentage = PlaidcustomRound(AVGData.ADBPercentage, 2);
                    groupReport.ColumnValue.IncDec = PlaidcustomRound(AVGData.IncDec, 2);
                }
                objGridDataNMonth.push(groupReport);
            });
        }
        return objGridDataNMonth;
    }
    function GetGridReport(monthlyReportList, accountName, accountType, accountNumber) {
        var objGridReport = {
            'AccountHolderName': '',
            'AccountType': '',
            'PDFReportName': '',
            'ReportList': [{
                'ReportHeader': '',
                'GridData': [{}
                ]
            }
            ]
        };
        var objGridDataAll = [];
        var MTDComparisionReport = {};
        var MTDPercentageReport = {};
        var MTDMonthFlag = 6;
        var monthlyReportGroup = [4, 7, 13];
        var reportList = [];
        var currentDate = new Date();
        var currentMonth = Plaidmonths[currentDate.getMonth()];
        var currentYear = currentDate.getFullYear();
        var skipMonth = currentMonth + '-' + currentYear;
        var monthCounter = 0;
        if (monthlyReportList !== undefined && monthlyReportList !== null && monthlyReportList.length > 0) {
            monthlyReportList.forEach(function (objMonthSummary) {
                if (objMonthSummary !== undefined) {
                    var currentMonthReport = GetGridReportFormat();
                    var gridNamePreFix = '';
                    if ((objMonthSummary.Name + '-' + objMonthSummary.Year) == skipMonth) {
                        gridNamePreFix = 'MTD ';
                        currentMonthReport.GridRowOrder = monthCounter;
                    }
                    else {
                        currentMonthReport.GridRowOrder = monthCounter + 2;
                    }
                    currentMonthReport.GridRowHeader = gridNamePreFix + objMonthSummary.Name + ' ' + objMonthSummary.Year;
                    currentMonthReport.ColumnValue.Deposits = PlaidcustomRound(objMonthSummary.TotalDepositAmount, 2);
                    currentMonthReport.ColumnValue.DepositCount = objMonthSummary.DepositCount;
                    currentMonthReport.ColumnValue.BeginningBalance = PlaidcustomRound(objMonthSummary.BeginingBalance, 2);
                    currentMonthReport.ColumnValue.EndingBalance = PlaidcustomRound(objMonthSummary.EndingBalance, 2);
                    currentMonthReport.ColumnValue.IncDec = PlaidcustomRound(objMonthSummary.EndingBalance - objMonthSummary.BeginingBalance, 2);
                    currentMonthReport.ColumnValue.ADB = PlaidcustomRound(objMonthSummary.AverageDailyBalance, 2);
                    currentMonthReport.ColumnValue.NegativeDays = objMonthSummary.NumberOfNegativeBalance;
                    currentMonthReport.ColumnValue.NSF = objMonthSummary.NumberOfNSF;
                    currentMonthReport.ColumnValue.Name = objMonthSummary.Name;
                    currentMonthReport.ColumnValue.Year = objMonthSummary.Year;
                    var ADBPercentage = 0;
                    var DepositAverage = 0;
                    if (objMonthSummary.TotalDepositAmount != null && objMonthSummary.TotalDepositAmount != undefined && objMonthSummary.TotalDepositAmount > 0) {
                        ADBPercentage = (objMonthSummary.AverageDailyBalance * 100) / objMonthSummary.TotalDepositAmount;
                        DepositAverage = objMonthSummary.TotalDepositAmount / objMonthSummary.DepositCount;
                    }
                    currentMonthReport.ColumnValue.DepositAverage = PlaidcustomRound(DepositAverage, 2);
                    currentMonthReport.ColumnValue.ADBPercentage = PlaidcustomRound(ADBPercentage, 2);
                    objGridDataAll.push(currentMonthReport);
                    monthCounter = monthCounter + 1;
                    if (monthCounter === MTDMonthFlag) {
                        var MTDData = objGridDataAll.filter(function (data) {
                            return (data.GridRowOrder == 0);
                        });
                        if (MTDData != null && MTDData != undefined && MTDData.length > 0) {
                            MTDData = MTDData[0];
                        }
                        else {
                            MTDData = null;
                        }
                        var MTDToCompareData = objGridDataAll.filter(function (data) {
                            return (data.GridRowOrder > 0);
                        });
                        var MTDTotal = GetTotalForGridReport(MTDToCompareData, 'AVG');
                        MTDComparisionReport = GetGridReportFormat();
                        MTDPercentageReport = GetGridReportFormat();
                        MTDComparisionReport.GridRowHeader = 'MTD COMPARISION';
                        MTDComparisionReport.ColumnValue.Deposits = MTDTotal.totalDepositsAmount > 0 ? PlaidcustomRound(MTDTotal.totalDepositsAmount / MTDToCompareData.length, 2) : 0.00;
                        MTDComparisionReport.ColumnValue.DepositCount = MTDTotal.totalDepositsCount > 0 ? PlaidcustomRound(MTDTotal.totalDepositsCount / MTDToCompareData.length, 2) : 0;
                        MTDComparisionReport.ColumnValue.DepositAverage = MTDTotal.totalDepositsAmount ? PlaidcustomRound(MTDTotal.totalDepositsAmount / MTDTotal.totalDepositsCount, 2) : 0.00;
                        MTDComparisionReport.ColumnValue.ADB = MTDTotal.totalADB > 0 ? PlaidcustomRound(MTDTotal.totalADB / MTDToCompareData.length, 2) : 0.00;
                        MTDComparisionReport.ColumnValue.NegativeDays = MTDTotal.totalNegativeDays > 0 ? PlaidcustomRound(MTDTotal.totalNegativeDays / MTDToCompareData.length, 2) : 0;
                        MTDComparisionReport.ColumnValue.NSF = MTDTotal.totalNSF > 0 ? PlaidcustomRound(MTDTotal.totalNSF / MTDToCompareData.length, 2) : 0;
                        MTDComparisionReport.ColumnValue.BeginningBalance = MTDTotal.beginningBalance > 0 ? PlaidcustomRound(MTDTotal.beginningBalance / MTDToCompareData.length, 2) : 0;
                        MTDComparisionReport.ColumnValue.EndingBalance = MTDTotal.endingBalance > 0 ? PlaidcustomRound(MTDTotal.endingBalance / MTDToCompareData.length, 2) : 0;
                        MTDComparisionReport.ColumnValue.IncDec = MTDTotal.IncDec > 0 ? PlaidcustomRound(MTDTotal.IncDec / MTDToCompareData.length, 2) : 0;
                        MTDComparisionReport.ColumnValue.ADBPercentage = MTDTotal.ADBPercentage > 0 ? PlaidcustomRound(MTDTotal.ADBPercentage / MTDToCompareData.length, 2) : 0;
                        MTDComparisionReport.GridRowOrder = 1;
                        MTDPercentageReport.GridRowHeader = 'MTD COMPARISION %';
                        if (MTDData != null && MTDData != undefined) {
                            MTDPercentageReport.ColumnValue.Deposits = MTDTotal.totalDepositsAmount > 0 && MTDComparisionReport.ColumnValue.Deposits > 0 ? PlaidcustomRound((MTDData.ColumnValue.Deposits * 100) / MTDComparisionReport.ColumnValue.Deposits, 2) : 0.00;
                            MTDPercentageReport.ColumnValue.DepositCount = MTDTotal.totalDepositsCount > 0 && MTDComparisionReport.ColumnValue.DepositCount > 0 ? PlaidcustomRound((MTDData.ColumnValue.DepositCount * 100) / MTDComparisionReport.ColumnValue.DepositCount, 2) : 0;
                            MTDPercentageReport.ColumnValue.DepositAverage = MTDTotal.totalDepositsCount > 0 && MTDComparisionReport.ColumnValue.DepositAverage > 0 ? PlaidcustomRound((MTDData.ColumnValue.DepositAverage * 100) / MTDComparisionReport.ColumnValue.DepositAverage, 2) : 0.00;
                            MTDPercentageReport.ColumnValue.ADB = MTDTotal.totalADB > 0 && MTDComparisionReport.ColumnValue.ADB > 0 ? PlaidcustomRound((MTDData.ColumnValue.ADB * 100) / MTDComparisionReport.ColumnValue.ADB, 2) : 0.00;
                            MTDPercentageReport.ColumnValue.NegativeDays = MTDTotal.totalNegativeDays > 0 && MTDComparisionReport.ColumnValue.NegativeDays > 0 ? PlaidcustomRound((MTDData.ColumnValue.NegativeDays * 100) / MTDComparisionReport.ColumnValue.NegativeDays, 2) : 0;
                            MTDPercentageReport.ColumnValue.NSF = MTDTotal.totalNSF > 0 && MTDComparisionReport.ColumnValue.NSF > 0 ? PlaidcustomRound((MTDData.ColumnValue.NSF * 100) / MTDComparisionReport.ColumnValue.NSF, 2) : 0;
                            MTDPercentageReport.ColumnValue.BeginningBalance = MTDTotal.beginningBalance > 0 && MTDComparisionReport.ColumnValue.NSF > 0 ? PlaidcustomRound((MTDData.ColumnValue.BeginningBalance * 100) / MTDComparisionReport.ColumnValue.BeginningBalance, 2) : 0;
                            MTDPercentageReport.ColumnValue.EndingBalance = MTDTotal.endingBalance > 0 && MTDComparisionReport.ColumnValue.EndingBalance > 0 ? PlaidcustomRound((MTDData.ColumnValue.EndingBalance * 100) / MTDComparisionReport.ColumnValue.EndingBalance, 2) : 0;
                            MTDPercentageReport.ColumnValue.IncDec = MTDTotal.IncDec > 0 && MTDComparisionReport.ColumnValue.IncDec > 0 ? PlaidcustomRound((MTDData.ColumnValue.IncDec * 100) / MTDComparisionReport.ColumnValue.IncDec, 2) : 0;
                            MTDPercentageReport.ColumnValue.ADBPercentage = MTDTotal.ADBPercentage > 0 && MTDComparisionReport.ColumnValue.ADBPercentage > 0 ? PlaidcustomRound((MTDData.ColumnValue.ADBPercentage * 100) / MTDComparisionReport.ColumnValue.ADBPercentage, 2) : 0;
                        }
                        MTDPercentageReport.GridRowOrder = 2;
                    }
                    if (monthlyReportGroup.indexOf(monthCounter) > -1) {
                        var reportItem = {
                            'ReportHeader': '',
                            'GridData': [{}
                            ],
                            'GridRowOrder': 1
                        };
                        reportItem.ReportHeader = 'Last ' + (monthCounter - 1).toString() + ' Months';
                        reportItem.GridData = GetLastNMonthGridReport(objGridDataAll);
                        reportList.push(reportItem);
                    }
                }
            });
        }
        objGridDataAll.push(MTDComparisionReport);
        objGridDataAll.push(MTDPercentageReport);
        objGridReport.AccountHolderName = accountName;
        objGridReport.AccountType = accountType;
        objGridReport.PDFReportName = accountNumber + '_Monthly_Grid.pdf';
        objGridDataAll = objGridDataAll.sort((a, b) => {
            return a.GridRowOrder - b.GridRowOrder;
        });
        var reportItem = {
            'ReportHeader': '',
            'GridData': [{}
            ],
            'GridRowOrder': 0
        };
        reportItem.ReportHeader = accountName;
        reportItem.GridData = objGridDataAll;
        reportList.push(reportItem);
        reportList = reportList.sort((a, b) => {
            return a.GridRowOrder - b.GridRowOrder;
        });
        objGridReport.ReportList = reportList;
        return objGridReport;
    }
}