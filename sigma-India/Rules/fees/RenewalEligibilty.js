function RenewalEligibilty(payload) {
	if (payload != null) {
		if (payload.AccualDetails != null && payload.AccualDetails.PaymentInfo != null) {
			if (payload.ScheduleDetails != null) {
				var repay = payload.ScheduleDetails.LoanAmount * payload.ScheduleDetails.FactorRate;
				var payoffPer = (payload.AccualDetails.PaymentInfo.CumulativeTotalPaid / repay) * 100;
				if (payoffPer > 80) {
					var result = {
						Result: true
					};
					return result;
				} else {
					var result = {
						Result: false
					};
					return result;
				}
			} else {
				var result = {
					Result: false,
					ErrorDetail: 'Schedule Detail not found'
				};
				return result;

			}

		} else {
			var result = {
				Result: false,
				ErrorDetail: 'Accrual Detail not found'
			};
			return result;
		}
	} else {
		var result = {
			Result: false,
			ErrorDetail: 'payload not found'
		};
		return result;
	}
}
