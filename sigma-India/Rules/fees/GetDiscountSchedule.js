function GetDiscountSchedule(payload) {
	var DiscountList = [];

	var discount = {
		PayOffFromDate:new Date(payload.LoanInformation.LoanOnboardedDate.Time) ,
		PayOffToDate: addDays(payload.LoanInformation.LoanOnboardedDate.Time, 90),
		Amount: (payload.LoanSchedule.LoanAmount * 6 / 100)

	};
	DiscountList.push(discount);
	var discount1 = {
		PayOffFromDate: addDays(payload.LoanInformation.LoanOnboardedDate.Time, 91),
		PayOffToDate: addDays(payload.LoanInformation.LoanOnboardedDate.Time, 120),
		Amount: (payload.LoanSchedule.LoanAmount * 4 / 100)

	};
DiscountList.push(discount1);
	var discount2 = {
		PayOffFromDate: addDays(payload.LoanInformation.LoanOnboardedDate.Time, 121),
		PayOffToDate: addDays(payload.LoanInformation.LoanOnboardedDate.Time, 150),
		Amount: (payload.LoanSchedule.LoanAmount * 2 / 100)

	};
	DiscountList.push(discount2);
function addDays(date, days) {
		var result = new Date(date);
		result.setDate(result.getDate() + days);
		return result;
	}
	return DiscountList;
}
