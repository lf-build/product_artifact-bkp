function GetFeeSchedule(payload) {
	var FeesList = [];
	var scheduledStartDate = new Date(payload.LoanSchedule.FirstPaymentDate.Time);
	for (var i = 0; i < 6; i++) {
			var fee = {
				FeeId: 'MAINTENANCE_FEE',
				FeeType: 'Scheduled',
				FeeName: 'MAINTENANCE FEE',
				FeeAmount: 10,
				FeeDueDate: new Date(scheduledStartDate.setMonth(scheduledStartDate.getMonth() + 1)),
				AutoSchedulePayment: true,
				Events: ['FEE_DUE'],
				IsProcessed: false
			};	
			FeesList.push(fee);
		}
	return FeesList;
}