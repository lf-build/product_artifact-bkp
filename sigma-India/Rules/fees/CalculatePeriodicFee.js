function CalculatePeriodicFee(payload) {
	var feeAmount = 0;
	if(payload == null || payload.LoanSchedule == null){
		return feeAmount;
	}
	var paymentFrequency = payload.LoanSchedule.PaymentFrequency;
	switch(paymentFrequency) {
		case 'daily':
			feeAmount = 2;
			break;
		case 'weekly':
			feeAmount = 5;
			break;
		case 'biweekly':
			feeAmount = 8;
			break;
		case 3:
			feeAmount = 10;
			break;
		case 'quarterly':
			feeAmount = 15;
			break;
		case 'yearly':
			feeAmount = 20;
			break;
		case 'semiyearly':
			feeAmount = 25;
			break;
		default:
			break;
	}
	return feeAmount;
}