function EMIScheduleEmailData(payload) {
	var self = this;
	if (payload) {
		var eventDetails = payload.eventData.EventData;
        var loanNo = eventDetails.LoanNumber;
        var bankName='';
        var applicationService = self.call('loanmanagement');
        var bankService =self.call('loanbankdetails');
		try {
			return applicationService.loanDetails(loanNo).then(function (data) {
                bankService.loanDetails(loanNo).then(function (data) { bankName=data.name;  });
				var url = '{{server_url}}:{{borrower_port}}/login';
				var queryStringData = '?token=';
				var result = {
					Email:data.primaryApplicantDetails.Emails[0].emailAddress,
					Name: data.primaryApplicantDetails.name.first,
					ContactName: data.primaryApplicantDetails.name.first,
					LoanNumber: data.loanNumber,
                    LegalBusinessName: data.primaryBusinessDetails.businessName,
                    ScheduleDate:data.ScheduleDate,
                    Nameofbank:bankName,
					Logo: '{{Logo_url}}',
					url: url,
					queryString: queryStringData
				};
				return {
					result: 'Passed',
					detail: null,
					data: result,
					rejectcode: '',
					exception: []
				};
			});
		} catch (e) {
			var errorData = [];
			errorData.push(e);
			return {
				result: 'Failed',
				detail: null,
				data: '',
				rejectcode: '',
				exception: errorData
			};
		}
	} else {
		var errorData = [];
		return {
			result: 'Failed',
			detail: null,
			data: '',
			rejectcode: '',
			exception: errorData
		};
	}
}