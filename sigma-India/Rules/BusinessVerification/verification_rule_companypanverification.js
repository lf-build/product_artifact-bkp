function verification_rule_companypanverification(payload) {
	var result = 'Failed';
	var Data = {};
	if (payload != null && payload != undefined) {
		Data = payload.companyPANVerificationData[0];
		if (Data != null && Data.IsVerificationDone == true) {
			result = 'Passed';
		}
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}