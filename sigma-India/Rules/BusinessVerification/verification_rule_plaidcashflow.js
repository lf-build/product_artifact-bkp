function verification_rule_plaidcashflow(payload) {
	var result = 'Passed';
	var IsNumberOfDepositPerMonth = true;
	var IsNumberOfNegativeDays = true;
	var IsNumberOfNSF = true;
	var IsAverageBalance = true;
	var IsMonthlyDepositAmount = true;
	var IsAverageDepositSize = true;
	var IsNumberOfDeposit = true;
	var IsZeroBalance = false;
	var MinNumberOfDepositPerMonth = 3;
	var MaxNSF = 12;
	var MinAverageDailyBalance = 1000;
	var MinNumberOfDeposit = 3;
	var AvgMonthlyDeposits = 10000;
	var ZeroBalance = 0;
	var AccountID = '',
		InstitutionName = '',
		AccountType = '';
	var errorData = [];
	var errorMessage;
	var Data = {};
	if (payload != null) {
		try {
			var SelectedAccountData = payload.cashflowVerificationData[0].SelectedAccountData;
			if (SelectedAccountData != null) {
				var selectedCashflow = SelectedAccountData[0];
				if (selectedCashflow != null) {
					AccountID = selectedCashflow.AccountNumber;
					InstitutionName = selectedCashflow.InstitutionName;
					AccountType = selectedCashflow.AccountType;
					if (selectedCashflow.CashFlow != null && selectedCashflow.CashFlow.TransactionSummary != null) {
						var input = selectedCashflow.CashFlow.TransactionSummary;
						if (input != null) {
							if (input.AverageDeposit < AvgMonthlyDeposits) {
								IsAverageDepositSize = false;
								result = 'Failed';
							}
							if (input.AverageDepositCount < MinNumberOfDeposit) {
								IsNumberOfDeposit = false;
								result = 'Failed';
							}
							if (input.AverageDailyBalance < MinAverageDailyBalance) {
								IsAverageBalance = false;
								result = 'Failed';
							}
							if (input.AverageNSFCount > 0) {
								if (input.AverageNSFCount > MaxNSF) {
									IsNumberOfNSF = false;
									result = 'Failed';
								}
							}
							if (input.CurrentBalance <= ZeroBalance) {
								IsZeroBalance = true;
								result = 'Failed';
							}
							Data = {
								'AccountID': AccountID,
								'InstitutionName': InstitutionName,
								'AccountType': AccountType,
								'IsNumberOfDepositPerMonth': IsNumberOfDepositPerMonth,
								'IsNumberOfNSF': IsNumberOfNSF,
								'IsAverageBalance': IsAverageBalance,
								'IsMonthlyDepositAmount': IsMonthlyDepositAmount,
								'IsAverageDepositSize': IsAverageDepositSize,
								'IsNumberOfDeposit': IsNumberOfDeposit,
								'IsZeroBalance': IsZeroBalance
							};
						}
					}
				} else {
					errorMessage = 'plaidCashflow information is Null : Unable to verify';
					errorData.push(errorMessage);
					result = 'Failed';
				}
			} else {
				errorMessage = 'plaidCashflow information is Null : Unable to verify';
				errorData.push(errorMessage);
				result = 'Failed';
			}
		} catch (e) {
			errorMessage = 'Unable to verify : ' + e.message;
			errorData.push(errorMessage);
			result = 'Failed';
		}
	} else {
		result = 'Failed';
		errorMessage = 'Payload is not available';
		errorData.push(errorMessage);
	}
	return {
		'result': result,
		'detail': '',
		'data': Data,
		'rejectcode': '',
		'exception': errorData
	};
}