function verification_rule_signedcontractverification(payload) {
    var SignedDoc = '';
    var result = 'Failed';
    function VerifyInput(payload) {
        try {
            if (payload != null && payload != undefined) {
                var objData = payload.signedContractVerificationData[0];
                if (objData != null) {
                    SignedDoc = objData.signedDoc;
                    if (SignedDoc == true) {
                        result = 'Passed';
                    }
                }
            }
            var Data = {
                'SignedDoc': SignedDoc
            };
            return {
                'result': result,
                'detail': null,
                'data': Data,
                'rejectcode': '',
                'exception': []
            };
        } catch (e) {
            return {
                'result': 'Failed',
                'detail': 'Unable to verify',
                'data': objData,
                'rejectcode': '',
                'exception': []
            };
        }
    }
    return VerifyInput(payload);
}