function verification_rule_pan(payload) {
	var result = 'Failed';
	if (payload != null && payload != undefined) {
		var objData = payload.panData[0];
		var applicationData = payload.application[0];
		var fullName = jsUcfirst(applicationData.owners[0].FirstName) + ' ' + jsUcfirst(applicationData.owners[0].LastName);
		var resultData = objData.PanData.result;
		if (resultData != null && resultData != undefined) {
			if (resultData.name != '' && resultData.name != null)
			{
				if(fullName==jsUcfirst(resultData.name))
				{
					result = 'Passed';
				}
			}
				
		}
		var Data = {
			'Result': resultData,
			'FullName':fullName
		};
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	}
	function jsUcfirst(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	};
}