function verification_rule_applicationdataverification_manual(payload) {
    try {
        var verificationData = null;
        var result = 'Failed';
        if (payload != null && payload != undefined) {
            var objData = payload.applicationDataVerificationManualData[0];
            if (objData != null) {
                verificationData = objData.IsVerificationDone;
                if (verificationData == true) {
                    result = 'Passed';
                }
            }
        }
        var Data = {
            'VerificationData': verificationData
        };
        return {
            'result': result,
            'detail': null,
            'data': Data,
            'rejectcode': '',
            'exception': []
        };
    } catch (e) {
        return {
            'result': 'Failed',
            'detail': 'Unable to verify',
            'data': null,
            'rejectcode': '',
            'exception': e.message
        };
    }
}