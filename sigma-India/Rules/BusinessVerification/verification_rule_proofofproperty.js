function verification_rule_proofofproperty(payload) {
 try
 {
 var result = 'Failed';	
	
	if (payload != null && payload != undefined) {		
     
		var objData = payload.proofOfPropertyOwnerShipManualData;
        if(objData.IsPGOnRent == false  && objData.IsPGGoodStanding == true && objData.IsRenewLeaseOption == true && objData.IsSubmittedMortgage == true)
        {
            result = 'Passed';
        }
        else
        {
            result = 'Failed';
        }       
			
	}	
	
     return {
        'result': result,
        'detail': null,
        'data': objData,
        'rejectcode': '',
        'exception': []
    };
    } catch (e) {
           return {
        'result': 'Failed',
        'detail': ['Unable to verify'],
        'data': objData,
        'rejectcode': '',
        'exception': []
    };
      
    }
}