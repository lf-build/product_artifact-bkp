function verification_rule_merchantstatementmanual(payload) {
	try {
		var result = 'Failed';

		if (payload != null && payload != undefined) {

			var objData = payload.merchantStatementVerificationData;
			if (objData.IsVerify == true) {
				result = true;
			} else {
				result = 'Failed';
			}

		}

		return {
			'result': result,
			'detail': null,
			'data': objData,
			'rejectcode': '',
			'exception': []
		};
	} catch (e) {
		return {
			'result': 'Failed',
			'detail': 'Unable to verify',
			'data': objData,
			'rejectcode': '',
			'exception': []
		};

	}
}