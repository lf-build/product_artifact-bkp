function verification_rule_aadhaar(payload) {
	var result = 'Passed';
	if (payload != null && payload != undefined) {
		var objData = payload.aadhaarData[0];
		var statusCode = objData.AadhaarData.AadhaarStatusCode;
		var statusResult = objData.AadhaarData.result.success;
		if (statusResult == false) {
			result = 'Failed';
		}
		var Data = {
			'StatusCode': statusCode,
			'statusResult': statusResult
		};
		return {
			'result': result,
			'detail': null,
			'data': Data,
			'rejectcode': '',
			'exception': []
		};
	}
}