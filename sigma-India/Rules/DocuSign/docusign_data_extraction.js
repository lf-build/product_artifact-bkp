function docusign_data_extraction(input) {
    var response = {
        'SignerEmail': null,
        'DocumentBytes': null,
        'Status': null,
        'EntityId': null,
        'EntityType': 'application',
        'DocumentName': null
    };
    var Status = '';
    var DocumentBytes = new Array();
    var SignerEmail = new Array();
    var EntityId = '';
    var DocumentName = new Array();
    if (input !== null && input !== undefined) {
        if (input.DocuSignEnvelopeInformation !== undefined && input.DocuSignEnvelopeInformation !== null) {
            var docuSignEnvelopeInformation = input.DocuSignEnvelopeInformation;
            if (docuSignEnvelopeInformation.EnvelopeStatus !== undefined && docuSignEnvelopeInformation.EnvelopeStatus !== null) {
                var envelopeStatus = docuSignEnvelopeInformation.EnvelopeStatus;
                Status = envelopeStatus.Status;
                if (envelopeStatus.RecipientStatuses !== undefined && envelopeStatus.RecipientStatuses !== null) {
                    var recipientStatuses = envelopeStatus.RecipientStatuses;
                    if (recipientStatuses.RecipientStatus !== undefined && recipientStatuses.RecipientStatus !== null) {
                        if (recipientStatuses.RecipientStatus.constructor === Array) {
                            recipientStatuses.RecipientStatus.forEach(function (obj) {
                                SignerEmail.push(obj.Email);
                            });
                        } else {
                            SignerEmail.push(recipientStatuses.RecipientStatus.Email);
                        }
                    }
                }
                if (envelopeStatus.CustomFields !== undefined && envelopeStatus.CustomFields !== null) {
                    if (envelopeStatus.CustomFields.CustomField !== undefined && envelopeStatus.CustomFields.CustomField !== null) {
                        EntityId = envelopeStatus.CustomFields.CustomField.Value;
                    }
                }
            }
            if (docuSignEnvelopeInformation.DocumentPDFs !== undefined && docuSignEnvelopeInformation.DocumentPDFs !== null) {
                if (docuSignEnvelopeInformation.DocumentPDFs.DocumentPDF !== undefined && docuSignEnvelopeInformation.DocumentPDFs.DocumentPDF !== null) {

                    var pdf = docuSignEnvelopeInformation.DocumentPDFs.DocumentPDF;
                    if (pdf.constructor === Array) {
                        docuSignEnvelopeInformation.DocumentPDFs.DocumentPDF.forEach(function (obj) {
                            DocumentBytes.push(obj.PDFBytes);
                            DocumentName.push(obj.Name + '.pdf');
                        });
                    } else {
                        DocumentName.push(pdf.Name + '.pdf');
                        DocumentBytes.push(pdf.PDFBytes);
                    }
                }
            }
        }
    }
    response = {
        'SignerEmail': SignerEmail,
        'DocumentBytes': DocumentBytes,
        'Status': Status,
        'EntityId': EntityId,
        'EntityType': 'application',
        'DocumentName': DocumentName
    };
    return response;
}