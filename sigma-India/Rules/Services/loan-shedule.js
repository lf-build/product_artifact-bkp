function loanManagement() {
    var self = this;
    var baseUrl = 'http://{{internal_ip}}:7094';

    return {
        getLoanSchedule: function (loanNumber) {
            var url = [baseUrl, loanNumber, 'schedule'].join('/');
            return self.http.get(url);
        },

        getLoanNumber: function (applicationNumber) {
            var url = [baseUrl, 'application', applicationNumber].join('/');
            return self.http.get(url);
        }
    };
}