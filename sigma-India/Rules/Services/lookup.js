
function lookup() {
    var self = this;
    var baseUrl = 'http://lookup-service:5000';

    return {
        get: function (name) {
            var url = [baseUrl, name].join('/');
            console.log(url);
            console.log(self.http.get(url));
            return self.http.get(url);
        },
        getWithKey: function (name, key) {
            var url = [baseUrl, name, key].join('/');
            return self.http.get(url);
        }
    };
}