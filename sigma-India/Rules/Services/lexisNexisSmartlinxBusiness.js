function lexisNexisSmartlinxBusiness(entityType, entityId) {
	var self = this;
	var eventHub = self.call("eventHub");
	var applicationService = self.call("application");
	var businessReportService = self.call("businessReport");
	var callService = function (entityType, entityId, legalBusinessName, addressLine1, city, state, zipCode) {
		return businessReportService.createReport(entityType, entityId, legalBusinessName, addressLine1, city, state, zipCode);
	};
	return applicationService.getWithApplicant(entityId).then(function (applicationData) {
		var applicant = applicationData.applicant;
		var primaryAddress = applicationData.application.primaryAddress;
		return callService(entityType, entityId, applicant.legalBusinessName, primaryAddress.addressLine1, primaryAddress.city, primaryAddress.state, primaryAddress.zipCode).then(function (result) {
			eventHub.publish("LexisNexisSmartlinxBusinessCalled", {
				EntityType: entityType,
				ReferenceNumber: "",
				EntityId: entityId,
				Response: result
			}).then(function () {
				return result;
			})
		})
	})
}
