function drawDown() {
    var self = this;
    var baseUrl = 'http://{{internal_ip}}:7088';

    return {
        getByLoCNumber: function (locNumber) {
            var url = [baseUrl,locNumber,'loc'].join('/');
            return self.http.get(url);
        }
    };
}