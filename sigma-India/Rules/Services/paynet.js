function paynetService() {
    var self = this;
    var baseUrl = '{{syndication_paynet_internal_url}}';

    return {
        //TODO: STATE TO STATE CODE?
        search: function (entityType, entityId, taxId, companyName, streetName, city, state, zip) {
            var url = [baseUrl, entityType, entityId, 'paynet', 'company', 'search'].join('/');
            var data = {
                CompanyName: companyName,
                City: city,
                StateCode: state.substring(0, 2),
                TaxId: taxId,
                UserField: "",
                NewMatchThreshold: null,
                Alias: null,
                Address: streetName,
                Phone: ""
            }
            return self.http.post(url, data);
        },
        getReport: function (entityType, entityId, payNetId) {
            var url = [baseUrl, entityType, entityId, 'paynet', 'company', 'report'].join('/');
            var data = {
                PaynetId: payNetId,
                ReportFormat: "BNDL",
                ProductType: "CHR"
            }
            return self.http.post(url, data);
        }
    };
}