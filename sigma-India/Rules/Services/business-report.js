function businessReport() {
    var self = this;
    var baseUrl = 'http://{{internal_ip}}:7081';

    return {
        createReport: function (entityType, entityId, legalBusinessName, addressLine1, city, state, zipCode) {
            var url = [baseUrl, entityType, entityId].join('/');
            return self.http.post(url, {
                "CompanyName": legalBusinessName,
                "Address": {
                    "city": city,
                    "state": state,
                    "streetAddress1": addressLine1,
                    "zip5": zipCode
                },
                "Radius": 25
            });
        }
    };
}