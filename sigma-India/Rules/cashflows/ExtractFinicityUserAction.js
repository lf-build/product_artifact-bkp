function ExtractFinicityUserAction(input) {
    function GetFinicityData(input) {
        var errorData = [];
        var AggregationStatusCodes = {
            103: 'Invalid Credentials',
            108: 'User Action Required',
            109: 'User Action Required',
            185: 'Missing or Incorrect MFA Answer',
            187: 'Missing or Incorrect MFA Answer',
            931: 'Bank security requires a one time passcode for every connection.',
            936: 'Customers language preference is not supported for aggregation.',
            11: 'MFA Question Required'
        };
        try {
            var lstAccounts = [];
            var data = {};
            if (input != null) {
                var cashflowResponse = input.Response;
                var cashflowRequest = input.Request;
                if (cashflowResponse.hasOwnProperty('MFASessionId')) {
                    if (cashflowRequest != null) {
                        var objAccountreq = {
                            ProviderAccountId: '',
                            AvailableBalance: 0,
                            CurrentBalance: 0,
                            BankName: '',
                            Institutionid: '',
                            AccountType: '',
                            AccountNumber: '',
                            Source: '',
                            IsCashflowAccount: false,
                            IsFundingAccount: false,
                            BalanceDate: '',
                            AggregationStatusCode: 0,
                            AggregationStatusDescription: '',
                            AggregationAttemptDate: '',
                            CustomerId: ''
                        };
                        objAccountreq.Source = 'Finicity';
                        objAccountreq.ProviderAccountId = cashflowRequest.id;
                        objAccountreq.Institutionid = cashflowRequest.Institutionid;
                        objAccountreq.AccountType = cashflowRequest.type;
                        objAccountreq.AccountNumber = cashflowRequest.number;
                        objAccountreq.AggregationStatusCode = cashflowRequest.aggregationStatusCode;
                        if (cashflowRequest.aggregationStatusCode != null && cashflowRequest.aggregationStatusCode != '') {
                            objAccountreq.AggregationStatusDescription = AggregationStatusCodes[cashflowRequest.aggregationStatusCode];
                        }
                        if (cashflowRequest.aggregationAttemptDate != null) {
                            objAccountreq.AggregationAttemptDate = ConvertEPOCDate(cashflowRequest.aggregationAttemptDate);
                        }
                        objAccountreq.CustomerId = cashflowRequest.customerId;
                        if (cashflowRequest.Detail != null && cashflowRequest.Detail.availableBalanceAmount != null) {
                            objAccountreq.AvailableBalance = cashflowRequest.Detail.availableBalanceAmount;
                        }
                        objAccountreq.CurrentBalance = cashflowRequest.balance;
                        if (cashflowRequest.RoutingNumber != null && cashflowRequest.RoutingNumber != '') {
                            objAccountreq.RoutingNumber = cashflowRequest.RoutingNumber;
                        }
                        if (cashflowRequest.balanceDate != null) {
                            objAccountreq.BalanceDate = ConvertEPOCDate(cashflowRequest.balanceDate)
                        }
                        lstAccounts.push(objAccountreq);
                    }
                } else if (cashflowResponse.hasOwnProperty('questions') && !cashflowResponse.hasOwnProperty('MFASessionId')) {
                    if (cashflowRequest != null) {
                        var objAccountreq = {
                            ProviderAccountId: '',
                            AvailableBalance: 0,
                            CurrentBalance: 0,
                            BankName: '',
                            Institutionid: '',
                            AccountType: '',
                            AccountNumber: '',
                            Source: '',
                            IsCashflowAccount: false,
                            IsFundingAccount: false,
                            BalanceDate: '',
                            AggregationStatusCode: 0,
                            AggregationStatusDescription: '',
                            AggregationAttemptDate: '',
                            CustomerId: ''
                        };
                        objAccountreq.Source = 'Finicity';
                        objAccountreq.ProviderAccountId = cashflowRequest.AccountId;
                        if (cashflowRequest.InstitutionId != null) {
                            objAccountreq.Institutionid = cashflowRequest.InstitutionId;
                        }
                        objAccountreq.CustomerId = cashflowRequest.CustomerId;
                        objAccountreq.AggregationStatusCode = 11;
                        objAccountreq.AggregationStatusDescription = AggregationStatusCodes[11];
                        lstAccounts.push(objAccountreq);
                    }
                } else {
                    if (cashflowResponse.CustomerAccountsDetails != null && cashflowResponse.CustomerAccountsDetails.length > 0) {
                        cashflowResponse.CustomerAccountsDetails.forEach(function(account) {
                            var objAccount = {
                                ProviderAccountId: '',
                                AvailableBalance: 0,
                                CurrentBalance: 0,
                                BankName: '',
                                AccountType: '',
                                AccountNumber: '',
                                Source: '',
                                IsCashflowAccount: false,
                                IsFundingAccount: false,
                                Institutionid: '',
                                BalanceDate: '',
                                AggregationStatusCode: 0,
                                AggregationStatusDescription: '',
                                AggregationAttemptDate: '',
                                CustomerId: ''
                            };
                            objAccount.Source = 'Finicity';
                            objAccount.ProviderAccountId = account.id;
                            objAccount.Institutionid = account.Institutionid;
                            objAccount.AccountType = account.type;
                            objAccount.AccountNumber = account.number;
                            objAccount.AggregationStatusCode = account.aggregationStatusCode;
                            if (account.aggregationStatusCode != null && account.aggregationStatusCode != '') {
                                objAccount.AggregationStatusDescription = AggregationStatusCodes[account.aggregationStatusCode];
                            }
                            if (account.aggregationAttemptDate != null) {
                                objAccount.AggregationAttemptDate = ConvertEPOCDate(account.aggregationAttemptDate);
                            }
                            objAccount.CustomerId = account.customerId;
                            if (account.Detail != null && account.Detail.availableBalanceAmount != null) {
                                objAccount.AvailableBalance = account.Detail.availableBalanceAmount;
                            }
                            objAccount.CurrentBalance = account.balance;
                            if (account.RoutingNumber != null && account.RoutingNumber != '') {
                                objAccount.RoutingNumber = account.RoutingNumber;
                            }
                            if (account.balanceDate != null) {
                                objAccount.BalanceDate = ConvertEPOCDate(account.balanceDate);
                            }
                            lstAccounts.push(objAccount);
                        });
                    }
                }
            }
        } catch (e) {
            return null
        }
        return data = {
            'CustomerDetails': lstAccounts
        };
    }
    return GetFinicityData(input);

    function ConvertEPOCDate(d) {
        var date = new Date(d * 1e3);
        date = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
        return date
    }
}