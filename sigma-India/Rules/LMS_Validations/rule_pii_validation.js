function rule_pii_validation(payload) {
    return {
        'Result': true,
        'Details': {
            'Pan': 'Valid',
            'Aadhar': 'Valid'
        }
    };
}