function rule_FunderIndormation(payload) {

    var FunderList = [{
            'FunderId': '1001',
            'FunderName': 'HDFC',
            'FunderType': 'BANK',
            'GSTIN': '5464647657757',
            'BankDetails': {
                'Name': 'FEDERAL RESERVE BANK',
                'AccountNumber': '68789797979',
                'AccountType': 'saving',
                'Branchidentifiers': {
                    'IFSC': 'HDFC012354'
                }
            }
        },
        {
            'FunderId': '1002',
            'FunderName': 'KOTAK',
            'FunderType': 'BANK',
            'GSTIN': '5464647657757',
            'BankDetails': {
                'Name': 'KOTAK BANK',
                'AccountNumber': '1435464765877678',
                'AccountType': 'checking',
                'Branchidentifiers': {
                    'IFSC': 'KOTK01567'
                }
            }
        },
        {
            'FunderId': '1003',
            'FunderName': 'ICICI',
            'FunderType': 'BANK',
            'GSTIN': '5464647657757',
            'BankDetails': {
                'Name': 'ICICI BANK',
                'AccountNumber': '17779766887989',
                'AccountType': 'saving',
                'Branchidentifiers': {
                    'IFSC': 'ICICI00001'
                }
            }
        }
    ];
    if (payload != null && typeof (payload) != 'undefined') {

        var objResult = FunderList.find(o => o.FunderId== payload.FunderId);
        return objResult;
    }
   
}