function checklistRuleForApproveDrawdown(entityType,entityId) {
	var self = this;
	var factService = self.call('factVerification');
	var dataAttributesService = self.call('dataAttribute');

	if (typeof (entityType) != 'undefined' && entityType != null) {
		return dataAttributesService.get(entityType,entityId, 'product').then(function (productDetail) {
			if (productDetail != null && productDetail.length > 0) {
				var productId = productDetail[0].ProductId;
				return factService.get(entityType,entityId, productId).then(function (factDetail) {
				var result = 'Passed';
				if (factDetail != null && factDetail.length > 0) {
					var unCompleteFacts = factDetail.filter(function (fact) {
						return (fact.currentStatus.toLowerCase() != 'completed')
					});
					if (unCompleteFacts != null && unCompleteFacts.length > 0) {
						result = 'Failed';
					}
				}
				return {
					'result': result,
					'detail': result=='Failed'? ['Verification Pending1']:'',
					'data': null,
					'rejectcode': '',
					'exception': []
				}
			}).catch(function (error) {
				return {
					'result': 'Failed',
					'detail': ['Verification Pending2'],
					'data': null,
					'rejectcode': '',
					'exception': []
				};
			});
		}
		return {
			'result': 'Failed',
			'detail': result=='Failed'? ['product data not found']:'',
			'data': null,
			'rejectcode': '',
			'exception': []
		}
		}).catch(function (error) {
			return {
				'result': 'Failed',
				'detail': ['Verification Pending3'],
				'data': null,
				'rejectcode': '',
				'exception': [error.message]
			};
		});
	}
	 else {
		return {
			'result': 'Failed',
			'detail': ['Verification Pending4'],
			'data': null,
			'rejectcode': '',
			'exception': []
		};
	}
}
