function MetricGenerationDataExtractionRule(payload) {
	var result = 'Passed';
	var zipCode = '';
	if (typeof(payload) != 'undefined') {
	
	if(payload.eventData.EventName == 'ApplicationCreated')
	{
		var filterData = payload.eventData.Data;
		
		if (filterData == null) {
			var Data = null;
		} else {
			var Data = {
				
					'facts': {
						'applicationCount': 1
					},
					'dimensions': {
						'businessType': filterData.BusinessType,
						'industryType': filterData.Industry,
						'partnerId': filterData.PartnerId,
						'purposeOfLoan': filterData.PurposeOfLoan,
						'zipCode': filterData.BusinessZipCode,
						'applicationDate' : filterData.ApplicationDate

					},
					'applicationDate':filterData.ApplicationDate
				
			}
		}
	}
	else if(payload.eventData.EventName == 'ApplicationFundingAdded')
	{
		var filterData = payload.eventData.Data;
		
		if (filterData == null) {
			var Data = null;
		} else {
			var Data = {
				
					'facts': {
						'fundedAmount': filterData.AmountFunded,
						'commissionAmount': filterData.CommissionAmount,
						'fundedApplicationCount': 1
					},
					'dimensions': {
						'businessType': filterData.BusinessType,
						'industryType': filterData.Industry,
						'partnerId': filterData.PartnerId,
						'purposeOfLoan': filterData.PurposeOfLoan,
						'zipCode': filterData.BusinessZipCode,
						'fundingDate':filterData.FundingRequestedDate
					},
					'applicationDate':filterData.ApplicationDate
				
			}
		}
	}
	} else {
		result = 'Failed';
		detail = 'payload is undefined';
	}
	return {
		'result': result,
		'detail': null,
		'data': Data,
		'rejectcode': '',
		'exception': []
	};
}
