var path = require('path');
var fs = require('fs');

var walkSync = function (dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function (file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        }
        else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

function saveProduct(serviceUrl, productFile, token) {
    var api = require('./api_module.js')(token);
    var pathObject = path.parse(productFile);
    if (pathObject.ext.toLocaleLowerCase() == ".json") {
        
        var name = pathObject.name;
        var data = fs.readFileSync(productFile, 'utf8').replace(/\r|\n|\t/g, ' ');        
            return api.post(serviceUrl + '/product', data).then(function (result) {
                if (result.statusCode == 200 || result.statusCode == 204 || result.statusCode == 404) {
                    console.log("[Done] Publishing product: " + name);
                }
                else {
                    console.log("[Error] Failed to publish product: " + JSON.stringify(result) );
                }
            });       
    }
}

function importProduct(serviceUrl, folderPath, token) {
    var files = walkSync(folderPath);
    var productToProcess = [];

    for (var i = 0; i < files.length; i++) {
        productToProcess.push(files[i]);
    }

    productToProcess = productToProcess.map(r => () => saveProduct(serviceUrl, r, token));

    return productToProcess.reduce((a, c) => {
        return a.then(_ => {
            return c();
        }).catch(reason => {         
            console.error('Product failed: ', reason);
            return c();
        });
    }, new Promise((resolve) => resolve({})));
}

module.exports = function (token) {
    return {
        saveProduct: function (serviceUrl, productFile) {
            return saveProduct(serviceUrl, productFile, token);
        },
        importProduct: function (serviceUrl, folderPath) {
            return importProduct(serviceUrl, folderPath, token);
        }
    }
}